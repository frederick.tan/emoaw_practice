// OpenGL 3.3
#version 330

// Output from Fragment Shader
out vec4 pixel_Colour;

void main () {
	//----------------------------------------------
	// Fragment Colour
	//----------------------------------------------
	pixel_Colour = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}