// Project Header
#include "image.h"

// stb_image Header
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// --------------------------------------------------------------------------------
// Image Functions
// --------------------------------------------------------------------------------

// Load an image from file
unsigned char* loadImage(const char *filename, int &width, int &height, int &n) {
	// Force RGBA 
	int force_channels = 4;

	// Load image
	unsigned char *image = stbi_load(filename, &width, &height, &n, force_channels);

	// Channels forced to 4
	n = 4;

	// Check result
	if(!image) {
		// Print error message
		std::cerr << "Error: could not load image: " << filename << std::endl;
		
		// Return error
		return NULL;
	}

	// Temporary buffer
	unsigned char *t = new unsigned char[width*n];

	// Flip image vertically
	for(int iy = 0; iy < height/2; iy++) {
		// Copy row iy into temporary buffer
		memcpy(t, &image[iy*width*n], width*n);

		// Copy row ((height-1)-iy) into row iy
		memcpy(&image[iy*width*n], &image[((height-1)-iy)*width*n], width*n);

		// Copy temporary buffer into row ((height-1)-iy)
		memcpy(&image[((height-1)-iy)*width*n], t, width*n);
	}

	// Delete temporary buffer
	delete[] t;

	// Check dimensions are power-of-2
	if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
		// Print warning message
		std::cerr << "Warning: image " << filename << " is not power-of-2 dimensions" << std::endl;
	}

	// Print log message
	std::cout << "Loaded: " << filename << std::endl;

	// Return image
	return image;
}

// Load a texture from file
GLuint loadTexture(std::string filename) {
	// Load Texture Map from file
	int x, y, n;
	unsigned char *image = loadImage(filename.c_str(), x, y, n);

	// Check image result
	if(image == NULL) {
		// Return error
		return 0;
	}

	// Texture
	GLuint texture;

	// Generate texture
	glGenTextures(1, &texture);

	// Bind texture
	glBindTexture(GL_TEXTURE_2D, texture);

	// Get log base 2 of width and height
	int levels_x = glm::log2((float)x);
	int levels_y = glm::log2((float)y);
	int min_levels = glm::min(levels_x, levels_y);

	// Set storage - log_2(image size)
	glTexStorage2D(GL_TEXTURE_2D, min_levels, GL_RGBA8, x, y);

	// Copy image data into texture
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, x, y, GL_RGBA, GL_UNSIGNED_BYTE, image);

	// Generate Mipmap
	glGenerateMipmap(GL_TEXTURE_2D);

	// Configure texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR); // With mip-mapping
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // With mip-mapping
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // No mip-mapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);

	// Get Maximum Anistropic level
	GLfloat maxAnistropy = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnistropy);

	// Enable Anistropic Filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnistropy);
	
	// Unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete image data
	delete[] image;
	image = NULL;

	// Return texture
	return texture;
}

// Load a surface texture from file
GLuint loadSurfaceTexture(std::string normal_filename, std::string height_filename) {
	// Load Normal Map from file
	int nx, ny, nn;
	unsigned char *normal_image = loadImage(normal_filename.c_str(), nx, ny, nn);

	// Load Texture Map from file
	int hx, hy, hn;
	unsigned char *height_image = loadImage(height_filename.c_str(), hx, hy, hn);

	// Check image result
	if(normal_image == NULL) {
		// Delete Height Image
		if(height_image != NULL) {
			delete[] height_image;
			height_image = NULL;
		}
		// Return error
		return 0;
	}

	// Check image result
	if(height_image == NULL) {
		// Delete Normal Image
		if(normal_image != NULL) {
			delete[] normal_image;
			normal_image = NULL;
		}
		// Return error
		return 0;
	}

	// Check image sizes
	if(nx != hx || ny != hy) {
		// Delete images
		delete[] normal_image;
		delete[] height_image;
		normal_image = NULL;
		height_image = NULL;

		// Return error
		return 0;
	}

	int x = nx;
	int y = ny;

	// Set alpha channel of image
	for(int k = 0; k < x * y; k++) {
		normal_image[k*4 + 3] = height_image[k*4 + 0];
	}

	// Texture
	GLuint texture;

	// Generate texture
	glGenTextures(1, &texture);

	// Bind texture
	glBindTexture(GL_TEXTURE_2D, texture);

	// Get log base 2 of width and height
	int levels_x = glm::log2((float)x);
	int levels_y = glm::log2((float)y);
	int min_levels = glm::min(levels_x, levels_y);

	// Set storage - log_2(image size)
	glTexStorage2D(GL_TEXTURE_2D, min_levels, GL_RGBA8, x, y);

	// Copy image data into texture
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, x, y, GL_RGBA, GL_UNSIGNED_BYTE, normal_image);

	// Generate Mipmap
	glGenerateMipmap(GL_TEXTURE_2D);

	// // Configure texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // With mip-mapping
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // No mip-mapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);

	// Get Maximum Anistropic level
	GLfloat maxAnistropy = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnistropy);

	// Enable Anistropic Filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnistropy);
	
	// Unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete image data
	delete[] normal_image;
	delete[] height_image;
	normal_image = NULL;
	height_image = NULL;

	// Return texture
	return texture;
}