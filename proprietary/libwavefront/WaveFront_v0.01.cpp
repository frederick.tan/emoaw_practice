/*
 * WaveFront.cpp
 *
 *  Created on: Sat May  5 10:46:10 NZST 2018
 *      Author: Frederick Tan
 */

#include <WaveFront.h>

#include <fstream>
#include <iostream>

using namespace std;

#if 0
WaveFront::KeyParserFunctionMap WaveFront::mParserMap = {
	{"cstype",	&WaveFront::ParseGeometricVertex},
	{"deg",		&WaveFront::ParseGeometricVertex},
	{"bmat",	&WaveFront::ParseGeometricVertex},
	{"step",	&WaveFront::ParseGeometricVertex},
	{"p",		&WaveFront::ParseGeometricVertex},
	{"l",		&WaveFront::ParseGeometricVertex},
	{"curv",	&WaveFront::ParseGeometricVertex},
	{"curv2",	&WaveFront::ParseGeometricVertex},
	{"surf",	&WaveFront::ParseGeometricVertex},
	{"parm",	&WaveFront::ParseGeometricVertex},
	{"trim",	&WaveFront::ParseGeometricVertex},
	{"hole",	&WaveFront::ParseGeometricVertex},
	{"scrv",	&WaveFront::ParseGeometricVertex},
	{"sp",		&WaveFront::ParseGeometricVertex},
	{"end",		&WaveFront::ParseGeometricVertex}
};
#endif
WaveFront::WaveFront(const std::string &filename)
	: mParserMap({
				make_pair("v", bind(&WaveFront::ParseGeometricVertex, this, placeholders::_1)),
				make_pair("f", bind(&WaveFront::ParseFaceElements, this, placeholders::_1)),
				make_pair("vp",	bind(&WaveFront::ParseParameterVertex, this, placeholders::_1)),
				make_pair("vn",	bind(&WaveFront::ParseNormalVertex, this, placeholders::_1)),
				make_pair("vt",	bind(&WaveFront::ParseTextureVertex, this, placeholders::_1))
				})
	, mTokenizer([](std::string &line, std::string delimiter) -> Tokens {
				vector<string> tokens;
				size_t pos;
				while ((pos = line.find(delimiter)) != std::string::npos) {
					string token = line.substr(0, pos);
					tokens.push_back(token);
					line.erase(0, pos + 1);
				}
				if (!line.empty()) tokens.push_back(line);
				return tokens;
			})
{
	Parse(filename);
};

WaveFront::~WaveFront()
{
};

bool WaveFront::Parse(const std::string &filename)
{
	ifstream file;
	file.open(filename);

	if (!file.is_open()) return false;

	string line;
	while (getline(file, line))
	{
		if (line[0] == '#') continue;
		size_t pos = line.find(' ');
		if (pos == std::string::npos) continue;
		string key = line.substr(0, pos);
		line.erase(0, pos + 1);
		auto f = mParserMap.find(key);
		if (f == mParserMap.end())
		{
			cout << "Unknown key: " << key << endl;
			continue;
		}
		f->second(mTokenizer(line, " "));
	}
	// Let's provide a normalization vector if it is missing
	if (mNormalVertices.empty())
	{
		cout << "Normal is empty!" << endl;
		for (auto a : mBuffer)
		{
			cout << a[0] << ", " << a[1] << ", " << a[2] << endl;
			mNormalVertices.push_back(glm::normalize(glm::vec4(a[0], a[1], a[2], 0.0f)));
		}
	}
	file.close();
}

bool WaveFront::ParseGeometricVertex(Tokens tokens)
{
	size_t size;
	double x = stod(tokens[0], &size);
	double y = stod(tokens[1], &size);
	double z = stod(tokens[2], &size);
	double w = tokens.size() >= 4 ? stod(tokens[3], &size) : 1.0f;
	mBuffer.push_back(glm::vec4(x, y, z, w));
	//mBuffer.push_back(glm::normalize(glm::vec4(x, y, z, 0.0f)));
	return true;
}

bool WaveFront::ParseFaceElements(Tokens tokens)
{
	int a = stoi(tokens[0]) - 1;
	int b = stoi(tokens[1]) - 1;
	int c = stoi(tokens[2]) - 1;
	mIndexes.push_back(glm::ivec3(a, b, c));
	return true;
}

bool WaveFront::ParseParameterVertex(Tokens tokens)
{
	size_t size;
	double u = stod(tokens[0], &size);
	double v = stod(tokens[1], &size);
	double w = stod(tokens[2], &size);
	mParameterVertices.push_back(glm::vec3(u, v, w));
	return true;
}

bool WaveFront::ParseNormalVertex(Tokens tokens)
{
	size_t size;
	double i = stod(tokens[0], &size);
	double j = stod(tokens[1], &size);
	double k = stod(tokens[2], &size);
	mNormalVertices.push_back(glm::vec4(i, j, k, 0.0f));
	return true;
}

bool WaveFront::ParseTextureVertex(Tokens tokens)
{
	size_t size;
	double u = stod(tokens[0], &size);
	double v = stod(tokens[1], &size);
	double w = stod(tokens[2], &size);
	mTextureVertices.push_back(glm::vec3(u, v, w));
	return true;
}

#if 0
int main(int argc, char *argv[])
{
	cout << argv[1];
	WaveFront image(argv[1]);
	return 0;
}
#endif
