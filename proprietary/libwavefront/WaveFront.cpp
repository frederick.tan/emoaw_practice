/*
 * WaveFront.cpp
 *
 *  Created on: Sat May  5 10:46:10 NZST 2018
 *      Author: Frederick Tan
 */

#include <WaveFront.h>
#include <Material.h>
#include <Materials.h>

#include <fstream>
#include <iostream>

using namespace std;

#if 0
WaveFront::KeyParserFunctionMap WaveFront::mParserMap = {
	{"cstype",	&WaveFront::ParseGeometricVertex},
	{"deg",		&WaveFront::ParseGeometricVertex},
	{"bmat",	&WaveFront::ParseGeometricVertex},
	{"step",	&WaveFront::ParseGeometricVertex},
	{"p",		&WaveFront::ParseGeometricVertex},
	{"l",		&WaveFront::ParseGeometricVertex},
	{"curv",	&WaveFront::ParseGeometricVertex},
	{"curv2",	&WaveFront::ParseGeometricVertex},
	{"surf",	&WaveFront::ParseGeometricVertex},
	{"parm",	&WaveFront::ParseGeometricVertex},
	{"trim",	&WaveFront::ParseGeometricVertex},
	{"hole",	&WaveFront::ParseGeometricVertex},
	{"scrv",	&WaveFront::ParseGeometricVertex},
	{"sp",		&WaveFront::ParseGeometricVertex},
	{"end",		&WaveFront::ParseGeometricVertex}
};
#endif
WaveFront::WaveFront(const std::string &filename)
	: mParserMap({
				make_pair("v", bind(&WaveFront::ParseGeometricVertex, this, placeholders::_1)),
				make_pair("f", bind(&WaveFront::ParseFaceElements, this, placeholders::_1)),
				make_pair("vp",	bind(&WaveFront::ParseParameterVertex, this, placeholders::_1)),
				make_pair("vn",	bind(&WaveFront::ParseNormalVertex, this, placeholders::_1)),
				make_pair("vt",	bind(&WaveFront::ParseTextureVertex, this, placeholders::_1)),
				make_pair("mtllib", bind(&WaveFront::ParseMaterial, this, placeholders::_1)),
				make_pair("usemtl", bind(&WaveFront::CreateGroup, this, placeholders::_1))
				})
	, mTokenizer([](std::string &line, std::string delimiter) -> Tokens {
				vector<string> tokens;
				size_t pos;
				printf("\nOriginal line: \"%s\"\n", line.c_str());
				//while ((pos = line.find_first_not_of(delimiter)) != std::string::npos) {
				while (!line.empty())
				{
					pos = line.find_first_not_of(delimiter);
					line.erase(0, pos);
					pos = line.find_first_of(delimiter);
					string token = line.substr(0, pos);
					printf("pos %d\n", pos);
					if (!token.empty())
					{
						tokens.push_back(token);
						printf("\nAdding token: \"%s\"\n", token.c_str());
					}
					if (pos == std::string::npos) line.clear();
					else line.erase(0, pos + 1);
				}
				if (!line.empty() && line != " ")
				{
					printf("\nAdding line: \"%s\"\n", line.c_str());
					tokens.push_back(line);
				}
				return tokens;
			})
	, mDefaultMaterial(new Material(Material::Coefficients({0.0215,  0.1745,   0.0215}),
									Material::Coefficients({0.07568, 0.61424,  0.07568}),
									Material::Coefficients({0.633,   0.727811, 0.633}),
									1,
									76.8))
{
	Parse(filename);
};

WaveFront::~WaveFront()
{
};

bool WaveFront::Parse(const std::string &filename)
{
	ifstream file;
	file.open(filename);

	if (!file.is_open()) return false;

	string line;
	while (getline(file, line))
	{
		if (line[0] == '#') continue;
		size_t pos = line.find(' ');
		if (pos == std::string::npos) continue;
		string key = line.substr(0, pos);
		line.erase(0, pos + 1);
		auto f = mParserMap.find(key);
		if (f == mParserMap.end())
		{
			//cout << "Unknown key: " << key << endl;
			continue;
		}
		f->second(mTokenizer(line, " \n\r"));
	}
	// Let's provide a normalization vector if it is missing
	if (mNormalVertices.empty())
	{
		for (auto a : mBuffer)
		{
			mNormalVertices.push_back(glm::normalize(glm::vec4(a[0], a[1], a[2], 1.0f)));
		}
	}
	file.close();

	return true;
}

bool WaveFront::ParseGeometricVertex(Tokens tokens)
{
	size_t size;
	double x = stod(tokens[0], &size);
	double y = stod(tokens[1], &size);
	double z = stod(tokens[2], &size);
	double w = tokens.size() >= 4 ? stod(tokens[3], &size) : 1.00f;
	mBuffer.push_back(glm::vec4(x, y, z, w));
	return true;
}

bool WaveFront::ParseFaceElements(Tokens tokens)
{
	if (mGroups.size() == 0)
	{
		mGroups.push_back(make_pair(mDefaultMaterial, std::vector<glm::ivec3>()));
	}
	std::vector <int> v;
	std::vector <int> t;
	std::vector <int> n;
	for (auto stoken: tokens)
	{
		Tokens vertices = mTokenizer(stoken, "/\n\r");
		v.push_back(stoi(vertices[0]) - 1);
		if (vertices.size() >= 2 && !vertices[1].empty())
		{
			t.push_back(stoi(vertices[1]) - 1);
		}
		if (vertices.size() >= 3 && !vertices[2].empty())
		{
			n.push_back(stoi(vertices[2]) - 1);
		}
	}

	for (int index = 1; index < (v.size() - 1); index++)
	{
		//cout << v[0] << ", " << v[index] << ", " << v[index + 1] << endl;
		mGroups.back().second.push_back(glm::ivec3(v[0], v[index], v[index + 1]));
		mIndexes.push_back(glm::ivec3(v[0], v[index], v[index + 1]));
		if (t.size() >= 3)
		{
			mTextureIndexes.push_back(glm::ivec3(t[0], t[index], t[index + 1]));
		}
		if (n.size() >= 3)
		{
			mNormalIndexes.push_back(glm::ivec3(n[0], n[index], n[index + 1]));
		}
	}

	return true;
}

bool WaveFront::ParseParameterVertex(Tokens tokens)
{
	// We do not support this at the moment, so just return
	if (0)
	{
		size_t size;
		double u = stod(tokens[0], &size);
		double v = tokens.size() >= 2 ? stod(tokens[1], &size) : 1.0f;
		double w = tokens.size() >= 3 ? stod(tokens[2], &size) : 1.0f;
		mParameterVertices.push_back(glm::vec4(u, v, w, 0.0f));
	}
	return true;
}

bool WaveFront::ParseNormalVertex(Tokens tokens)
{
	size_t size;
	double i = stod(tokens[0], &size);
	double j = stod(tokens[1], &size);
	double k = stod(tokens[2], &size);
	mNormalVertices.push_back(glm::vec4(i, j, k, 1.0f));
	return true;
}

bool WaveFront::ParseTextureVertex(Tokens tokens)
{
	size_t size;
	double u = stod(tokens[0], &size);
	double v = tokens.size() >= 2 ? stod(tokens[1], &size) : 0.0f;
	double w = tokens.size() >= 3 ? stod(tokens[2], &size) : 0.0f;
	mTextureVertices.push_back(glm::vec4(u, v, w, 1.0f));
	return true;
}

bool WaveFront::ParseMaterial(Tokens tokens)
{
	for (auto a: tokens)
	{
		mMaterialsDef.push_back(make_shared<Materials>(a));
	}
	return 0;
}

bool WaveFront::CreateGroup(Tokens tokens)
{
	for (auto const&a: mMaterialsDef)
	{
		if (auto const &m = a->GetMaterial(tokens[0]))
		{
			mGroups.push_back(make_pair(m, std::vector<glm::ivec3>()));
			break;	
		}
	}
	return true;
}

#if 0
int main(int argc, char *argv[])
{
	cout << argv[1];
	WaveFront image(argv[1]);
	return 0;
}
#endif
