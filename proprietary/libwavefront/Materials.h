/*
 * Materials.h
 *
 *  Created on: Mon May  7 22:29:38 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef _MATERIALS_H_
#define _MATERIALS_H_

#include <memory>
#include <string>
#include <map>

class Material;

class Materials
{
private:
	static const std::string Keys[6];
public:
	Materials(const std::string &fileName);
	virtual ~Materials();
	void Print();
	std::shared_ptr<Material> GetMaterial(const std::string &name);
// {return mMaterials[name];};
private:
	bool Parse(const std::string &fileName);
private:
	std::map<std::string, std::shared_ptr<Material>> mMaterials;
};
#endif
