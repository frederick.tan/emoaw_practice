/*
 * Material.cpp
 *
 *  Created on: Mon May  7 22:21:27 NZST 2018
 *      Author: Frederick Tan
 */

#include <Material.h>

#include <algorithm>
#include <iostream>

using namespace std;

Material::Material()
	: mIllum(1)
	, mNs(1.0)
	, mTokenizer([](string &line, const string &delimiter) -> Coefficients {
			Coefficients coeffs;
			size_t pos;
			while ((pos = line.find(delimiter)) != string::npos) {
				string token = line.substr(0, pos);
				size_t size;
				double value = stod(token, &size);
				coeffs.push_back(value);
				line.erase(0, pos + 1);
			}
			if (!line.empty())
			{
				size_t size;
				double value = stod(line, &size);
				coeffs.push_back(value);
			}
			return coeffs;
		})
{
	mKa = {0.0f, 0.0f, 0.0f};
	mKd = {0.0f, 0.0f, 0.0f};
	mKs = {0.0f, 0.0f, 0.0f};
};

Material::Material(Coefficients ka, Coefficients kd, Coefficients ks, int illum, double ns)
	: mKa(ka)
	, mKd(kd)
	, mKs(ks)
	, mIllum(illum)
	, mNs(ns)
	, mTokenizer([](string &line, const string &delimiter) -> Coefficients {
			Coefficients coeffs;
			size_t pos;
			while ((pos = line.find(delimiter)) != string::npos) {
				string token = line.substr(0, pos);
				size_t size;
				double value = stod(token, &size);
				coeffs.push_back(value);
				line.erase(0, pos + 1);
			}
			if (!line.empty())
			{
				size_t size;
				double value = stod(line, &size);
				coeffs.push_back(value);
			}
			return coeffs;
		})
{
}

bool Material::Parse(string &line)
{
	size_t pos = line.find(' ');
	if (pos == string::npos) return true;
	string key = line.substr(0, pos);
	line.erase(0, pos + 1);
	transform(key.begin(), key.end(), key.begin(), ::tolower);
	if (key == "ka")
	{
		mKa = mTokenizer(line, " ");
	}
	else if (key == "kd")
	{
		mKd = mTokenizer(line, " ");
	}
	else if (key == "ks")
	{
		mKs = mTokenizer(line, " ");
	}
	else if (key == "illum")
	{
		mIllum = stoi(line);
	}
	else if (key == "ns")
	{
		size_t size;
		mNs = stod(line, &size);
	}
	return true;
}

Material::~Material()
{
};

void Material::Print()
{
	cout << "Ka: " << mKa[0] << ", " << mKa[1] << ", " << mKa[2] << endl;
	cout << "Kd: " << mKd[0] << ", " << mKd[1] << ", " << mKd[2] << endl;
	cout << "Ks: " << mKs[0] << ", " << mKs[1] << ", " << mKs[2] << endl;
	cout << "Illum: " << mIllum << endl;
	cout << "Ns: " << mNs << endl;
}

