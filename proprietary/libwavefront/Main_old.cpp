/**
 * Assignment number 1, 158.709, 2018 S1
 * Tan, Frederick Abril, 16227617
 * This program will create an empty world with only a flag in the middle
 **/

#include <OpenGLWrapper.h>
#include <WaveFront.h>
#include <BasicShader.h>
#include <TestScene.h>
#include <Transform.h>

using namespace OpenGLWrapper;

int main(int argc, char *argv[])
{
	unsigned int speedFactor = 1;
	if (argc == 2)
	{
		if (argv[1][0] == 'h')
		{
			printf("Usage: %s <speed>\n", argv[0]);
			printf("Where <speed> is the speed factor from 1 to 10 (1x to 10x)\n");
		}
		else
		{
			speedFactor = atoi(argv[1]);
			if (speedFactor > 10) speedFactor = 10;
		}
	}
	OpenGLWrapper::OpenGLWrapper oglw(800, 800, "Fred Test");
	BasicShader shader;
	TestScene testScene(argv[1]);

	testScene.CreateScene();
	shader.LoadObjectData(testScene);
	shader.UseProgram();

	// Model Matrix
#if 1
	float modelMatrix[16];
	Transform::Translate(0.0f, 0.0f, 0.0f, modelMatrix);
	shader.SetMatrix(Shader::mtModel, modelMatrix);
	// ----------------------------------------

	// View Matrix
	float viewMatrix[16];
	float viewPosition[3] = { 0.0f,  0.0f,  0.0f};
	float viewUp[3]       = { 0.0f,  2.0f,  0.0f};
	float viewForward[3]  = { 0.0f,  0.0f, -2.0f};

	Transform::Normalize(viewUp, viewUp);
	Transform::Normalize(viewForward, viewForward);

	// identity(viewMatrix);
	Transform::View(viewPosition, viewForward, viewUp, viewMatrix);
	shader.SetMatrix(Shader::mtView, viewMatrix);
	// Projection Matrix
	float projectionMatrix[16];
	Transform::Identity(projectionMatrix);
	shader.SetMatrix(Shader::mtProjection, projectionMatrix);
#endif
	while (!oglw.ShouldCloseWindow())
	{
		// Make the context of the given window current on the calling thread
		oglw.MakeContextCurrent();

		oglw.ClearWindow();

		// Use Program
		shader.UseProgram();

		// // ----------------------------------------
		// // Rotation Matrix - X
		float rotation[16];
		Transform::RotateY(speedFactor * glfwGetTime() / 10, rotation);

		shader.SetMatrix(Shader::mtModel, rotation);

		glBindVertexArray(shader.Vao());

		// Draw Elements (Triangles)
		glDrawElements(GL_TRIANGLES, testScene.IndexesSize(), GL_UNSIGNED_INT, NULL);

		// Swap the back and front buffers
		oglw.SwapBuffers();

		// Poll window events
		glfwPollEvents();
	}
	return 0;
}
