/**
 * @file: TestScene.h
 * @about: 
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_TEST_SCENE_H__
#define __OPENGLWRAPPER_TEST_SCENE_H__

#include <SceneStager.h>

#include <string>

class TestScene : public OpenGLWrapper::SceneStager
{
public:
	TestScene(const std::string &filename);
	virtual ~TestScene();
	virtual bool CreateScene();
private:
	const std::string mFilename;
};

#endif
