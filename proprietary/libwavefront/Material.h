/*
 * Material.h
 *
 *  Created on: Mon May  7 22:21:27 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include <functional>
#include <string>
#include <vector>

class Material
{
public:
	typedef std::vector<double> Coefficients;
	typedef std::function<Coefficients (std::string&, const std::string&)> Tokenizer;
public:
	Material();
	Material(Coefficients ka, Coefficients kd, Coefficients ks, int illum, double ns);
	virtual ~Material();
	bool Parse(std::string &line);
	void Print();
	const Coefficients & GetKa() const { return mKa; };
	const Coefficients & GetKd() const { return mKd; };
	const Coefficients & GetKs() const { return mKs; };
	int GetIllumination() const { return mIllum; };
	double GetNs() const { return mNs; };
private:
	Coefficients mKa;
	Coefficients mKd;
	Coefficients mKs;
	int mIllum;
	double mNs;
	Tokenizer mTokenizer;
};
#endif
