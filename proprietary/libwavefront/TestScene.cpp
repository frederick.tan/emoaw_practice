/**
 * @file: TestScene.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <TestScene.h>
#include <BasicShape.h>
#include <WaveFront.h>

#include <stdio.h>

using namespace OpenGLWrapper;

TestScene::TestScene(const std::string &filename)
	: mFilename(filename)
{
}

TestScene::~TestScene()
{
	for (auto &a: mShapes)
	{
		delete a;
	}
	mShapes.clear();
}

bool TestScene::CreateScene()
{
	BasicShape *object = new WaveFront(mFilename);
	object->SetColor(0, 0, 1.0f);
	object->SetStartingPoint(-0.02f, -0.25f, 0.0f);
	//object->SetSize(0.0162f);
	//object->SetDensity(10, 10);
	//object->Create();
	mShapes.push_back(object);

	return true;
}
