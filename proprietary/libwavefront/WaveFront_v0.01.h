/*
 * WaveFront.h
 *
 *  Created on: Sat May  5 10:46:10 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef _WAVE_FRONT_H_
#define _WAVE_FRONT_H_

#include <opengl_platform_headers.h>

#include <functional>
#include <map>
#include <string>
#include <vector>

class WaveFront
{
private:
	typedef std::vector<std::string> Tokens;
	typedef std::function<bool (Tokens)> ParserFunction;
	typedef std::string Key;
	typedef std::map<Key, ParserFunction> KeyParserFunctionMap;
	typedef std::function<Tokens (std::string&, std::string)> Tokenizer;
public:
	WaveFront(const std::string &filename);
	virtual ~WaveFront();
	const GLvoid* Data() { return (GLvoid*)mBuffer.data(); };
	GLsizeiptr DataSize() { return mBuffer.size() * sizeof(glm::vec4); };
	const GLvoid* Indexes() { return (GLvoid*)mIndexes.data(); };
	GLsizeiptr IndexesSize() { return mIndexes.size() * sizeof(glm::ivec3); };
	const GLvoid* NV() { return (GLvoid*)mNormalVertices.data(); };
	GLsizeiptr NVSize() { return mNormalVertices.size() * sizeof(glm::vec3); };
	const GLvoid* PV() { return (GLvoid*)mParameterVertices.data(); };
	GLsizeiptr PVSize() { return mParameterVertices.size() * sizeof(glm::vec3); };
	const GLvoid* TV() { return (GLvoid*)mTextureVertices.data(); };
	GLsizeiptr TVSize() { return mTextureVertices.size() * sizeof(glm::vec3); };
public:
	bool Parse(const std::string &filename);
	bool ParseGeometricVertex(Tokens tokens);
	bool ParseFaceElements(Tokens tokens);
	bool ParseParameterVertex(Tokens tokens);
	bool ParseNormalVertex(Tokens tokens);
	bool ParseTextureVertex(Tokens tokens);
protected:
	KeyParserFunctionMap mParserMap;
	Tokenizer mTokenizer;
	std::vector<glm::vec4> mBuffer;
	std::vector<glm::ivec3> mIndexes;
	std::vector<glm::vec3> mParameterVertices;
	std::vector<glm::vec4> mNormalVertices;
	std::vector<glm::vec3> mTextureVertices;
};
#endif
