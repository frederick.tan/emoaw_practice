/**
 * @file: Transform.h
 * @about:
 * @author: Frederick Tan
 **/

#include <Transform.h>

#include <cmath>

namespace OpenGLWrapper
{

Transform::Transform()
{
}

Transform::~Transform()
{
}

void Transform::Identity(float I[16])
{
	I[0]  = 1.0f;  I[4]  = 0.0f;  I[8]  = 0.0f;  I[12] = 0.0f;
	I[1]  = 0.0f;  I[5]  = 1.0f;  I[9]  = 0.0f;  I[13] = 0.0f;
	I[2]  = 0.0f;  I[6]  = 0.0f;  I[10] = 1.0f;  I[14] = 0.0f;
	I[3]  = 0.0f;  I[7]  = 0.0f;  I[11] = 0.0f;  I[15] = 1.0f;
}

void Transform::Translate(float tx, float ty, float tz, float T[16])
{
	T[0]  = 1.0f;  T[4]  = 0.0f;  T[8]  = 0.0f;  T[12] = tx;
	T[1]  = 0.0f;  T[5]  = 1.0f;  T[9]  = 0.0f;  T[13] = ty;
	T[2]  = 0.0f;  T[6]  = 0.0f;  T[10] = 1.0f;  T[14] = tz;
	T[3]  = 0.0f;  T[7]  = 0.0f;  T[11] = 0.0f;  T[15] = 1.0f;
}

void Transform::RotateX(float theta, float Rx[16])
{
	// Calculate sin(theta) and cos(theta)
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);

	Rx[0]  = 1.0f;  Rx[4]  = 0.0f;      Rx[8]  =      0.0f;  Rx[12] = 0.0f;
	Rx[1]  = 0.0f;  Rx[5]  = cosTheta;  Rx[9]  = -sinTheta;  Rx[13] = 0.0f;
	Rx[2]  = 0.0f;  Rx[6]  = sinTheta;  Rx[10] =  cosTheta;  Rx[14] = 0.0f;
	Rx[3]  = 0.0f;  Rx[7]  = 0.0f;      Rx[11] =      0.0f;  Rx[15] = 1.0f;
}

void Transform::RotateY(float theta, float Ry[16])
{
	// Calculate sin(theta) and cos(theta)
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);

	Ry[0]  =  cosTheta;  Ry[4]  = 0.0f;  Ry[8]  =  sinTheta;  Ry[12] = 0.0f;
	Ry[1]  =  0.0f;      Ry[5]  = 1.0f;  Ry[9]  =      0.0f;  Ry[13] = 0.0f;
	Ry[2]  = -sinTheta;  Ry[6]  = 0.0f;  Ry[10] =  cosTheta;  Ry[14] = 0.0f;
	Ry[3]  =  0.0f;      Ry[7]  = 0.0f;  Ry[11] =      0.0f;  Ry[15] = 1.0f;
}

void Transform::RotateZ(float theta, float Rz[16])
{
	// Calculate sin(theta) and cos(theta)
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);

	Rz[0]  = cosTheta;  Rz[4]  = -sinTheta;  Rz[8]  = 0.0f;  Rz[12] = 0.0f;
	Rz[1]  = sinTheta;  Rz[5]  =  cosTheta;  Rz[9]  = 0.0f;  Rz[13] = 0.0f;
	Rz[2]  = 0.0f;      Rz[6]  =  0.0f;      Rz[10] = 1.0f;  Rz[14] = 0.0f;
	Rz[3]  = 0.0f;      Rz[7]  =  0.0f;      Rz[11] = 0.0f;  Rz[15] = 1.0f;
}

void Transform::Rotate(float theta, float rx, float ry, float rz, float R[16])
{
	// Calculate sin(theta) and cos(theta)
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);

	float l = sqrt(rx*rx + ry*ry + rz*rz);
	rx /= l;
	ry /= l;
	rz /= l;

	R[0]  = cosTheta + (1-cosTheta)*rx*rx;		R[4]  = (1-cosTheta)*rx*ry - rz*sinTheta;
	R[8]  = (1 - cosTheta)*rx*ry + ry*sinTheta;	R[12] = 0.0f;

	R[1]  = (1-cosTheta)*rx*ry + rz*sinTheta;	R[5]  = cosTheta + (1-cosTheta)*ry*ry;
	R[9]  = (1 - cosTheta)*ry*rz - rx*sinTheta;	R[13] = 0.0f;

	R[2]  = (1-cosTheta)*rx*rz - ry*sinTheta;	R[6]  = (1-cosTheta)*ry*rz + rx*sinTheta;
	R[10] = cosTheta + (1-cosTheta)*rz*rz;		R[14] = 0.0f;

	R[3]  = 0.0f;								R[7]  = 0.0f;
	R[11] = 0.0f;								R[15] = 1.0f;
}

void Transform::Scale(float sx, float sy, float sz, float S[16])
{
	// Scaling Matrix
	S[0]  = sx;    S[4]  =  0.0f;  S[8]  = 0.0f;  S[12] = 0.0f;
	S[1]  = 0.0f;  S[5]  =  sy;    S[9]  = 0.0f;  S[13] = 0.0f;
	S[2]  = 0.0f;  S[6]  =  0.0f;  S[10] = sz;    S[14] = 0.0f;
	S[3]  = 0.0f;  S[7]  =  0.0f;  S[11] = 0.0f;  S[15] = 1.0f;
}

void Transform::Multiply44(float a[16], float b[16], float c[16])
{
	// Multiply each row of A with each column of B to give C
	c[0]  = a[0]*b[0]  + a[4]*b[1]  + a[8]*b[2]   + a[12]*b[3];
	c[4]  = a[0]*b[4]  + a[4]*b[5]  + a[8]*b[6]   + a[12]*b[7];
	c[8]  = a[0]*b[8]  + a[4]*b[9]  + a[8]*b[10]  + a[12]*b[11];
	c[12] = a[0]*b[12] + a[4]*b[13] + a[8]*b[14]  + a[12]*b[15];

	c[1]  = a[1]*b[0]  + a[5]*b[1]  + a[9]*b[2]   + a[13]*b[3];
	c[5]  = a[1]*b[4]  + a[5]*b[5]  + a[9]*b[6]   + a[13]*b[7];
	c[9]  = a[1]*b[8]  + a[5]*b[9]  + a[9]*b[10]  + a[13]*b[11];
	c[13] = a[1]*b[12] + a[5]*b[13] + a[9]*b[14]  + a[13]*b[15];

	c[2]  = a[2]*b[0]  + a[6]*b[1]  + a[10]*b[2]  + a[14]*b[3];
	c[6]  = a[2]*b[4]  + a[6]*b[5]  + a[10]*b[6]  + a[14]*b[7];
	c[10] = a[2]*b[8]  + a[6]*b[9]  + a[10]*b[10] + a[14]*b[11];
	c[14] = a[2]*b[12] + a[6]*b[13] + a[10]*b[14] + a[14]*b[15];

	c[3]  = a[3]*b[0]  + a[7]*b[1]  + a[11]*b[2]  + a[15]*b[3];
	c[7]  = a[3]*b[4]  + a[7]*b[5]  + a[11]*b[6]  + a[15]*b[7];
	c[11] = a[3]*b[8]  + a[7]*b[9]  + a[11]*b[10] + a[15]*b[11];
	c[15] = a[3]*b[12] + a[7]*b[13] + a[11]*b[14] + a[15]*b[15];
}

void Transform::Multiply3(float s, float u[3], float v[3])
{
	v[0] = s * u[0];
	v[1] = s * u[1];
	v[2] = s * u[2];
}

void Transform::Multiply4(float s, float u[4], float v[4])
{
	v[0] = s * u[0];
	v[1] = s * u[1];
	v[2] = s * u[2];
	v[3] = s * u[3];
}

float Transform::Length3(float v[3])
{
	return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

float Transform::Length4(float v[4])
{
	return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3]);
}

void Transform::Normalize(float v[3], float u[3])
{
	return Multiply3(1.0f/Length3(v), v, u);
}

void Transform::CrossProduct(float a[3], float b[3], float c[3])
{
	c[0] = a[1]*b[2] - a[2]*b[1];
	c[1] = a[2]*b[0] - a[0]*b[2];
	c[2] = a[0]*b[1] - a[1]*b[0];
}

float Transform::DotProduct3(float a[3], float b[3])
{
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

float Transform::DotProduct4(float a[4], float b[4])
{
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + a[3]*b[3];
}

void Transform::View(float p[4], float f[3], float u[3], float V[16])
{
	// Calculate right vector
	float r[3];
	CrossProduct(f, u, r);

	// View rotation and translation
	float Vr[16], Vt[16];

	// Set view rotation
	Vr[0]  =  r[0];  Vr[4]  =  r[1];  Vr[8]  =  r[2];  Vr[12] =  0.0f;
	Vr[1]  =  u[0];  Vr[5]  =  u[1];  Vr[9]  =  u[2];  Vr[13] =  0.0f;
	Vr[2]  = -f[0];  Vr[6]  = -f[1];  Vr[10] = -f[2];  Vr[14] =  0.0f;
	Vr[3]  =  0.0f;  Vr[7]  =  0.0f;  Vr[11] =  0.0f;  Vr[15] =  1.0f;

	// Set view translation
	Translate(-p[0], -p[1], -p[2], Vt);

	// Calculate view transform
	Multiply44(Vr, Vt, V);
}

void Transform::Orthographic(float width, float height, float near1, float far1, float matrix[16])
{
	matrix[0] = 2.0/width;	matrix[4]	= 0.0f;
	matrix[8] =  0.0f;		matrix[12] = 0.0f;

	matrix[1] = 0.0f;		matrix[5]	= 2.0f/height;
	matrix[9] =  0.0f;		matrix[13] = 0.0f;

	matrix[2] = 0.0f;		matrix[6]	= 0.0f;
	matrix[10] = -2.0f / (far1-near1);  matrix[14] = -(far1+near1) / (far1-near1);

	matrix[3] = 0.0f;		matrix[7]  = 0.0f;
	matrix[11] =  0.0f;		matrix[15] = 1.0f;
}

void Transform::Perspective(float aspect, float fov, float near1, float far1, float matrix[16])
{
	// Calculate f
	float f = 1.0f / tan(fov * 0.5);

	// Create Perspective matrix
	matrix[0] = f/aspect;						matrix[4] = 0.0f;
	matrix[8] = 0.0f;							matrix[12] = 0.0f;

	matrix[1] = 0.0f;							matrix[5] = f;
	matrix[9] = 0.0f;							matrix[13] = 0.0f;

	matrix[2] = 0.0f;							matrix[6] = 0.0f;
	matrix[10] = (far1+near1)/(near1-far1);		matrix[14] = (2*far1*near1) / (near1 - far1);

	matrix[3] = 0.0f;							matrix[7] = 0.0f;
	matrix[11] = -1.0f; matrix[15] = 0.0f;
}
};
