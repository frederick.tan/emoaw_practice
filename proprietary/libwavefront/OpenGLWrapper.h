/**
 * @file: OpenGLWrapper.h
 * @about: Wrapper class for OpenGL
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_OPENGLWRAPPER_H__
#define __OPENGLWRAPPER_OPENGLWRAPPER_H__

#include <opengl_platform_headers.h>

#include <string.h>
#include <memory>

namespace OpenGLWrapper
{

class OpenGLWrapper
{
public:
	OpenGLWrapper(int width, int height, const char *title);
	virtual ~OpenGLWrapper();
	bool ShouldCloseWindow();
	void MakeContextCurrent();
	void SwapBuffers();
	void ClearWindow();
	int GetKey();
private:
	static void OnError(int error, const char *description);
	static void OnWindowClose(GLFWwindow *window);
	static void OnFramebufferSize(GLFWwindow *window, int width, int height);
	bool Init();
	bool CreateWindow();
private:
	static const int MajorVersion;
	static const int MinorVersion;
private:
	int mWidth;
	int mHeight;
	GLFWwindow* mWindow;
	GLFWmonitor* mMonitor;
	GLFWwindow* mSharedWindow;
	std::string mTitle;
};
};

#endif
