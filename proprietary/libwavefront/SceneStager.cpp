/**
 * @file: SceneStager.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <SceneStager.h>
#include <BasicShape.h>

#include <stdio.h>

namespace OpenGLWrapper
{

SceneStager::SceneStager()
{
}

SceneStager::~SceneStager()
{
}

GLsizeiptr SceneStager::DataSize()
{
	GLsizeiptr size = 0;
	for (auto &a : mShapes)
	{
		size += a->DataSize();
	}
	return size;
}

GLsizeiptr SceneStager::IndexesSize()
{
	GLsizeiptr size = 0;
	for (auto &a : mShapes)
	{
		size += a->IndexesSize();
	}
	return size;
}

};
