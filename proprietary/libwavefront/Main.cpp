/**
 * Assignment number 2, 158.709, 2018 S1
 * Tan, Frederick Abril, 16227617
 * This program will create an empty world with only a flag in the middle
 **/

#include <OpenGLWrapper.h>
#include <image.h>
#include <Material.h>
#include <PhongShader.h>
#include <WaveFront.h>
#include <Transform.h>

#include <iostream>
#include <memory>

using namespace OpenGLWrapper;
using namespace std;

int main(int argc, char *argv[])
{
	unsigned int speedFactor = 1;
	int useTextureFile = 0;
	if (argc == 2)
	{
		if (argv[1][0] == 'h')
		{
			printf("Usage: %s <OBJ file> <optional: texture file>\n", argv[0]);
		}
		else
		{
			speedFactor = atoi(argv[1]);
			if (speedFactor > 10) speedFactor = 10;
		}
	}
	string textureFile;
	if (argc >= 3)
	{
		textureFile = (const char*)argv[2];
		useTextureFile = 1;
	}
	OpenGLWrapper::OpenGLWrapper oglw(800, 800, "WaveFront Object Viewer");
	PhongShader shader;
	WaveFront object(argv[1]);
	shader.LoadObjectData(&object,textureFile);
	shader.UseProgram();
	// Get Sampler Location
	GLuint textureMapLoc = glGetUniformLocation(shader.GetProgram(), "u_texture_Map");

	// Set Sample Texture Unit
	glUniform1i(textureMapLoc, 0);

	// Get Sampler Location
	GLuint useTextureFileLoc = glGetUniformLocation(shader.GetProgram(), "frag_Use_Texture");

	// Set Sample Texture Unit
	glUniform1i(useTextureFileLoc, useTextureFile);
	// ----------------------------------------
	// Model Matrix
	glm::mat4 modelMatrix = glm::mat4();
	float modelThetaX = 0.0f;
	float modelThetaY = 0.0f;
	float modelThetaZ = 0.0f;

	// Get Model Matrix location
	GLint modelLoc = glGetUniformLocation(shader.GetProgram(), "u_Model");

	// Copy Rotation Matrix to Shader
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
	// ----------------------------------------

	// ----------------------------------------
	// View Matrix
	glm::mat4 viewMatrix;
	glm::vec3 viewPosition(0.0f,  0.0f,  8.0f);
	glm::vec3 viewUp      (0.0f,  1.0f,  0.0f);
	glm::vec3 viewForward (0.0f,  0.0f, -1.0f);

	// Normalise Vectors
	viewUp      = glm::normalize(viewUp);
	viewForward = glm::normalize(viewForward);

	// Construct View Matrix
	viewMatrix = glm::lookAt(viewPosition, viewPosition + viewForward, viewUp);

	// Get View Matrix location
	GLint viewLoc = glGetUniformLocation(shader.GetProgram(), "u_View");

	// Copy View Matrix to Shader
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(viewMatrix));
	// ----------------------------------------

	// ----------------------------------------
	// Projection Matrix
	glm::mat4 projectionMatrix;
	
	// Calculate Perspective Projection
	projectionMatrix = glm::perspective(glm::radians(67.0f), 1.0f, 0.2f, 50.0f);

	// Get Projection Matrix location
	GLint projectionLoc = glGetUniformLocation(shader.GetProgram(), "u_Projection");

	// Copy Projection Matrix to Shader
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	// ----------------------------------------

	while (!oglw.ShouldCloseWindow())
	{
		// Make the context of the given window current on the calling thread
		oglw.MakeContextCurrent();

		oglw.ClearWindow();

		// Use Program
		shader.UseProgram();

		// ----------------------------------------		
		// Input/Output - Model Matrix
		// ----------------------------------------
		int key = oglw.GetKey();
		if (key != GLFW_KEY_UNKNOWN)
		{
			bool updateModelMatrix = false, updateViewMatrix = false;
			switch (key)
			{
			case GLFW_KEY_UP:
				updateModelMatrix = true;
				modelThetaY += 0.05f;
			break;
			case GLFW_KEY_DOWN:
				updateModelMatrix = true;
				modelThetaY -= 0.05f;
			break;
			case GLFW_KEY_LEFT:
				updateModelMatrix = true;
				modelThetaX -= 0.05f;
			break;
			case GLFW_KEY_RIGHT:
				updateModelMatrix = true;
				modelThetaX += 0.05f;
			break;
			case GLFW_KEY_A:
				updateModelMatrix = true;
				modelThetaZ -= 0.05f;
			break;
			case GLFW_KEY_S:
				updateModelMatrix = true;
				modelThetaZ += 0.05f;
			break;
			case GLFW_KEY_KP_ADD:
				updateViewMatrix = true;
				viewPosition.z -= 0.05f;
			break;
			case GLFW_KEY_KP_SUBTRACT:
				updateViewMatrix = true;
				viewPosition.z += 0.05f;
			break;
			case GLFW_KEY_PAGE_UP:
				updateViewMatrix = true;
				viewPosition.y -= 0.05f;
			break;
			case GLFW_KEY_PAGE_DOWN:
				updateViewMatrix = true;
				viewPosition.y += 0.05f;
			break;
			}
			if (updateModelMatrix)
			{
				// Calculate Model Matrix
				modelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f)) *
							glm::rotate(glm::mat4(), modelThetaX, glm::vec3(1.0f, 0.0f, 0.0f)) *
							glm::rotate(glm::mat4(), modelThetaY, glm::vec3(0.0f, 1.0f, 0.0f)) *
							glm::rotate(glm::mat4(), modelThetaZ, glm::vec3(0.0f, 0.0f, 1.0f));

				// Copy Rotation Matrix to Shader
				glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMatrix));
			}
			else if (updateViewMatrix)
			{
				// // Construct View Matrix
				viewMatrix = glm::lookAt(viewPosition, viewPosition + viewForward, viewUp);

				// // Copy View Matrix to Shader
				glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(viewMatrix));
			}
		}

		glBindVertexArray(shader.Vao());

		// Set active Texture Unit 0
		glActiveTexture(GL_TEXTURE0);

		// Bind Texture Map
		glBindTexture(GL_TEXTURE_2D, shader.GetTexture());

		const WaveFront::DrawGroups drawGroups = object.GetDrawGroups();
		size_t size_processed = 0;
		int kaLoc = glGetUniformLocation(shader.GetProgram(), "frag_Material_Ka");
		int kdLoc = glGetUniformLocation(shader.GetProgram(), "frag_Material_Kd");
		int ksLoc = glGetUniformLocation(shader.GetProgram(), "frag_Material_Ks");
		int nsLoc = glGetUniformLocation(shader.GetProgram(), "frag_Material_a");
		int illumLoc = glGetUniformLocation(shader.GetProgram(), "frag_Material_illum");
		for (const auto &a: drawGroups)
		{
			shared_ptr<Material> material = a.first;
			size_t indexes_size = a.second.size() * sizeof(glm::ivec3);
			Material::Coefficients vKa = material->GetKa();
			Material::Coefficients vKd = material->GetKd();
			Material::Coefficients vKs = material->GetKs();
			double ns = material->GetNs();
			int illum = material->GetIllumination();
			glm::vec4 Ka = glm::vec4(vKa[0],  vKa[1],   vKa[2],  1.0f);
			glm::vec4 Kd = glm::vec4(vKd[0],  vKd[1],   vKd[2],  1.0f);
			glm::vec4 Ks = glm::vec4(vKs[0],  vKs[1],   vKs[2],  1.0f);
			glUniform4fv(kaLoc, 1, glm::value_ptr(Ka));
			glUniform4fv(kdLoc, 1, glm::value_ptr(Kd));
			glUniform4fv(ksLoc, 1, glm::value_ptr(Ks));
			glUniform1f(nsLoc, ns);
			glUniform1i(illumLoc, illum);
			// Draw Elements (Triangles)
			glDrawElements(GL_TRIANGLES, indexes_size, GL_UNSIGNED_INT, (void*)size_processed);
			size_processed += indexes_size;
		}

		// Set active Texture Unit 0
		glActiveTexture(GL_TEXTURE0);

		// Unbind Texture Map
		glBindTexture(GL_TEXTURE_2D, 0);
		// Swap the back and front buffers
		oglw.SwapBuffers();

		// Poll window events
		glfwPollEvents();
		//break;
	}
	return 0;
}
