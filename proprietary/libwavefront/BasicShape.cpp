/**
 * @file: BasicShape.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <BasicShape.h>

#include <stdio.h>

namespace OpenGLWrapper
{

BasicShape::BasicShape()
	: mSize(1.0f)
	, mDensity1(100)
	, mDensity2(100)
{
	mColor[0] = 0.0f;
	mColor[1] = 0.0f;
	mColor[2] = 0.0f;
	mStartingPoint[0] = 0.0f;
	mStartingPoint[1] = 0.0f;
	mStartingPoint[2] = 0.0f;
}

BasicShape::~BasicShape()
{
	mBuffer.clear();
	mIndexes.clear();
}

void BasicShape::SetColor(const GLfloat r, GLfloat g, GLfloat b)
{
	mColor[0] = r;
	mColor[1] = g;
	mColor[2] = b;
}

void BasicShape::SetStartingPoint(const GLfloat x, const GLfloat y, const GLfloat z)
{
	mStartingPoint[0] = x;
	mStartingPoint[1] = y;
	mStartingPoint[2] = z;
}

void BasicShape::SetSize(const GLfloat size)
{
	mSize = size;
}

void BasicShape::SetDensity(const GLint density1, const GLint density2)
{
	mDensity1 = density1;
	mDensity2 = density2;
}

};
