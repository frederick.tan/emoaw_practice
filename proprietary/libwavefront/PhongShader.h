/**
 * @file: PhongShader.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_PHONG_SHADER_H__
#define __OPENGLWRAPPER_PHONG_SHADER_H__

#include <Shader.h>

namespace OpenGLWrapper
{

class PhongShader : public Shader
{
public:
	PhongShader();
	virtual ~PhongShader();
private:
	virtual void SetAttributes(int offsetNormal = 4, int offsetTexture = 8);
private:
	GLuint mPositionLocation;
	GLuint mNormalLocation;
};

};
#endif
