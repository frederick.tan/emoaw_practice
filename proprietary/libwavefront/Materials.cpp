/*
 * Materials.cpp
 *
 *  Created on: Mon May  7 22:29:38 NZST 2018
 *      Author: Frederick Tan
 */

#include <Materials.h>
#include <Material.h>

#include <algorithm>
#include <fstream>
#include <iostream>

using namespace std;

const std::string Materials::Keys[] = {"newmtl", "ka", "kd", "ks", "illum", "Ns"};

Materials::Materials(const std::string &fileName)
{
	Parse(fileName);
};

bool Materials::Parse(const std::string &fileName)
{
	ifstream file;
	file.open(fileName);

	if (!file.is_open()) return false;

	string line;
	bool done = false;
	while (!done)
	{
		if (!getline(file, line)) break;
		if (line.empty()) continue;

		size_t pos = line.find(' ');
		if (pos == std::string::npos) continue;
		string key = line.substr(0, pos);
		line.erase(0, pos + 1);
		std::transform(key.begin(), key.end(), key.begin(), ::tolower);
		if (key == "newmtl")
		{
			std::shared_ptr<Material> material(new Material);
			string name = line;
			while (true)
			{
				if (!getline(file, line)) break;	
				if (line.empty()) break;
				if (!material->Parse(line)) break;
			}
			mMaterials[name] = material;
		}
	}

	return true;
}

Materials::~Materials()
{
};

void Materials::Print()
{
	for (auto &a : mMaterials)
	{
		cout << a.first << ": " << endl;
		a.second->Print();
	}
}

std::shared_ptr<Material> Materials::GetMaterial(const std::string &name)
{
	map<string, shared_ptr<Material>>::iterator it;

	it = mMaterials.find(name);

	if (it == mMaterials.end())
	{
		return nullptr;
	}
	else
	{
		return it->second;
	}
}

