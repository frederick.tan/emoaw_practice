/**
 * @file: SceneStager.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_SCENE_STAGER_H__
#define __OPENGLWRAPPER_SCENE_STAGER_H__

#include <opengl_platform_headers.h>

#include <vector>

namespace OpenGLWrapper
{

class BasicShape;

class SceneStager
{
public:
	typedef std::vector<BasicShape*> BasicShapeList;
public:
	SceneStager();
	virtual ~SceneStager();
	GLsizeiptr DataSize();
	const GLvoid* Data(); 
	GLsizeiptr IndexesSize();
	const GLvoid* Indexes(); 
	const BasicShapeList& Objects() { return mShapes; };
protected:
	virtual bool CreateScene() = 0;
protected:
	BasicShapeList mShapes;
};

};

#endif
