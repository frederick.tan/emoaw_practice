// OpenGL 3.3
#version 330

// Input from Vertex Shader
in vec4 frag_Position;
in vec4 frag_Normal;
in vec4 frag_Light_Direction;
in vec4 frag_UV;
uniform int frag_Use_Texture = 0;

// Material
uniform vec4 frag_Material_Ka;
uniform vec4 frag_Material_Kd;
uniform vec4 frag_Material_Ks;
uniform float frag_Material_a;
uniform int frag_Material_illum;

// Texture
uniform sampler2D u_texture_Map;

// Output from Fragment Shader
out vec4 pixel_Colour;

// Light Source
uniform vec4 Ia = vec4(1.0f, 1.0f, 1.0f, 1.0f);
uniform vec4 Id = vec4(1.0f, 1.0f, 1.0f, 1.0f);
uniform vec4 Is = vec4(1.0f, 1.0f, 1.0f, 1.0f);

void main () {
	//----------------------------------------------
	// Phong Reflection Model
	//----------------------------------------------

	// ---------- Calculate Vectors ----------
	// Direction to Light (normalised)
	vec4 l = normalize(-frag_Light_Direction);

	// Surface Normal (normalised)
	vec4 n = normalize(frag_Normal);

	// Reflected Vector
	vec4 r = reflect(-l, n);

	// View Vector
	vec4 v = normalize(-frag_Position);

	// ---------- Calculate Terms ----------
	// Ambient Term
	vec4 Ta = frag_Material_Ka * Ia * frag_Material_illum;

	// Diffuse Term
	vec4 Td = frag_Material_Kd * max(dot(l, n), 0.0) * Id;

	// Specular Term
	vec4 Ts = frag_Material_Ks * pow((max(dot(r, v), 0.0)), frag_Material_a) * Is;

	//----------------------------------------------
	// Fragment Colour
	//----------------------------------------------
	if (frag_Use_Texture == 1)
	{
		pixel_Colour = texture(u_texture_Map, frag_UV.xy);
	}
	else
	{
		pixel_Colour = Ta + Td + Ts;
	}
}
