/**
 * @file: PhongShader.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <PhongShader.h>

#include <iostream>
#include <fstream>

using namespace std;

namespace OpenGLWrapper
{

PhongShader::PhongShader()
	: Shader("./shader/fred.vert.glsl", "", "", "", "./shader/fred.frag.glsl")
{
}

PhongShader::~PhongShader()
{
}

void PhongShader::SetAttributes(int offsetNormal, int offsetTexture)
{
	// Get Position Attribute location (must match name in shader)
	GLuint positionLoc = glGetAttribLocation(mProgram, "vert_Position");
	// Get Normal Attribute location (must match name in shader)
	GLuint normalLoc = glGetAttribLocation(mProgram, "vert_Normal");
	// Get Texture Attribute location (must match name in shader)
	GLuint textureLoc = glGetAttribLocation(mProgram, "vert_UV");
	// Set Vertex Attribute Pointers
	glVertexAttribPointer(positionLoc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);
	glVertexAttribPointer(normalLoc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(offsetNormal));
	glVertexAttribPointer(textureLoc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(offsetTexture));
	// Enable Vertex Attribute Arrays
	glEnableVertexAttribArray(positionLoc);
	glEnableVertexAttribArray(normalLoc);
	glEnableVertexAttribArray(textureLoc);
}

};
