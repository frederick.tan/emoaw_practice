/**
 * File: Main.cpp
 * Description: Main file
 * Created by: Frederick tan
 */

#include <ObjectCounter.h>
#include <MedianFilter.h>
#include <Thresholding.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <chrono>
#include <ctime>
#include <cmath>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

#include <string.h>
#include <unistd.h>

using namespace std;
using namespace cv;
using namespace chrono;

static const int DefaultBgThreshold=50;

void Usage(const char *appName)
{
	cout << appName << "[-sbh] <filename>" << endl;
}

int main(int argc, char *argv[])
{
	system_clock::time_point start;
	system_clock::time_point end;
	Mat image, imageGray, imageMedian, imageTh, imageProcessed;
	VideoCapture videoCapture;
	int bgThreshold = DefaultBgThreshold;
	enum EState
	{
		sProcessParam,
		sConfigureVideo,
		sProcessStillImage,
		sCaptureFrame,
		sConvertToGrayScale,
		sDoMedianFilter,
		sDoThresholding,
		sDoObjectCounting,
		sDisplayImages,
		sReadKey,
		sEnd
	};
	enum EMode
	{
		mLiveVideo,
		mStillImage
	};
	EState state = sProcessParam;
	EMode mode = mStillImage;
	bool startProcessing = false;
	bool processedImageAvailable = false;
	bool done = false;
	bool silentMode = false;
	while (!done)
	{
		switch (state)
		{
		case sProcessParam:
		{
			if (argc == 1)
			{
				state = sConfigureVideo;
				mode = mLiveVideo;
				bgThreshold = 180;
			}
			else
			{
				char c;
				while ((c = getopt(argc, argv, "sb:h")) != -1)
				{
					switch (c)
					{
					case 's':
						silentMode = true;
						break;
					case 'h':
						Usage(argv[0]);
						return 0;
					case 'b':
						bgThreshold = (int)atol(optarg);
						break;
					}
				}
				state = sProcessStillImage;
				mode = mStillImage;
			}
			break;
		}
		case sProcessStillImage:
		{
			// Read as grayscale
			start = system_clock::now();
			image = imread(argv[optind], 0);
			image.copyTo(imageGray);
			state = sDoMedianFilter;
			startProcessing = true;
			break;
		}
		case sConfigureVideo:
		{
			videoCapture.open(0);
			if (!videoCapture.isOpened())
			{
				cout << "Failed to open camera" << endl;
				return -1;
			}
			videoCapture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
			videoCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
			state = sCaptureFrame;
			startProcessing = false;
			break;
		}
		case sCaptureFrame:
		{
			videoCapture >> image;
			state = sConvertToGrayScale;
			start = system_clock::now();
			break;
		}
		case sConvertToGrayScale:
		{
			cv::cvtColor(image, imageGray, CV_BGR2GRAY);
			state = sDoMedianFilter;
			break;
		}
		case sDoMedianFilter:
		{
			MedianFilter filter(imageGray);
			filter.Apply();
			filter.GetFilteredImage().copyTo(imageMedian);
			state = sDoThresholding;
			break;
		}
		case sDoThresholding:
		{
			Thresholding filter(imageMedian, bgThreshold);
			filter.Apply();
			filter.GetFilteredImage().copyTo(imageTh);
			if (startProcessing) state = sDoObjectCounting;
			else state = sDisplayImages;
			break;
		}
		case sDoObjectCounting:
		{
			imageTh.copyTo(imageProcessed);
			int count = ObjectCounter::Count(imageProcessed);
			char textToDisplay[100];
			end = system_clock::now();
			double seconds = duration_cast<microseconds>(end - start).count();
			sprintf(textToDisplay, "%d (ts: %0.2f s) (fps: %0.2f fps)", count, seconds/1000000.00, 1000000.00/seconds);
			putText(imageProcessed, textToDisplay, Point(10, 30), FONT_HERSHEY_SIMPLEX, 1, 255);
			processedImageAvailable = true;
			state = sDisplayImages;
			break;
		}
		case sDisplayImages:
		{
			if (!silentMode)
			{
				imshow("Raw", image);
				imshow("GrayScale", imageGray);
				imshow("+Median Filter", imageMedian);
				imshow("+Thresholding", imageTh);
				if (processedImageAvailable) imshow("Processed", imageProcessed);
			}
			state = sReadKey;
			break;
		}
		case sReadKey:
		{
			switch (waitKey(1))
			{
				case 's':
					startProcessing = startProcessing ? false : true;
					break;
				case 'x':
					done = true;
					break;
			}
			state = (mode == mStillImage) ? sEnd : sCaptureFrame;
			break;
		}
		default:
			done = true;
			waitKey(0);
			break;
		}
	}
	if (mode == mStillImage)
	{
		system("mkdir -p result");
		char newFileName[100];
		sprintf(newFileName, "result/%s", basename(argv[optind]));
		imwrite(newFileName, imageProcessed);
	}
	return 0;
}
