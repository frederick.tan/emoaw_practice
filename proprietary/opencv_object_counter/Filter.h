/**
 * File: Filter.h
 * Description: Base Class for Filter
 * Created by: Frederick Tan 
 **/
#ifndef _FILTER_H_
#define _FILTER_H_

#include <opencv2/opencv.hpp>
//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

class Filter
{
public:
	Filter(const cv::Mat &image)
	{
		mFilteredImage.create(image.size(), CV_8UC1);
		image.copyTo(mFilteredImage);
	};
	virtual ~Filter() { mFilteredImage.release(); };
	virtual void Apply() = 0;
	const cv::Mat &GetFilteredImage() { return mFilteredImage; };
protected:
	cv::Mat mFilteredImage;
};

#endif
