/**
 * File: ObjectCounter.cpp
 * Description: Counts objects in an image
 * Assumption is that the background is black
 * Created by: Frederick Tan 
 **/

#include <ObjectCounter.h>
#include <MedianFilter.h>
#include <Thresholding.h>

#include <chrono>
#include <ctime>

using namespace std;
using namespace cv;

static const int DefaultMaxShadowBetweenObjects = 7;

ObjectCounter::ObjectCounter()
{
}

ObjectCounter::~ObjectCounter()
{
}

// Input should be in gray scale
int ObjectCounter::Count(Mat &image)
{
	int counter = -1;
	Mat A(image.rows, image.cols, CV_32SC1);
	vector< set< pair<int, int> > > SET;

	A = Scalar::all(-1);

	int totalPixelCount = image.cols * image.rows;
	int nonZeroPixelPct = (countNonZero(image) * 100) / totalPixelCount;

	//cout << "Non Zero: " << countNonZero(image) << endl;
	//cout << "Total Pixel: " << totalPixelCount << endl;
	//cout << "Object Density: " << nonZeroPixelPct << "%" << endl;

	int maxShadowBetweenObjects = nonZeroPixelPct < 5 ? DefaultMaxShadowBetweenObjects : 5;

	for (int y = maxShadowBetweenObjects; y < image.rows; y++)
	{
		for (int x = maxShadowBetweenObjects; x < image.cols; x++)
		{
			uchar pixel = image.at<uchar>(y, x);
			if (pixel > 0)
			{
				bool hasASet = false;
				for (int j = 0; j < maxShadowBetweenObjects && !hasASet; j++)
				{
					for (int i = 0; i < maxShadowBetweenObjects && !hasASet; i++)
					{
						if ((j == 0) && (i == 0)) continue;
						hasASet = A.at<int>(y - j, x - i) > 0 ? true : false;
					} 
				}
				if (!hasASet)
				{
						hasASet = A.at<int>(y - 1, x + 1) > 0 ? true : false;
				}
				if (hasASet)
				{
					std::vector<int> sets;
					int setId = A.at<int>(y - 1, x + 1);
					if (setId != -1)
					{
						sets.push_back(setId);
						SET[setId].insert(make_pair(x, y));
						A.at<int>(y, x) = setId;
					}
					for (int j = 0; j < maxShadowBetweenObjects; j++)
					{
						for (int i = 0; i < maxShadowBetweenObjects; i++)
						{
							if ((j == 0) && (i == 0)) continue;
							int setId = A.at<int>(y - j, x - i);
							if (setId == -1) continue;
							sets.push_back(setId);
							SET[setId].insert(make_pair(x, y));
							A.at<int>(y, x) = setId;
						} 
					}
					for (auto a : sets)
					{
						for (auto b : sets)
						{
							if (a == b) continue;
							SET[a].insert(SET[b].begin(), SET[b].end());
							for (auto c : SET[b])
							{
								A.at<int>(c.second, c.first) = a;
							}
							SET[b].clear();
						}
					}
				}
				else
				{
					counter++;
					set< pair<int, int> > nSet;
					nSet.insert(make_pair(x, y));
					SET.push_back(nSet);
					A.at<int>(y, x) = counter;
				}
			}
		}
	}
	int objectCount = 0;
	char objectCountText[20];
	for (auto a : SET)
	{
		if (a.size() >= 50)
		{
			objectCount++;
			//printf("%d\n", (int)a.size());
			sprintf(objectCountText, "%d", objectCount);
			putText(image, objectCountText, Point(a.begin()->first, a.begin()->second), FONT_HERSHEY_SIMPLEX, 1, 255);
		}
	}
	A.release();
	return objectCount;
}
