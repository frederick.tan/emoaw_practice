/**
 * File: MedianFilter.cpp
 * Description: Class Implementing the Median Filter
 * Created by: Frederick Tan 
 **/

#include <MedianFilter.h>
#include <QuickSort.h>

#include <vector>

using namespace cv;

MedianFilter::MedianFilter(const Mat &image, EBorderValuesMethod method)
	: Filter(image)
	, mBorderValuesMethod(method)
{
}
MedianFilter::~MedianFilter()
{
}
void MedianFilter::Apply()
{
	Mat tempImage;
	int start = (mBorderValuesMethod == bvmUnchanged) ? 1 : 0;

	tempImage.create(mFilteredImage.size(), CV_8UC1);
	mFilteredImage.copyTo(tempImage);

	for (int y = start; y < mFilteredImage.rows; y++)
	{
		for (int x = start; x < mFilteredImage.cols; x++)
		{
			QuickSort sort;
			uchar pixel1 = mFilteredImage.at<uchar>(y - 1, x - 1);
			uchar pixel2 = mFilteredImage.at<uchar>(y - 1, x);
			uchar pixel3 = mFilteredImage.at<uchar>(y - 1, x + 1);
			uchar pixel4 = mFilteredImage.at<uchar>(y, x - 1);
			uchar pixel5 = mFilteredImage.at<uchar>(y, x);
			uchar pixel6 = mFilteredImage.at<uchar>(y, x + 1);
			uchar pixel7 = mFilteredImage.at<uchar>(y + 1, x - 1);
			uchar pixel8 = mFilteredImage.at<uchar>(y + 1, x);
			uchar pixel9 = mFilteredImage.at<uchar>(y + 1, x + 1);
			sort.AddObject(pixel1);
			sort.AddObject(pixel2);
			sort.AddObject(pixel3);
			sort.AddObject(pixel4);
			sort.AddObject(pixel5);
			sort.AddObject(pixel6);
			sort.AddObject(pixel7);
			sort.AddObject(pixel8);
			sort.AddObject(pixel9);
			sort.Sort();
			std::vector<int> kernel = sort.GetObjectList();
			tempImage.at<uchar>(y, x) = kernel[4];
		}
	}
	tempImage.copyTo(mFilteredImage);
}

