/**
 * File: QuickSort.cpp
 * Description: Sorts numbers
 * Created by: Frederick Tan 
 **/

#include <QuickSort.h>

#include <iostream>

using namespace std;

QuickSort::QuickSort()
{
}

QuickSort::~QuickSort()
{
}

bool QuickSort::Sort(std::vector<int> &vect, int m, int n)
{
	int key;
	int i, j, k;

	if (m < n)
	{
		k = SelectPivot(m, n);
		Swap(vect, m, k);
		key = vect[m];
		i = m + 1;
		j = n;
		while (i <= j)
		{
			while((i <= n) && (vect[i] <= key)) i++;
			while((j >= m) && (vect[j] > key)) j--;
			if (i < j) Swap(vect, i, j);
		}
		Swap(vect, m, j);
		Sort(vect, m, j - 1);
		Sort(vect, j + 1, n);
	}

	return true;
}
