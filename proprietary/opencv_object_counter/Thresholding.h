/**
 * File: Thresholding.h
 * Description: Class Implementing the Median Filter
 * Created by: Frederick Tan 
 **/
#ifndef _THRESHOLDING_H_
#define _THRESHOLDING_H_

#include <Filter.h>
#include <opencv2/opencv.hpp>

class Thresholding : public Filter
{
public:
	Thresholding(const cv::Mat &image, uchar threshold = 128);
	virtual ~Thresholding();
	virtual void Apply();
private:
	uchar mThreshold;
};

#endif
