/**
 * File: MedianFilter.h
 * Description: Class Implementing the Median Filter
 * Created by: Frederick Tan 
 **/
#ifndef _MEDIANFILTER_H_
#define _MEDIANFILTER_H_

#include <Filter.h>
#include <opencv2/opencv.hpp>

class MedianFilter : public Filter
{
public:
	enum EBorderValuesMethod
	{
		bvmUnchanged,
		bvmExtendWithDuplicates,
		bvmExtendWithZeroes
	};
public:
	MedianFilter(const cv::Mat &image, EBorderValuesMethod method = bvmUnchanged);
	virtual ~MedianFilter();
	virtual void Apply();
private:
	EBorderValuesMethod mBorderValuesMethod;
};

#endif
