/**
 * File: Thresholding.cpp
 * Description: Class Implementing the Median Filter
 * Created by: Frederick Tan 
 **/

#include <Thresholding.h>

using namespace cv;

Thresholding::Thresholding(const Mat &image, uchar threshold)
	: Filter(image)
	, mThreshold(threshold)
{
}
Thresholding::~Thresholding()
{
}
void Thresholding::Apply()
{
	Mat tempImage;

	tempImage.create(mFilteredImage.size(), CV_8UC1);
	mFilteredImage.copyTo(tempImage);

	for (int y = 0; y < mFilteredImage.rows; y++)
	{
		for (int x = 0; x < mFilteredImage.cols; x++)
		{
			uchar pixel = mFilteredImage.at<uchar>(y, x);
			tempImage.at<uchar>(y, x) = (pixel > mThreshold) ? pixel : 0;
		}
	}
	tempImage.copyTo(mFilteredImage);
}

