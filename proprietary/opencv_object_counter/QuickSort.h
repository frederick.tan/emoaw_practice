/**
 * File: QuickSort.h
 * Description: Sorts numbers
 * Created by: Frederick Tan 
 **/
#ifndef _LIBSORT_QUICKSORT_H_
#define _LIBSORT_QUICKSORT_H_

#include <vector>

class QuickSort
{
public:
	QuickSort();
	virtual ~QuickSort();
	void AddObject(int object) { mObjectList.push_back(object); };
	bool Sort() { return Sort(mObjectList, 0, mObjectList.size() - 1); };
	std::vector<int>& GetObjectList() { return mObjectList; };
private:
	void Swap(std::vector<int> &vect, int i, int j) { int temp = vect[i]; vect[i] = vect[j]; vect[j] = temp; };
	int SelectPivot(int i, int j)
	{
		return ((i + j) / 2);
	};
	bool Sort(std::vector<int> &vect, int m, int n);
private:
	std::vector<int> mObjectList;
};

#endif
