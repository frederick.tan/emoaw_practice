/**
 * File: ObjectCounter.h
 * Description: Counts objects in an image
 * Assumption is that the background is black
 * Created by: Frederick Tan 
 **/
#ifndef _OBJECT_COUNTER_H_
#define _OBJECT_COUNTER_H_

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class ObjectCounter
{
public:
	ObjectCounter();
	virtual ~ObjectCounter();
	static int Count(cv::Mat &image);
};

#endif
