/*
 * elliptic_fourier_descriptors.cpp
 *
 *  Created on: Sun Jun 17 15:31:49 NZST 2018
 *      Author: Frederick Tan
 */

#include <elliptic_fourier_descriptors.h>

using namespace std;
using namespace cv;

EllipticFourierDescriptors::EllipticFourierDescriptors()
{
};

EllipticFourierDescriptors::~EllipticFourierDescriptors()
{
};

void EllipticFourierDescriptors::Describe(const vector<Point>& contour, vector<float>& CE)
{
    vector<float> ax, ay, bx, by;
    const int n = 20;
    int m = contour.size();
    float t = (2 * CV_PI) / m;
    for (int k = 0; k < n; k++)
    {
        ax.push_back(0.0);
        ay.push_back(0.0);
        bx.push_back(0.0);
        by.push_back(0.0);
        for (int i = 0; i < m; i++)
        {
            ax[k] = ax[k] + contour[i].x * cos((k + 1) * t * i);
            bx[k] = bx[k] + contour[i].x * cos((k + 1) * t * i);
            ay[k] = ay[k] + contour[i].x * cos((k + 1) * t * i);
            by[k] = by[k] + contour[i].x * cos((k + 1) * t * i);
        }
        ax[k] = (ax[k]) / m;
        bx[k] = (bx[k]) / m;
        ay[k] = (ay[k]) / m;
        by[k] = (by[k]) / m;
    }
    for (int k = 0; k < n; k++)
    {
        CE.push_back(sqrt((ax[k] * ax[k]+ ay[k] * ay[k]) / (ax[0] * ax[0] + ay[0] * ay[0])) +
                     sqrt((bx[k] * bx[k]+ by[k] * by[k]) / (bx[0] * bx[0] + by[0] * by[0])));
    }
    //for (int count = 0; count < n && count < (int)CE.size(); count++)
    //{
    //    printf("%d CE: %f ax: %f ay: %f bx: %f by: %f\n", count, CE[count], ax[count], ay[count], bx[count], by[count]);
    //}
}