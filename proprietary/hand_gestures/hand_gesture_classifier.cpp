/*
 * hand_gesture_classifier.cpp
 *
 *  Created on: Sun Jun 17 22:46:34 NZST 2018
 *      Author: Frederick Tan
 */

#include <hand_gesture_classifier.h>
#include <elliptic_fourier_descriptors.h>

#include <sys/types.h>
#include <dirent.h>

using namespace std;
using namespace cv;
using namespace cv::ml;

std::map<std::string, int> TrainingSetData::ClassId = {
    {"0", 0}, {"1", 1}, {"2", 2}, {"3", 3}, {"4", 4}, {"5", 5}, {"6", 6},
    {"7", 7}, {"8", 8}, {"9", 9}, {"a",10}, {"b",11}, {"c",12}, {"d",13},
    {"e",14}, {"f",15}, {"g",16}, {"h",17}, {"i",18}, {"j",19}, {"k",20},
    {"l",21}, {"m",22}, {"n",23}, {"o",24}, {"p",25}, {"q",26}, {"r",27},
    {"s",28}, {"t",29}, {"u",30}, {"v",31}, {"w",32}, {"x",33}, {"y",34},
    {"z",35}};

HandGestureClassifier::HandGestureClassifier(const std::string& classifierDataFile)
	: mClassifierIsReady(false)
{
	mPredictionModel = StatModel::load<ANN_MLP>(classifierDataFile);
	if (!mPredictionModel.empty()) mClassifierIsReady = true;
}

HandGestureClassifier::HandGestureClassifier(const std::string& imagesLocation, const std::string& saveToFilename)
	: mDirectory(imagesLocation)
	, mSaveToFilename(saveToFilename)
	, mPredictionModel(ANN_MLP::create())
	, mClassifierIsReady(false)
{
	BuildTrainingSet();
	BuildClassifier();
	mClassifierIsReady = true;
};

HandGestureClassifier::~HandGestureClassifier()
{
};

bool HandGestureClassifier::BuildTrainingSet()
{
    DIR* dirp = opendir(mDirectory.c_str());

    if (dirp == NULL) return false;

    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL)
    {
        std::string filename = dp->d_name;
        size_t pos = filename.find("_");
        if (pos == std::string::npos) continue;
        std::string className = filename.substr(pos + 1);
        pos = className.find("_");
        if (pos == std::string::npos) continue;
        className.erase(pos);
        //printf("filename: %s class: %s\n", filename.c_str(), className.c_str());
        Mat image = imread((mDirectory + "/" + filename).c_str());
        cvtColor(image, image, CV_BGR2GRAY);
        threshold(image, image, 5, 255, CV_THRESH_BINARY);
        vector<vector<Point>> contours;
        findContours(image, contours, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);
        sort(contours.begin(), contours.end(), [](vector<Point>x, vector<Point>y){return x.size() < y.size();});
        int largestcontour = contours.size() - 1;
        TrainingSetData data(className);
        EllipticFourierDescriptors::Describe(contours[largestcontour], data.mCEs);
        data.mCEs.erase(data.mCEs.begin());
        data.mCEs.push_back((float)contours.size());
        mTSData.push_back(data);
    }
    closedir(dirp);
    return true;
}

inline TermCriteria TC(int iters, double eps)
{
    return TermCriteria(TermCriteria::MAX_ITER + (eps > 0 ? TermCriteria::EPS : 0), iters, eps);
}

void HandGestureClassifier::BuildClassifier()
{
    const int classCount = TrainingSetData::ClassId.size();
    const int nSamples = mTSData.size();
    const int columnSize = mTSData.at(0).mCEs.size();
    printf("Number of Samples: %d x %d\n", nSamples, columnSize);
    Mat data(nSamples, columnSize, CV_32F);
    for(int i = 0; i < nSamples; ++i)
        for(int j = 0; j < columnSize; ++j)
            data.at<float>(i, j) = mTSData.at(i).mCEs.at(j);
    
    Mat trainData = data.rowRange(0, nSamples);
    //Mat trainResponses = Mat::zeros(nSamples, 1, CV_32F);
    Mat trainResponses = Mat::zeros(nSamples, classCount, CV_32F);
    // 1. unroll the responses
    for(int i = 0; i < nSamples; i++ )
    {
        int classLabel = mTSData.at(i).mClassId;
        //cout << "labels " << classLabel << endl;
        trainResponses.at<float>(i, classLabel) = 1.f;
    }
    int layer_sz[] = { columnSize, 180, 240, 180, 180, classCount };
    int nlayers = (int)(sizeof(layer_sz)/sizeof(layer_sz[0]));
    Mat layer_sizes(1, nlayers, CV_32S, layer_sz);
    Ptr<TrainData> tdata = TrainData::create(trainData, ROW_SAMPLE, trainResponses);
    mPredictionModel = ANN_MLP::create();
    mPredictionModel->setLayerSizes(layer_sizes);
    mPredictionModel->setActivationFunction(ANN_MLP::SIGMOID_SYM, 0, 0);
    mPredictionModel->setTermCriteria(TC(11700,0));
    mPredictionModel->setTrainMethod(ANN_MLP::BACKPROP, 0.001);
    mPredictionModel->train(tdata);
            
    cout << "Calculating prediction capability of this model..." << endl;

    int correctPrediction = 0;
    for (int i = 0; i < nSamples; i++)
    {
	Mat sample = data.row(i);
	float r = mPredictionModel->predict( sample );
	if( (int)r == (int)(mTSData.at(i).mClassId) ) //prediction is correct
            correctPrediction++;
    }
    float resultPercentage = correctPrediction * 100.0 / nSamples;
    cout << "Result: " << correctPrediction << " / " << nSamples << " = " << resultPercentage << endl;
    if (!mSaveToFilename.empty()) mPredictionModel->save(mSaveToFilename);
}

string HandGestureClassifier::Interpret(const Mat& image)
{
    Mat processImage;
    image.copyTo(processImage);
    cvtColor(processImage, processImage, CV_BGR2GRAY);
    threshold(processImage, processImage, 5, 255, CV_THRESH_BINARY);
    vector<vector<Point>> contours;
    findContours(processImage, contours, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);
    sort(contours.begin(), contours.end(), [](vector<Point>x, vector<Point>y){return x.size() < y.size();});
        
    vector<float> ces;
    EllipticFourierDescriptors::Describe(contours[contours.size() - 1], ces);
    ces.erase(ces.begin());
    cout << "CSize: " << contours.size() << endl;
    ces.push_back((float)contours.size()); // Best at 3.8889 %
    Mat sample = (Mat_<float>(1,20) << ces[0], ces[1], ces[2], ces[3], ces[4], ces[5], ces[6], ces[7],
                  ces[8], ces[9], ces[10], ces[11], ces[12], ces[13], ces[14], ces[15],
                  ces[16], ces[17], ces[18], ces[19]);
    float foundId = mPredictionModel->predict(sample);
    
    cout << "Found: " << foundId << endl;
    auto it = std::find_if(TrainingSetData::ClassId.begin(), TrainingSetData::ClassId.end(),
		[&](auto&& p) { return p.second == foundId;});
    if (it == TrainingSetData::ClassId.end())
	return {};

    return it->first;
}
