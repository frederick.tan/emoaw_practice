/*
 * training_set.h
 *
 *  Created on: Sun Jun 17 22:46:34 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef TRAINING_SET_H
#define TRAINING_SET_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>

#include <memory>
#include <string>
#include <vector>

class TrainingSetData
{
public:
    TrainingSetData(const std::string& className) : mClassId(TrainingSetData::ClassId[className]) {};
    ~TrainingSetData() {};
    void AddCE(float ce) { mCEs.push_back(ce); };
public:
    static std::map<std::string, int> ClassId;
public:
    const int mClassId;
    std::vector<float> mCEs;
};

class TrainingSet
{
public:
    TrainingSet(const std::string& directory);
    virtual ~TrainingSet();
    bool Build();
    bool SaveTo(const std::string& fileName);
    bool BuildClassifier();
    int Predict(std::vector<float> ceList);
private:
    const std::string mDirectory;
    std::vector<TrainingSetData> mTSData;
    cv::Ptr<cv::ml::ANN_MLP> mPredictionModel;
};
#endif
