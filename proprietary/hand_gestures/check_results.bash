#!/bin/bash

cat results$1.log | grep Result | awk -F"=" '{print $2}' | sort -n
