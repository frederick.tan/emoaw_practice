/*
 * training_set.cpp
 *
 *  Created on: Sun Jun 17 22:46:34 NZST 2018
 *      Author: Frederick Tan
 */

#include <training_set.h>
#include <elliptic_fourier_descriptors.h>

#include <sys/types.h>
#include <dirent.h>

using namespace std;
using namespace cv;
using namespace cv::ml;

std::map<std::string, int> TrainingSetData::ClassId = {
    {"0", 0}, {"1", 1}, {"2", 2}, {"3", 3}, {"4", 4}, {"5", 5}, {"6", 6},
    {"7", 7}, {"8", 8}, {"9", 9}, {"a",10}, {"b",11}, {"c",12}, {"d",13},
    {"e",14}, {"f",15}, {"g",16}, {"h",17}, {"i",18}, {"j",19}, {"k",20},
    {"l",21}, {"m",22}, {"n",23}, {"o",24}, {"p",25}, {"q",26}, {"r",27},
    {"s",28}, {"t",29}, {"u",30}, {"v",31}, {"w",32}, {"x",33}, {"y",34},
    {"z",35}};

TrainingSet::TrainingSet(const std::string& directory)
    : mDirectory(directory)
    , mPredictionModel(ANN_MLP::create())
{
};

TrainingSet::~TrainingSet()
{
};

bool TrainingSet::Build()
{
    DIR* dirp = opendir(mDirectory.c_str());

    if (dirp == NULL) return false;

    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL)
    {
        std::string filename = dp->d_name;
        size_t pos = filename.find("_");
        if (pos == std::string::npos) continue;
        std::string className = filename.substr(pos + 1);
        pos = className.find("_");
        if (pos == std::string::npos) continue;
        className.erase(pos);
        //printf("filename: %s class: %s\n", filename.c_str(), className.c_str());
        Mat image = imread((mDirectory + "/" + filename).c_str());
        cvtColor(image, image, CV_BGR2GRAY);
        threshold(image, image, 5, 255, CV_THRESH_BINARY);
        vector<vector<Point>> contours;
        findContours(image, contours, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);
        sort(contours.begin(), contours.end(), [](vector<Point>x, vector<Point>y){return x.size() < y.size();});
        
        int secondLargest = contours.size() - 2;
        int largestcontour = contours.size() - 1;
        //if (secondLargest < 0) secondLargest = largestcontour;
        //long largestsize = 0;
        //for (unsigned int i = 0; i < contours.size(); i++)
        //{
        //    if (largestsize < (long)contours[i].size())
        //    {
        //        largestsize = contours[i].size();
        //        largestcontour = i;
        //    }
        //}
        TrainingSetData data(className);
        EllipticFourierDescriptors::Describe(contours[largestcontour], data.mCEs);
        data.mCEs.erase(data.mCEs.begin());
        std::vector<float> tempCEs;
        if (secondLargest < 0)
        {
            //printf("Debug 01: %s %d\n", filename.c_str(), (int)contours.size());
            tempCEs.insert(tempCEs.begin(), 19, -1.0f);
        }
        else
        {
            EllipticFourierDescriptors::Describe(contours[secondLargest], tempCEs);
            tempCEs.erase(tempCEs.begin());
        }
        //data.mCEs.insert(data.mCEs.end(), tempCEs.begin(), tempCEs.end());
        data.mCEs.push_back((float)contours.size()); // Best at 3.8889 %
        mTSData.push_back(data);
    }
    closedir(dirp);
    
    //for (auto data : mTSData)
    //{
    //    printf("Class Name: %d\n", data.mClassId);
    //    for (auto ce : data.mCEs)
    //    {
    //        printf("CE: %f\n", ce);
    //    }
    //}
    return true;
}

bool TrainingSet::SaveTo(const std::string& fileName)
{
    if (mTSData.empty()) return false;
    
    for (auto data : mTSData)
    {
        printf("Class Name: %d\n", data.mClassId);
        for (auto ce : data.mCEs)
        {
            printf("CE: %f\n", ce);
        }
    }
    return true;
}

inline TermCriteria TC(int iters, double eps)
{
    return TermCriteria(TermCriteria::MAX_ITER + (eps > 0 ? TermCriteria::EPS : 0), iters, eps);
}

bool TrainingSet::BuildClassifier()
{
    const int classCount = TrainingSetData::ClassId.size();
    const int nSamples = mTSData.size();
    const int columnSize = mTSData.at(0).mCEs.size();
    printf("Number of Samples: %d x %d\n", nSamples, columnSize);
    Mat data(nSamples, columnSize, CV_32F);
    for(int i = 0; i < nSamples; ++i)
        for(int j = 0; j < columnSize; ++j)
            data.at<float>(i, j) = mTSData.at(i).mCEs.at(j);
    
    Mat trainData = data.rowRange(0, nSamples);
    //Mat trainResponses = Mat::zeros(nSamples, 1, CV_32F);
    Mat trainResponses = Mat::zeros(nSamples, classCount, CV_32F);
    // 1. unroll the responses
    for(int i = 0; i < nSamples; i++ )
    {
        int classLabel = mTSData.at(i).mClassId;
        //cout << "labels " << classLabel << endl;
        trainResponses.at<float>(i, classLabel) = 1.f;
    }
    //for( int i = 0; i < nSamples; i++ )
    //{
    //    int classLabel = mTSData.at(i).mClassId;
    //    cout << "labels " << classLabel << endl;
    //    trainResponses.at<int>(i, 0) = classLabel;
    //    cout << "i, classLabel " << trainResponses.at<float>(i, 0) << endl;
    //}
    // 2. train classifier
    //int layer_sz[] = { columnSize, 20, 120, classCount }; // 4%
    // README FRED: HIGHEST SO FAR IS 240, 120, 120
    // iter = 8500
    // value = 96.3419
    // PLAN IS MILK 240, 120, 120 and find max value for innerLayer1 then go to 
    // innerLayer 2
    int innerLayerSize = 240;
    float largestResult = 0.0;
    float largestIter = 0.0;
    int largestInnerLayerSize = 0;
    while (innerLayerSize < 500)
    {
        int layer_sz[] = { columnSize, 180, innerLayerSize, 120, 160, classCount };
        cout <<  " sizeof layer_sz " << sizeof(layer_sz) << " sizeof layer_sz[0]) " << sizeof(layer_sz[0]) << endl;
        int nlayers = (int)(sizeof(layer_sz)/sizeof(layer_sz[0]));
        cout << " nlayers  " << nlayers << ": " << innerLayerSize << endl;
        Mat layer_sizes( 1, nlayers, CV_32S, layer_sz );

        //cout << "DBG" << endl;

        Ptr<TrainData> tdata = TrainData::create(trainData, ROW_SAMPLE, trainResponses);
        int method = ANN_MLP::BACKPROP;
        double method_param = 0.001;
        //int max_iter = 10500;
        int max_iter = 10900;
        //int method = ANN_MLP::RPROP;
        //double method_param = 0.1;
        //int max_iter = 1000;
        // The largest percentage is 5.288270% which was accomplished with iter 710.000000
        for (; max_iter < 12000; max_iter += 100)
        {
            cv::Ptr<cv::ml::ANN_MLP> mPredictionModel = ANN_MLP::create();
            cout << "ANN config: " << method << ", " << method_param << ", " << max_iter << endl;
            cout << "Training the classifier (may take a few minutes)...\n";
            mPredictionModel->setLayerSizes(layer_sizes);
            mPredictionModel->setActivationFunction(ANN_MLP::SIGMOID_SYM, 0, 0);
            mPredictionModel->setTermCriteria(TC(max_iter,0));
            mPredictionModel->setTrainMethod(method, method_param);
            mPredictionModel->train(tdata);
            cout << endl;
            cout << "Testing..." << endl;
            int training_correct_predict=0;
            for (int i = 0; i < nSamples; i++)
            {
                Mat sample = data.row(i);
        //cout << "Sample: " << trainResponses.at<int>(i) << " row " << data.row(i) << endl;
                float r = mPredictionModel->predict( sample );
        //cout << "Predict:  r = " << r << endl;
                if( (int)r == (int)(mTSData.at(i).mClassId) ) //prediction is correct
                  training_correct_predict++;
            }
            cout << "Result: " << training_correct_predict << "/" << nSamples << "=" << training_correct_predict * 100.0/ nSamples<< endl;
            float resultPercentage = training_correct_predict * 100.0 / nSamples;
            if (largestResult < resultPercentage)
            {
                largestResult = resultPercentage;
                largestIter = max_iter;
                largestInnerLayerSize = innerLayerSize;
            }
            //delete mPredictionModel;
        }
        innerLayerSize+=10;
    }
    printf("The largest percentage is %02f%% which was accomplished with iter %02f (inner layer count: %d)\n", largestResult, largestIter, largestInnerLayerSize);
    return true;
}

int TrainingSet::Predict(vector<float> ceList)
{
    Mat sample = (Mat_<float>(1,19) << ceList[0], ceList[1], ceList[2], ceList[3], ceList[4], ceList[5], ceList[6], ceList[7],
                                    ceList[8], ceList[9], ceList[10], ceList[11], ceList[12], ceList[13], ceList[14], ceList[15],
                                    ceList[16], ceList[17], ceList[18]);
    float foundId = mPredictionModel->predict(sample);
    
    cout << "Found: " << foundId << endl;
    //auto it = std::find_if(std::begin(pHandles), std::end(pHandles),
    //                       [](auto&& p) { return p->second == name; });

    //if (it == std::end(pHandles))
    //    return -1;

    //return it->first
            
    //return (TrainingSetData::ClassId[]);
    return foundId;
}