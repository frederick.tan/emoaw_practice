/*
 * hand_gesture_classifier.h
 *
 *  Created on: Sun Jun 17 22:46:34 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef HAND_GESTURE_CLASSIFIER_H
#define HAND_GESTURE_CLASSIFIER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>

#include <memory>
#include <string>
#include <vector>

class TrainingSetData
{
public:
    TrainingSetData(const std::string& className) : mClassId(TrainingSetData::ClassId[className]) {};
    ~TrainingSetData() {};
    void AddCE(float ce) { mCEs.push_back(ce); };
public:
    static std::map<std::string, int> ClassId;
public:
    const int mClassId;
    std::vector<float> mCEs;
};

// For now, let us fix this to ANN
class HandGestureClassifier
{
public:
	HandGestureClassifier(const std::string& classifierDataFile);
	HandGestureClassifier(const std::string& imagesLocation, const std::string& saveToFilename);
    	virtual ~HandGestureClassifier();
	std::string Interpret(const cv::Mat& image);
	bool IsReady() const { return mClassifierIsReady; };
private:
	void BuildClassifier();
	bool BuildTrainingSet();
private:
    const std::string mDirectory;
	const std::string mSaveToFilename;
    std::vector<TrainingSetData> mTSData;
    cv::Ptr<cv::ml::ANN_MLP> mPredictionModel;
	bool mClassifierIsReady;
};
#endif
