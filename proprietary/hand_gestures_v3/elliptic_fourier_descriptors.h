/*
 * elliptic_fourier_descriptors.h
 *
 *  Created on: Sun Jun 17 15:31:49 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef ELLIPTIC_FOURIER_DESCRIPTORS_H
#define ELLIPTIC_FOURIER_DESCRIPTORS_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <vector>

class EllipticFourierDescriptors
{
public:
    EllipticFourierDescriptors();
    virtual ~EllipticFourierDescriptors();
public:
    static void Describe(const std::vector<cv::Point>& contour, std::vector<float>& CE);
};
#endif
