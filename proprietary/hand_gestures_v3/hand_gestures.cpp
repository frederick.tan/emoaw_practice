/*
 * hand_gestures.cpp
 *
 *  Created on: Sun Jun 17 14:43:38 NZST 2018
 *      Author: Frederick Tan
 */

#include <hand_gesture_classifier.h>

#include <fstream>

using namespace std;
using namespace cv;
using namespace cv::ml;

static const string ClassifierFilename = "./hand_gesture.classifier";
static const string ImagesDirectory = "./images";

static const string hot_keys =
        "\n\nHot keys: \n"
        "\tESC - quit the program\n"
        "\tp - pause video\n";

static string random_messages[] = {
        "Man, I love how we celebrated the 25th anniversary of the human colony in Mars. We used up the last of our resources that time. Man, that was hilarious",
        "Man, that was a nice sleep. Oh, how was the fallout? Has it cleared already?",
        "I heard that man who created me was the first President in the Europa colony. How is he?",
        "Pffft... That's a bummer. I thought I can sleep for at least a day. Oh well.",
        "Ding! And it's done!"
};

int main(int argc, char **argv) {
    unique_ptr<HandGestureClassifier> cl;

    // Check if the model exists, else, build the cl
    if ([](const string &filename) -> bool {
        std::ifstream infile(filename);
        return infile.good();
    }(ClassifierFilename)) {
        cl = unique_ptr<HandGestureClassifier>(new HandGestureClassifier(ClassifierFilename));
        if (!cl->IsReady()) {
            cerr << "Error reading the cl data file. Possibly corrupted?" << endl;
            return -1;
        }
    } else {
        cout << "This is your first time running this application. Or perhaps the classifer data file is missing."
             << endl;
        cout << "Please wait while I build the classifier model. This may take a while..." << endl;
        cl = unique_ptr<HandGestureClassifier>(new HandGestureClassifier(ImagesDirectory, ClassifierFilename));
        if (!cl->IsReady()) {
            cerr << "Error building the cl. Perhaps there are no training images?" << endl;
            return -1;
        } else {
            cout << "Whew! That took an eternity!" << endl;
            cout << random_messages[rand() % 4] << endl;
        }
    }
    string theSecretCode;
    Mat image;
    namedWindow("Hand Gesture Reader", CV_WINDOW_AUTOSIZE);
    if (argc > 1) {
        image = imread(argv[1]);
        theSecretCode = cl->Interpret(image);
        cout << "The secret code: " << theSecretCode << endl;
        int x = image.cols / 2;
        int y = image.rows / 2;
        putText(image, theSecretCode, Point(x - 20, y + 5), FONT_HERSHEY_SIMPLEX, 4.0, Scalar(255, 0, 0), 4);
        imshow("Hand Gesture Reader", image);
        waitKey(0);
    } else {
        // Camera
        VideoCapture cap;
        cap.open(0);
        if (!cap.isOpened()) {
            cout << "***Could not initialize capturing...***\n";
            cout << "Current parameter's value: \n";
            return -1;
        }
        Mat frame;
        bool paused = true;
        bool sentenceMode = false;
        string sentence;
        cap >> frame;
        for (;;) {
            double timer = (double) getTickCount();
            cap >> frame;
            if (frame.empty())
                break;
            frame.copyTo(image);
            if (!paused) {
                theSecretCode = cl->Interpret(image);
                string codeText = "Code: ";
                if (sentenceMode) {
                    sentence += theSecretCode;
                    codeText += sentence;
                } else {
                    codeText += theSecretCode;
                }
                putText(image, codeText, Point(20, 40), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0, 255, 0), 2);
            }
            string fps = "FPS: " + to_string(getTickFrequency() / ((double) getTickCount() - timer));
            putText(image, fps, Point(20, 20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255, 0, 0), 2);
            imshow("Hand Gesture Reader", image);
            char c = (char) waitKey(10);
            if (c == 27)
                break;
            else if (c == 'p')
                paused = !paused;
            else if (c == 's')
                sentenceMode = !sentenceMode;
            else if (c == 'c')
                sentence.clear();
        }
    }
    return (0);
}
