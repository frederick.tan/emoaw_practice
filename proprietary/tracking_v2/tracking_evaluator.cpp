/*
 * tracking_evaluator.cpp
 *
 *  Created on: Thu Jun 21 15:14:25 NZST 2018
 *      Author: Frederick Tan
 */

#include <tracking_evaluator.h>

using namespace std;
using namespace cv;

TrackingEvaluator::TrackingEvaluator(vector<Rect> resultData, vector<Rect> expectedData)
    : mResultData(resultData)
    , mExpectedData(expectedData)
    , mSuccessRate(0.0)
    , mPrecisionRateOverall(0.0)
    , mPrecisionRateSuccessOnly(0.0)
{
    Evaluate();
};

TrackingEvaluator::~TrackingEvaluator()
{
};

void TrackingEvaluator::Evaluate()
{
    cout << "Calculating the success rate..." << endl;
    cout << "[Summation of (intersect(result, expected) / union(result, expected)) > 0.5 ] / N" << endl;
    auto resultIt = mResultData.begin();
    auto expectedIt = mExpectedData.begin() + 1;
    int fullError = 0;
    while (resultIt != mResultData.end())
    {
        // Is the result within the ground truth window
        int leftBound = expectedIt->x;
        int rightBound = leftBound + expectedIt->width;
        int topBound = expectedIt->y;
        int bottomBound = topBound + expectedIt->height;
        
        int x1 = resultIt->x;
        int x2 = x1 + resultIt->width;
        int y1 = resultIt->y;
        int y2 = y1 + resultIt->height;
        int areaOfExpected = (rightBound - leftBound) * (bottomBound - topBound);
        int areaOfResult = (x2 - x1) * (y2 - y1);
        double precision = areaOfExpected == 0 ? 0.0 : double(areaOfResult) / double(areaOfExpected);
        if ((rightBound <= x1) || (x2 <= leftBound) || (bottomBound <= y1) || (y2 <= topBound))
        {
            // Do nothing
            mSuccessPerFrame.push_back(0.0);
            //cout << "Success: 0.0" << endl;
            mIsSuccessfulFrame.push_back(false);
            fullError++;
        }
        else
        {
            //Area of intersection
            int intersectionX1 = (x1 > leftBound) ? x1 : leftBound;
            int intersectionX2 = (x2 < rightBound) ? x2 : rightBound;
            int intersectionY1 = (y1 > topBound) ? y1 : topBound;
            int intersectionY2 = (y2 < bottomBound) ? y2 : bottomBound;
            int areaOfIntersection = (intersectionX2 - intersectionX1) * (intersectionY2 - intersectionY1);
            
            // Area of union
            int areaOfUnion = areaOfExpected + areaOfResult - areaOfIntersection;
            double successfulGrade = double(areaOfIntersection) / double(areaOfUnion);
            //cout << "Success: " << successfulGrade << endl;
            mSuccessPerFrame.push_back(successfulGrade);
            mPrecisionPerFrame.push_back(precision);
            mIsSuccessfulFrame.push_back(successfulGrade > 0.5);
            if (successfulGrade > 0.5)
            {
                mSuccessRate++;
                mPrecisionRateSuccessOnly += precision;
            }
        }
        mPrecisionRateOverall += precision;
        resultIt++;
        expectedIt++;
    }
    double successRemovingFullError = mSuccessRate / (mSuccessPerFrame.size() - fullError);
    mPrecisionRateSuccessOnly /= mSuccessRate;
    mPrecisionRateOverall /= mSuccessPerFrame.size();
    mSuccessRate /= mSuccessPerFrame.size();
    cout << "The success rate is: " << mSuccessRate << " or " << successRemovingFullError << endl;
    cout << "The precision rate is: " << mPrecisionRateSuccessOnly << " or " << mPrecisionRateOverall << endl;
}
