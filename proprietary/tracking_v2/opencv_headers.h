/*
 * opencv_headers.h
 *
 *  Created on: Tue Jun 19 05:20:03 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef OPENCV_HEADERS_H
#define OPENCV_HEADERS_H

#include <iostream>
#include <opencv2/core/utility.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#endif
