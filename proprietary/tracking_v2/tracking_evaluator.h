/*
 * tracking_evaluator.h
 *
 *  Created on: Thu Jun 21 15:14:25 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef TRACKING_EVALUATOR_H
#define TRACKING_EVALUATOR_H

#include <opencv_headers.h>

#include <string>
#include <vector>

class TrackingEvaluator
{
public:
    TrackingEvaluator(std::vector<cv::Rect> resultData, std::vector<cv::Rect> expectedData);
    virtual ~TrackingEvaluator();
    double GetSuccessRate() const { return mSuccessRate; };
    double GetPrecisionRateOverall() const { return mPrecisionRateOverall; };
    double GetPrecisionRateSuccessOnly() const { return mPrecisionRateSuccessOnly; };
private:
    void Evaluate();
private:
    std::vector<cv::Rect> mResultData;
    std::vector<cv::Rect> mExpectedData;
    double mSuccessRate;
    double mPrecisionRateOverall;       // Gets a zero for unsuccessful rate
    double mPrecisionRateSuccessOnly;
    std::vector<double> mSuccessPerFrame;
    std::vector<double> mPrecisionPerFrame;
    std::vector<bool> mIsSuccessfulFrame;
};
#endif
