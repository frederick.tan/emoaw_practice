#!/bin/bash

for d in `ls ./videos/images/`; do
	if [ ! -f ./videos/images/$d/groundtruth_rect.txt ]; then
		if [ -f ./videos/images/$d/groundtruth_rect.1.txt ]; then
			cp ./videos/images/$d/groundtruth_rect.1.txt ./videos/images/$d/groundtruth_rect.txt
		fi
	fi
done
