/*
 * mean_shift_tracker.cpp
 *
 *  Created on: Tue Jun 19 05:20:03 NZST 2018
 *      Author: Frederick Tan
 */

#include <opencv_headers.h>
#include <ground_truth.h>
#include <tracking_evaluator.h>

#include <ctype.h>
#include <dirent.h>
#include <sys/stat.h>

#include <chrono>
#include <iostream>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

using namespace cv;
using namespace std;
using namespace chrono;

// Note: The code is based on camshiftdemo.cpp example from opencv.org
// The original code can be copied from here: https://docs.opencv.org/3.3.1/d2/dc1/camshiftdemo_8cpp-example.html
// Major modification is to allow the user to input a link to a video file
Mat image;
bool backprojMode = false;
bool selectObject = false;
int trackObject = 0;
bool showHist = true;
Point origin;
Rect selection;
int vmin = 10, vmax = 256, smin = 30;

// User draws box around object to track. This triggers CAMShift to start tracking
static void onMouse( int event, int x, int y, int, void* )
{
    if( selectObject )
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);
        selection &= Rect(0, 0, image.cols, image.rows);
    }
    switch( event )
    {
    case EVENT_LBUTTONDOWN:
        origin = Point(x,y);
        selection = Rect(x,y,0,0);
        selectObject = true;
        break;
    case EVENT_LBUTTONUP:
        selectObject = false;
        if( selection.width > 0 && selection.height > 0 )
            trackObject = -1;   // Set up CAMShift properties in main() loop
        break;
    }
}
string hot_keys =
    "\n\nHot keys: \n"
    "\tESC - quit the program\n"
    "\tc - stop the tracking\n"
    "\tb - switch to/from backprojection view\n"
    "\th - show/hide object histogram\n"
    "\tp - pause video\n"
    "To initialize tracking, select the object with mouse\n";
static void help()
{
    cout << "\nThis is a demo that shows mean-shift based tracking\n"
            "You select a color objects such as your face and it tracks it.\n"
            "This reads from video camera (0 by default, or the camera number the user enters\n"
            "Usage: \n"
            "   ./meanshift [camera number]\n";
    cout << hot_keys;
}
const char* keys =
{
    "{help h | | show help message}{@camera_number| 0 | camera number}"
};
int main( int argc, const char** argv )
{
    Rect trackWindow;
    int hsize = 16;
    float hranges[] = {0,180};
    const float* phranges = hranges;
    CommandLineParser parser(argc, argv, keys);
    if (parser.has("help"))
    {
        help();
        return 0;
    }
    Mat frame, hsv, hue, mask, hist, histimg = Mat::zeros(200, 320, CV_8UC3), backproj;
    bool paused = false;
    bool start = false;
    string directory;
    string filename;
    //string frameNumberToString;
    int frameNumber = 0;
    unique_ptr<GroundTruth> gt;
    vector<Rect> trackedWindows;
    vector<double> trackingDurationPerFrame;
    vector<string> files;
    string directory_img;
        
    if (argc > 1)
    {
        paused = true;
	directory = argv[1];
        directory_img = directory + "/img/";
        cout << directory_img << endl;
        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir (directory_img.c_str())) != NULL) {
            /* print all the files and directories within directory */
            while ((ent = readdir(dir)) != NULL) {
                string fname = ent->d_name;
                if ((fname != ".") && (fname != ".."))
                    files.push_back(fname);
            }
            closedir (dir);
        }
        sort(files.begin(), files.end());
        //for (auto f: files)
        //    cout << f << endl;
        //frameNumberToString = to_string(frameNumber);
	//filename = directory + "/img/" + std::string(4 - frameNumberToString.length(), '0') + frameNumberToString + ".jpg";
	frame = imread(directory_img + files[frameNumber++]);
	gt = unique_ptr<GroundTruth>(new GroundTruth(directory + "/groundtruth_rect.txt"));
	//frameNumber++;
        //return 0;
	//cout << "path: " << directory << endl;
    }
    else
    {
	cerr << "You need to pass the path of the image." << endl;
	return -1;
    }
    cout << hot_keys;
    namedWindow( "Histogram", 0 );
    namedWindow( "MeanShift Object Tracking", 0 );
    setMouseCallback( "MeanShift Object Tracking", onMouse, 0 );
    createTrackbar( "Vmin", "MeanShift Object Tracking", &vmin, 256, 0 );
    createTrackbar( "Vmax", "MeanShift Object Tracking", &vmax, 256, 0 );
    createTrackbar( "Smin", "MeanShift Object Tracking", &smin, 256, 0 );
    for(;;)
    {
        if( !paused )
        {
            if (frameNumber >= (int)files.size())
                break;
            //cout << "fs: " << files.size() << endl;
            //cout << "fm: " << frameNumber << endl;
            //cout << directory_img + files[frameNumber] << endl;
            frame = imread(directory_img + files[frameNumber++]);
            if( frame.empty() )
               	break;
        }
        frame.copyTo(image);
        if( !paused )
        {
            double timer = (double)getTickCount();
            //system_clock::time_point start = system_clock::now();
            cvtColor(image, hsv, COLOR_BGR2HSV);
            if( trackObject )
            {
                int _vmin = vmin, _vmax = vmax;
                inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
                        Scalar(180, 256, MAX(_vmin, _vmax)), mask);
                int ch[] = {0, 0};
                hue.create(hsv.size(), hsv.depth());
                mixChannels(&hsv, 1, &hue, 1, ch, 1);
                if( trackObject < 0 )
                {
                    // Object has been selected by user, set up CAMShift search properties once
                    Mat roi(hue, selection), maskroi(mask, selection);
                    calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
                    normalize(hist, hist, 0, 255, NORM_MINMAX);
                    trackWindow = selection;
                    trackObject = 1; // Don't set up again, unless user selects new ROI
                    histimg = Scalar::all(0);
                    int binW = histimg.cols / hsize;
                    Mat buf(1, hsize, CV_8UC3);
                    for( int i = 0; i < hsize; i++ )
                        buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./hsize), 255, 255);
                    cvtColor(buf, buf, COLOR_HSV2BGR);
                    for( int i = 0; i < hsize; i++ )
                    {
                        int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
                        rectangle( histimg, Point(i*binW,histimg.rows),
                                   Point((i+1)*binW,histimg.rows - val),
                                   Scalar(buf.at<Vec3b>(i)), -1, 8 );
                    }
                }
                // Perform CAMShift
                calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
                backproj &= mask;
                //RotatedRect trackBox = CamShift(backproj, trackWindow,
                //RotatedRect trackBox = CamShift(backproj, trackWindow, TermCriteria(TermCriteria::EPS | TermCriteria::COUNT, 10, 1 ));
                //cout << "Tracing." << endl;
                meanShift(backproj, trackWindow, TermCriteria(TermCriteria::EPS | TermCriteria::COUNT, 10, 1 ));
                //cout << "Tracing.." << endl;
                if(trackWindow.area() <= 1 )
                {
                    //cout << "Tracing..." << endl;
                    int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
                    trackWindow = Rect(trackWindow.x - r, trackWindow.y - r,
                                       trackWindow.x + r, trackWindow.y + r) &
                                  Rect(0, 0, cols, rows);
                }
                //system_clock::time_point end = system_clock::now();
                //cout << "A!" << endl;
                trackingDurationPerFrame.push_back(1000.0 * ((double)getTickCount() - timer) / getTickFrequency());
                //cout << "A!!" << endl;
                //trackingDurationPerFrame.push_back(std::chrono::duration_cast<std::chrono::microseconds>(end - start).count());
                if( backprojMode )
                    cvtColor( backproj, image, COLOR_GRAY2BGR );
                //cout << "A!!!" << endl;
                //ellipse( image, trackBox, Scalar(0,0,255), 3, LINE_AA );
		//cout << "(x, y)" << trackBox.boundingRect().x << ", " << trackBox.boundingRect().y << endl;
		//cv::rectangle(image, trackBox.boundingRect().tl(), trackBox.boundingRect().br(), Scalar(0,0,255), 3, LINE_AA);
		cv::rectangle(image, trackWindow.tl(), trackWindow.br(), Scalar(255,0,0), 3, LINE_AA);
                //cout << frameNumber - 1<< " vs " << gt->mRectPerFrame.size() << endl;
		cv::rectangle(image, gt->mRectPerFrame[frameNumber - 1].tl(),
			      gt->mRectPerFrame[frameNumber - 1].br(), Scalar(0,255,0), 3, LINE_AA);
                trackedWindows.push_back(trackWindow);
                //cout << trackedWindows.size() << endl;
            }
        }
        else if( trackObject < 0 )
            paused = false;
	if (frameNumber == 1)
	{
		static bool isFirstPass = true;
		if (isFirstPass) {
			selection = gt->mRectPerFrame[frameNumber];
			selectObject = true;
			isFirstPass = false;
		}
		else {
			selectObject = false;
			trackObject = -1;
			paused = false;
		}
	}
	//else if (!start)
	//	cv::rectangle(image, gt->mRectPerFrame[1].tl(), gt->mRectPerFrame[1].br(), Scalar(255,0,0), 3, LINE_AA);
        if( selectObject && selection.width > 0 && selection.height > 0 )
        {
            Mat roi(image, selection);
            bitwise_not(roi, roi);
        }
        imshow( "MeanShift Object Tracking", image );
        imshow( "Histogram", histimg );
        char c = (char)waitKey(10);
        if( c == 27 )
            break;
        switch(c)
        {
        case 'b':
            backprojMode = !backprojMode;
            break;
        case 'c':
            trackObject = 0;
            histimg = Scalar::all(0);
            break;
        case 'h':
            showHist = !showHist;
            if( !showHist )
                destroyWindow( "Histogram" );
            else
                namedWindow( "Histogram", 1 );
            break;
        case 'p':
            paused = !paused;
            break;
        case 's':
            start = !start;
            break;
        default:
            ;
        }
    }
    //cout << "Almost there." << endl;
    TrackingEvaluator evaluator(trackedWindows, gt->mRectPerFrame);
    //cout << "Almost there.." << endl;
    double averageTs = 0.0;
    for (auto ts : trackingDurationPerFrame)
    {
        averageTs += ts;
    }
    //cout << "Almost there..." << endl;
    averageTs /= trackingDurationPerFrame.size();
    ofstream myfile;
    myfile.open(directory + "/stats.csv");
    myfile << averageTs << "," << evaluator.GetSuccessRate() << "," << evaluator.GetPrecisionRateSuccessOnly() << "," << evaluator.GetPrecisionRateOverall();
    myfile << endl;
    myfile.close();
    return 0;
}
