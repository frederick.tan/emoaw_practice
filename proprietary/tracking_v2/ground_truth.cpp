/*
 * ground_truth.cpp
 *
 *  Created on: Thu Jun 21 01:10:04 NZST 2018
 *      Author: Frederick Tan
 */

#include <ground_truth.h>

#include <fstream>
#include <sstream>

using namespace std;
using namespace cv;

GroundTruth::GroundTruth(const std::string& filename)
{
	Read(filename);
};

GroundTruth::~GroundTruth()
{
};

void GroundTruth::Read(const std::string& filename)
{
	ifstream file(filename);
	string line;
	while (getline(file, line))
	{
		stringstream ss(line);
		string item;
		vector<int> rectElems;
		size_t pos = 0, lastPos = 0;
		while ((pos = line.find_first_of(",\t", lastPos)) != string::npos)
		//while (getline(ss, item, ','))
		{
			item = line.substr(lastPos, pos-lastPos+1);
			lastPos = pos+1;
			rectElems.push_back(stoi(item));
		}
		item = line.substr(lastPos);
		rectElems.push_back(stoi(item));
		mRectPerFrame.push_back(Rect(rectElems[0], rectElems[1], rectElems[2], rectElems[3]));
	}
}
