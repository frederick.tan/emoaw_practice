/*
 * ground_truth.h
 *
 *  Created on: Thu Jun 21 01:10:04 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef GROUND_TRUTH_H
#define GROUND_TRUTH_H

#include <opencv_headers.h>

#include <string>
#include <vector>

class GroundTruth
{
public:
	GroundTruth(const std::string& filename);
	virtual ~GroundTruth();
private:
	void Read(const std::string& filename);
public:
	std::vector<cv::Rect> mRectPerFrame;
};
#endif
