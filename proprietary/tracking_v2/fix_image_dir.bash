#!/bin/bash

for d in `ls ./videos/images/`; do
	if [ ! -f ./videos/images/$d/groundtruth_rect.txt ] && [ ! -f ./videos/images/$d/groundtruth_rect.1.txt ]; then
		cp -rfp ./videos/images/$d/$d/* ./videos/images/$d/
	fi
done
