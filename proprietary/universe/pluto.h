/*
 * pluto.h
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef PLUTO_H
#define PLUTO_H

#include <planet.h>

class Pluto : public Planet
{
public:
	Pluto(Camera *camera);
	virtual ~Pluto();
};
#endif

