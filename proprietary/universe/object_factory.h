/**
 * @file: ObjectFactory.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_OBJECT_FACTORY_H__
#define __OPENGLWRAPPER_OBJECT_FACTORY_H__

#include <opengl_platform_headers.h>

#include <vector>

class ObjectFactory
{
public:
	ObjectFactory();
	virtual ~ObjectFactory();
	static void CreateSphere(std::vector<glm::vec4> &buffer, std::vector<glm::ivec3> &indexes, float r, int sub1, int sub2);
};
#endif
