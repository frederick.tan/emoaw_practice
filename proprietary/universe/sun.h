/*
 * sun.h
 *
 *  Created on: Fri Jun  8 06:21:26 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef SUN_H
#define SUN_H

#include <heavenly_body.h>

class Sun : public HeavenlyBody
{
public:
	Sun(Camera* camera);
	virtual ~Sun();
	void Draw(double dt, double modelThetaX, double modelThetaY);
};
#endif
