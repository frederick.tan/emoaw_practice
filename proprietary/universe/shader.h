/**
 * @file: Shader.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_SHADER_H__
#define __OPENGLWRAPPER_SHADER_H__

#include <opengl_platform_headers.h>

#include <string>

class Shader
{
public:
	Shader(const char *vertexFilename, const char *controlFilename, const char *evaluationFileName, const char *geometryFilename, const char *fragmentFilename);
	virtual ~Shader();
	GLuint GetProgram() { return mProgram; };
private:
	char *ReadFile(const char *filename);
	GLuint CheckShader(GLuint shader);
	GLuint CheckProgram(GLuint program);
	GLuint LoadShader(GLuint type, const char *filename);
	void LoadProgram();
private:
	GLuint mProgram;
	const std::string mVertexFileName;
	const std::string mControlFileName;
	const std::string mEvaluationFileName;
	const std::string mGeometryFileName;
	const std::string mFragmentFileName;
};
#endif
