/*
 * mercury.cpp
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#include <mercury.h>

Mercury::Mercury(Camera *camera)
	: Planet(camera, 0.0000024397 * 1000, "images/2k_mercury.jpg")
{
	mEccentricity = 0.206;
	mSemiMajor = 0.005791;
	mRevolutionInSec = 87.97;
};

Mercury::~Mercury()
{
};

