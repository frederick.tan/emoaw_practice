/*
 * earth.h
 *
 *  Created on: Wed Jun  6 19:44:31 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef EARTH_H
#define EARTH_H

#include <planet.h>

class Earth : public Planet
{
public:
	Earth(Camera *camera);
	virtual ~Earth();
	//virtual void Move(double dt, double modelThetaX, double modelThetaY);
};
#endif
