/*
 * mercury.h
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef MERCURY_H
#define MERCURY_H

#include <planet.h>

class Mercury : public Planet
{
public:
	Mercury(Camera *camera);
	virtual ~Mercury();
};
#endif

