/*
 * venus.h
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef VENUS_H
#define VENUS_H

#include <planet.h>

class Venus : public Planet
{
public:
	Venus(Camera *camera);
	virtual ~Venus();
};
#endif
