/**
 * @file: shader.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <shader.h>

#include <iostream>
#include <fstream>

using namespace std;

Shader::Shader(const char *vertexFilename, const char *controlFilename, const char *evaluationFileName, const char *geometryFilename, const char *fragmentFilename)
	: mProgram(0)
	, mVertexFileName(vertexFilename)
	, mControlFileName(controlFilename)
	, mEvaluationFileName(evaluationFileName)
	, mGeometryFileName(geometryFilename)
	, mFragmentFileName(fragmentFilename)
{
	LoadProgram();
}

Shader::~Shader()
{
}

char *Shader::ReadFile(const char *filename)
{
	std::ifstream input(filename);
	// Check if file is open
	if (!input.good())
	{
		std::cerr << "Error: Could not open " << filename << std::endl;
		return 0;
	}
	// Find end of file
	input.seekg(0, std::ios::end);

	// Calculate Size
	size_t size = input.tellg();

	// Allocate required memory
	char *data = new char[size+1];

	// Rewind to beginning
	input.seekg(0, std::ios::beg);

	// Read file into memory
	input.read(data, size);

	// Append '\0'
	data[size] = '\0';

	// Close file
	input.close();

	// Return file contents
	return data;
}

// Check the status of a Shader
GLuint Shader::CheckShader(GLuint shader)
{
	// Compile status
	GLint status = 0;

	// Check compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	// Error detected
	if (status != GL_TRUE)
	{
		// Get error message length
		int size;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &size);

		// Get error message
		char *message = new char[size];
		glGetShaderInfoLog(shader, size, &size, message);

		// Print error message
		std::cerr << message << std::endl;

		// Delete message
		delete[] message;

		// Return error
		return GL_FALSE;
	}

	// Return success
	return GL_TRUE;
}

// Check the status of a Program
GLuint Shader::CheckProgram(GLuint mProgram)
{
	// Link status
	GLint status = 0;

	// Check link status
	glGetProgramiv(mProgram, GL_LINK_STATUS, &status);

	// Error detected
	if (status != GL_TRUE)
	{
		// Get error message length
		int size;
		glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &size);

		// Get error message
		char *message = new char[size];
		glGetProgramInfoLog(mProgram, size, &size, message);

		// Print error message
		std::cerr << message << std::endl;

		// Delete message
		delete[] message;

		// Return error
		return GL_FALSE;
	}

	// Return success
	return GL_TRUE;
}

// Load and Compiler Shader for source file
GLuint Shader::LoadShader(GLuint type, const char *filename)
{
	// Read the shader source from file
	char *source = ReadFile(filename);

	// Check shader source
	if (source == 0)
	{
		// Return Error
		return 0;
	}

	// Create the OpenGL Shaders
	GLuint shader = glCreateShader(type);

	// Load the source into the shaders
	glShaderSource(shader, 1, &source, NULL);

	// Compile the Shaders
	glCompileShader(shader);

	// Check shaders for errors
	if (CheckShader(shader) == GL_TRUE)
	{
		// Log
		std::cout << "Loaded: " << filename << std::endl;
	}
	else
	{
		// Print Error
		std::cerr << "Error: could not compile " << filename << std::endl;

		// Return Error
		shader = 0;
	}

	// Delete shader source
	delete[] source;

	// Return shader
	return shader;
}

void Shader::LoadProgram()
{
	mProgram = glCreateProgram();

	// Shader Handles
	GLuint vertShader = 0;
	GLuint ctrlShader = 0;
	GLuint evalShader = 0;
	GLuint geomShader = 0;
	GLuint fragShader = 0;

	// Load Shaders
	std::cout << "loading shaders: " << mVertexFileName << ", ";
	std::cout << mControlFileName << ", " << mEvaluationFileName << ", ";
	std::cout << mGeometryFileName << ", " << mFragmentFileName << endl;
	if (!mVertexFileName.empty()) vertShader = LoadShader(GL_VERTEX_SHADER, mVertexFileName.c_str());
	if (!mControlFileName.empty()) ctrlShader = LoadShader(GL_TESS_CONTROL_SHADER, mControlFileName.c_str());
	if (!mEvaluationFileName.empty()) evalShader = LoadShader(GL_TESS_EVALUATION_SHADER, mEvaluationFileName.c_str());
	if (!mGeometryFileName.empty()) geomShader = LoadShader(GL_GEOMETRY_SHADER, mGeometryFileName.c_str());
	if (!mFragmentFileName.empty()) fragShader = LoadShader(GL_FRAGMENT_SHADER, mFragmentFileName.c_str());

	// Attach shaders
	if (vertShader != 0) glAttachShader(mProgram, vertShader);
	if (ctrlShader != 0) glAttachShader(mProgram, ctrlShader);
	if (evalShader != 0) glAttachShader(mProgram, evalShader);
	if (geomShader != 0) glAttachShader(mProgram, geomShader);
	if (fragShader != 0) glAttachShader(mProgram, fragShader);

	// Check Vertex Shader
	if ((vertShader == 0) || (fragShader == 0))
	{
		// Print Error
		std::cerr << "Error: program missing vertex or fragment shader." << std::endl;

		// Return Error
		mProgram = 0;
	}
	else
	{
		// Link mProgram
		glLinkProgram(mProgram);
	}

	// Delete Shaders (no longer needed)
	if (vertShader != 0) glDeleteShader(vertShader);
	if (ctrlShader != 0) glDeleteShader(ctrlShader);
	if (evalShader != 0) glDeleteShader(evalShader);
	if (geomShader != 0) glDeleteShader(geomShader);
	if (fragShader != 0) glDeleteShader(fragShader);

	// Check mProgram for errors
	if (CheckProgram(mProgram) == GL_TRUE)
	{
		// Print Log
		std::cout << "Loaded: program" << std::endl;
	}
	else
	{
		// Print Error
		std::cerr << "Error: could not link mProgram" << std::endl;

		// Return Error
		mProgram = 0;
	}
}
