// OpenGL 3.3
#version 330

// Input from Vertex Shader
in vec4 frag_Position;
in vec4 frag_Normal;
in vec4 frag_Light_Direction;
in vec4 frag_UV;
flat in int frag_is_light_source;
uniform int frag_Use_Texture = 0;

// Material
uniform vec4 frag_Material_Ka;
uniform vec4 frag_Material_Kd;
uniform vec4 frag_Material_Ks;
uniform float frag_Material_a;
uniform int frag_Material_illum = 1;
uniform vec3 frag_LightColor;

// Material - Brass
uniform vec4 Ka = vec4(0.329412, 0.223529, 0.027451, 1.0);
uniform vec4 Kd = vec4(0.780392, 0.568627, 0.113725, 1.0);
uniform vec4 Ks = vec4(0.992157, 0.941176, 0.807843, 1.0);
uniform float a = 27.89743616;

// Texture
uniform sampler2D u_texture_Map;

// Output from Fragment Shader
out vec4 pixel_Colour;

// Light Source
uniform vec4 Ia_nls = vec4(0.5f, 0.0f, 0.5f, 10.0f);
uniform vec4 Id_nls = vec4(1.0f, 1.0f, 1.0f, 10.0);
uniform vec4 Is_nls = vec4(0.0f, 0.0f, 0.0f, 0.0f);
uniform vec4 Ia_ls = vec4(1.0f, 1.0f, 1.0f, 0.0f);
uniform vec4 Id_ls = vec4(10.0f, 10.0f, 10.0f, 10.0);
uniform vec4 Is_ls = vec4(1.0f, 1.0f, 1.0f, 1.0f);

void main () {
	//----------------------------------------------
	// Phong Reflection Model
	//----------------------------------------------
	vec4 Ia;
	vec4 Id;
	vec4 Is;
	if (frag_is_light_source == 1)
	{
		Ia = Ia_ls;
		Id = Id_ls;
		Is = Is_ls;
	}
	else
	{
		Ia = Ia_nls;
		Id = Id_nls;
		Is = Is_nls;
	}

	// ---------- Calculate Vectors ----------
	// Direction to Light (normalised)
	vec4 l = normalize(-frag_Light_Direction);

	// Surface Normal (normalised)
	vec4 n = normalize(frag_Normal);

	// Reflected Vector
	vec4 r = reflect(-l, n);

	// View Vector
	vec4 v = normalize(-frag_Position);

	// ---------- Calculate Terms ----------
	// Ambient Term
	vec4 Ta = Ka * Ia * frag_Material_illum;

	// Diffuse Term
	vec4 Td = Kd * max(dot(l, n), 0.0) * Id;

	// Specular Term
	vec4 Ts = Ks * pow((max(dot(r, v), 0.0)), a) * Is;

	//----------------------------------------------
	// Fragment Colour
	//----------------------------------------------
	//if (frag_Use_Texture == 1)
	//{
		pixel_Colour = texture(u_texture_Map, frag_UV.xy);
	//}
	//else
	//{
		pixel_Colour = (Ta + Td + Ts) * pixel_Colour;
	//}
}
