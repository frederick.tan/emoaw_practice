// OpenGL 3.3
#version 430

// Input to Vertex Shader
in vec4 vert_Position;
in vec4 vert_Normal;
in vec4 vert_UV;

// Transform Matrices
uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;

// Light Source - Directional
uniform vec4 u_Light_Position = vec4(0.5f, 0.0f, 0.5f, 1000.00f);
uniform int frag_is_light_source = 0;

// Output to Fragment Shader
out vec4 frag_Position;
out vec4 frag_Normal;
out vec4 frag_Light_Direction;
out vec4 frag_UV;
//flat out int frag_is_light_source;

void main() {
	// Frag Position
	frag_Position = u_View * u_Model * vert_Position;

	// Frag Normal
	frag_Normal = u_View * u_Model * vert_Normal;

	// Frag Light Position
	if (frag_is_light_source == 0)
		frag_Light_Direction = frag_Position - u_View * u_Light_Position;
	else
		frag_Light_Direction = frag_Position - u_View * vert_Normal * 1.5;

	// Frag UV
	frag_UV = vert_UV;

	//----------------------------------------------
	// Vertex Position
	//----------------------------------------------
	gl_Position = u_Projection * u_View * u_Model * vert_Position;
}
