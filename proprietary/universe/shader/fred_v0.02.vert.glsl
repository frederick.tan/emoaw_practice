// OpenGL 3.3
#version 330

// Input to Vertex Shader
in vec4 vert_Position;
in vec4 vert_Normal;
in vec4 vert_UV;
in int vert_Use_Texture;
in int vert_is_light_source;

// Transform Matrices
uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;
uniform vec3 u_LightPosition;

// Light Source - Directional
//uniform vec4 u_Light_Direction = vec4(0.5f, 0.0f, 0.5f, 1000.00f);
//uniform vec4 u_Light_Direction_LightSource = vec4(1.0f, 1.0f, 1.0f, 1000.00f);
//uniform vec4 u_Light_Direction = vec4(0.00f, 0.0f, 0.00f, 0.0f);

// Output to Fragment Shader
out vec4 frag_Position;
out vec4 frag_Normal;
out vec4 frag_Light_Direction;
out vec4 frag_UV;
flat out int frag_is_light_source;

void main() {
	// Frag Position
	frag_Position = u_View * u_Model * vert_Position;

	// Frag Normal
	frag_Normal = u_View * u_Model * vert_Normal;

	// Frag Light Position
	if (vert_is_light_source == 1)
	{
		frag_Light_Direction = u_View * u_Light_Direction_LightSource;
	}
	else
	{
		frag_Light_Direction = u_View * u_Light_Direction;
		frag_Light_Direction = (-frag_Light_Direction);
	}
	frag_is_light_source = vert_is_light_source;
	//frag_is_light_source = 1;
	frag_Light_Direction = u_LightPosition - vert_Position;

	// Frag UV
	frag_UV = vert_UV;

	//----------------------------------------------
	// Vertex Position
	//----------------------------------------------
	gl_Position = u_Projection * u_View * u_Model * vert_Position;
}
