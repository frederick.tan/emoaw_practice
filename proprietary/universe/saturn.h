/*
 * saturn.h
 *
 *  Created on: Wed Jun  6 19:44:31 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef SATURN_H
#define SATURN_H

#include <planet.h>

class Saturn : public Planet
{
public:
	Saturn(Camera *camera);
	virtual ~Saturn();
	//virtual void Move(double dt, double modelThetaX, double modelThetaY);
};
#endif
