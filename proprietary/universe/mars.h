/*
 * mars.h
 *
 *  Created on: Wed Jun  6 19:44:34 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef MARS_H
#define MARS_H

#include <planet.h>

class Mars : public Planet
{
public:
	Mars(Camera *camera);
	virtual ~Mars();
};
#endif
