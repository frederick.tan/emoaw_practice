#include <cmath>

// SemiMajor in KM
// Source: http://www.smartconversion.com/otherInfo/Semimajor_Axis_of_planets_and_the_sun.aspx
// Pluto	5.90638 x 109
// Neptune	4.49506 x 109
// Uranus	2.87246 x 109
// Saturn	1.43353 x 109
// Jupiter	7.7857 x 108
// Mars	2.2792 x 108
// Earth	1.496 x 108
// Venus	1.0821 x 108
// Mercury	5.791 x 107
// Moon	3.844 x 105

const double SemiMajorPluto = 0.590638;
const double SemiMajorNeptune = 0.449506;
const double SemiMajorUranus = 0.287246;
const double SemiMajorSaturn = 0.143353;
const double SemiMajorJupiter = 0.077857;
const double SemiMajorMars = 0.022792;
const double SemiMajorEarth = 0.01496;
const double SemiMajorVenus = 0.010821;
const double SemiMajorMercury = 0.005791;
const double SemiMajorMoon = 0.00003844;

// Radius in KM
// Source: http://www.smartconversion.com/otherInfo/Volumetric_mean_radius_of_planets_and_the_sun.aspx
// Sun	696000
// Jupiter	69911
// Saturn	58232
// Uranus	25362
// Neptune	24622
// Earth	6371
// Venus	6051.8
// Mars	3389.5
// Mercury	2439.7
// Moon	1737.1
// Pluto	1195

const double MeanRadiusSun = 0.000696000;
const double MeanRadiusJupiter = 0.000069911;
const double MeanRadiusSaturn = 0.000058232;
const double MeanRadiusUranus = 0.000025362;
const double MeanRadiusNeptune = 0.000024622;
const double MeanRadiusEarth = 0.000006371;
const double MeanRadiusVenus = 0.0000060518;
const double MeanRadiusMars = 0.0000033895;
const double MeanRadiusMercury = 0.0000024397;
const double MeanRadiusMoon = 0.0000017371;
const double MeanRadiusPluto = 0.000001195;

// eccentricity
// source: http://www.enchantedlearning.com/subjects/astronomy/glossary/Eccentricity.shtml
const double EccentricityMercury = 0.206;
const double EccentricityVenus = 0.007;
const double EccentricityEarth = 0.017;
const double EccentricityMars = 0.093;
const double EccentricityJupiter = 0.048;
const double EccentricitySaturn = 0.056;
const double EccentricityUranus = 0.047;
const double EccentricityNeptune = 0.009;
const double EccentricityPluto = 0.248;

// Planetary rotation and revolution
// Source: https://www.exploratorium.edu/ronh/age/
// Planet	Rotation	Revolution
// Mercury	58.6 days	87.97 days
// Venus	243 days	224.7 days
// Earth	0.99 days	365.26 days
// Mars		1.03 days	1.88 years
// Jupiter	0.41 days	11.86 years
// Saturn	0.45 days	29.46 years
// Uranus	0.72 days	84.01 years
// Neptune	0.67 days	164.79 years
// Pluto	6.39 days	248.59 years
// Scale: 1 day == 1 second (?)
// 1 year = 365 days = 365 seconds
// For pluto = 365 seconds * 248.59 = 90735.35 seconds (about 1 day and 1 hour)
const double RotationInSecMercury	= 58.6;
const double RotationInSecVenus		= 243.0;
const double RotationInSecEarth		= 1.0;
const double RotationInSecMars		= 1.03;
const double RotationInSecJupiter	= 0.41;
const double RotationInSecSaturn	= 0.45;
const double RotationInSecUranus	= 0.72;
const double RotationInSecNeptune	= 0.67;
const double RotatioInSecnPluto		= 6.39;

const double RevolutionInSecMercury	= 87.97;
const double RevolutionInSecVenus	= 224.7;
const double RevolutionInSecEarth	= 365.26;
const double RevolutionInSecMars	= 1.88 * 365.0;
const double RevolutionInSecJupiter	= 11.86 * 365.0;
const double RevolutionInSecSaturn	= 29.46 * 365.0;
const double RevolutionInSecUranus	= 84.01 * 365.0;
const double RevolutionInSecNeptune	= 164.79 * 365.0;
const double RevolutionInSecPluto	= 248.59 * 365.0;

double EccAnom(double ec, double m, double dp)
{
	// arguments:
	// ec=eccentricity, m=mean anomaly,
	// dp=number of decimal places
	double K = M_PI/180.0;
	const int MaxIteration = 30
	int i = 0;
	double delta = pow(10,-dp);
	double E, F;

	m = m/360.0;

	m = 2.0 * M_PI * (m - floor(m));

	if (ec<0.8) E = m;
	else E = M_PI;

	F = E - ec * sin(m) - m;

	while ((abs(F) > delta) && (i < MaxIteration))
	{

		E = E - F/(1.0 - ec * cos(E));
		F = E - ec * sin(E) - m;

		i = i + 1;
	}
	E=E/K;
	return round(E * pow(10, dp)) / pow(10, dp);
}

double TrueAnom(double ec,double E,double dp)
{

	double K = M_PI / 180.0;
	double S = sin(E);

	double C = cos(E);

	double fak = sqrt(1.0-ec*ec);

	double phi = atan2(fak * S, C - ec)/K;

	return round(phi * pow(10,dp))/ pow(10,dp);
}

void position(double a, double ec, double E)
{
	// a=semimajor axis, ec=eccentricity, E=eccentric anomaly
	// x,y = coordinates of the planet with respect to the Sun

	double C = cos(E);
	double S = sin(E);

	double x = a * (C-ec);

	double y = a * sqrt(1.0 - ec * ec) * S;
}

void Move(double dt)
{
	// (1) Calculate M
	//		n = 360 / revolution (angle per second)
	//		current_time += dt
	//		if (current_time > revolution time) current_time -= revolution_time
	//		M = n * current_time
	// current position
	
}
