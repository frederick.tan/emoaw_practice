/*
 * neptune.cpp
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#include <neptune.h>

Neptune::Neptune(Camera *camera)
	: Planet(camera, 0.000024622 * 1000, "images/2k_neptune.jpg")
{
	mEccentricity = 0.009;
	mSemiMajor = 0.449506;
	mRevolutionInSec = 164.79 * 365.0;
};

Neptune::~Neptune()
{
};

