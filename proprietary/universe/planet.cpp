/*
 * planet.cpp
 *
 *  Created on: Wed Jun  6 16:23:17 NZST 2018
 *      Author: Frederick Tan
 */

#include <planet.h>
#include <shader.h>

#include <cmath>

using namespace std;

Planet::Planet(Camera* camera, double radius, const std::string& textureFilename)
	: HeavenlyBody(camera, radius * 1.5, textureFilename)
{
};

Planet::~Planet()
{
};

double Planet::ComputeEccentricAnomaly(double ec, double m, double dp)
{
	// arguments:
	// ec=eccentricity, m=mean anomaly,
	// dp=number of decimal places
	double K = M_PI / 180.0;
	const int MaxIteration = 30;
	int i = 0;
	double delta = pow(10,-dp);
	double E, F;
	m = m / 360.0;
	m = 2.0 * M_PI * (m - floor(m));
	if (ec < 0.8) E = m;
	else E = M_PI;
	F = E - ec * sin(m) - m;
	while ((abs(F) > delta) && (i < MaxIteration))
	{
		E = E - F/(1.0 - ec * cos(E));
		F = E - ec * sin(E) - m;
		i = i + 1;
	}
	E = E / K;
	return round(E * pow(10, dp)) / pow(10, dp);
}

double Planet::ComputeTrueAnomaly(double ec, double E, double dp)
{
	double K = M_PI / 180.0;
	double S = sin(E);
	double C = cos(E);
	double fak = sqrt(1.0 - ec * ec);
	double phi = atan2(fak * S, C - ec) / K;
	return round(phi * pow(10, dp)) / pow(10,dp);
}

void Planet::Move(double dt, double modelThetaX, double modelThetaY)
{
	// (1) Calculate M
	//		n = 360 / revolution (angle per second)
	//		current_time += dt
	//		if (current_time > revolution time) current_time -= revolution_time
	//		M = n * current_time
	// current position
	double n = 360.0 / mRevolutionInSec;
	//double n = 2 * M_PI / mRevolutionInSec;
	mEllapsedTime += dt;
	if (mEllapsedTime > mRevolutionInSec) mEllapsedTime -= mRevolutionInSec;
	double M = n * mEllapsedTime;
	double E = ComputeEccentricAnomaly(mEccentricity, M, 9);
	E = ComputeTrueAnomaly(mEccentricity, E, 9);
	double C = cos(E);
	double S = sin(E);
	double x = mSemiMajor * pow(10, 2.0) * (C - mEccentricity);
	double z = mSemiMajor * pow(10, 2.0) * sqrt(1.0 - mEccentricity * mEccentricity) * S;
	if (x >= 0) x += 0.5 + 0.000696000 * 1000;
	else x -= (0.5 + 0.000696000 * 1000);
	if (z >= 0) z += 0.5 + 0.000696000 * 1000;
	else z -= (0.5 + 0.000696000 * 1000);
	//printf("x: %08f y: %08f et: %08f\n", x, y, mEllapsedTime);
	//mModelMatrix = glm::translate(glm::mat4(), glm::vec3(x, y, 0.0f)) * 
	mModelMatrix = glm::translate(glm::mat4(), glm::vec3(x, 0.0f, z)) * 
		       glm::rotate(glm::mat4(), (float)modelThetaY, glm::vec3(0.0f, 1.0f, 0.0f)) *
		       glm::rotate(glm::mat4(), (float)modelThetaX, glm::vec3(1.0f, 0.0f, 0.0f));
	// Copy to Reflection mProgram
	glUseProgram(mProgram);

	// Copy Model Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_Model"), 1, GL_FALSE, glm::value_ptr(mModelMatrix));
	// Copy View Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_View"),  1, GL_FALSE, glm::value_ptr(mCamera->getViewMatrix()));
	//glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_IView"), 1, GL_FALSE, glm::value_ptr(glm::inverse(mCamera->getViewMatrix())));
}
