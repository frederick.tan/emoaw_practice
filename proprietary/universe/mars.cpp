/*
 * mars.cpp
 *
 *  Created on: Wed Jun  6 19:44:34 NZST 2018
 *      Author: Frederick Tan
 */

#include <mars.h>

Mars::Mars(Camera *camera)
	: Planet(camera, 0.0000033895 * 1000, "images/2k_mars.jpg")
{
	mEccentricity = 0.093;
	mSemiMajor = 0.022792;
	mRevolutionInSec = 1.88 * 365.0;
};

Mars::~Mars()
{
};
