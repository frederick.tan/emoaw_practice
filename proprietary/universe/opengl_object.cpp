/*
 * opengl_object.cpp
 *
 *  Created on: Wed Jun  6 16:23:17 NZST 2018
 *      Author: Frederick Tan
 */

#include <opengl_object.h>

OpenGLObject::OpenGLObject()
{
}

OpenGLObject::~OpenGLObject()
{
}

void OpenGLObject::Init()
{
	glUseProgram(mProgram);
	int x, y, n;
	//unsigned char *image = OpenGLUtil::LoadImage(filename.c_str(), mWidth, mHeight, mN, true);
	unsigned char *image = OpenGLUtil::LoadImage(mTextureFilename.c_str(), x, y, n, true);
	if (image == NULL) return;
	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	// ------------------------------
	// Mip-Mapping
	// ------------------------------
	// Set storage - log_2(image size)
	glTexStorage2D(GL_TEXTURE_2D, 8, GL_RGBA8, x, y);

	// Copy image data into texture
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, x, y, GL_RGBA, GL_UNSIGNED_BYTE, image);

	// Generate Mipmap
	glGenerateMipmap(GL_TEXTURE_2D);

	// Configure texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	// Configure Texture Coordinate Wrapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);

	// ------------------------------
	// Anistropic Filtering
	// ------------------------------
	// Get Maximum Anistropic level
	GLfloat maxAnistropy = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnistropy);

	// Enable Anistropic Filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnistropy);

	// ------------------------------
	// Unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete image data
	delete[] image;
	image = NULL;

	// Create VAO, VBO & EBO
	glGenVertexArrays(1, &mVao);
	glGenBuffers(1, &mVbo);
	glGenBuffers(1, &mEbo);

	// Bind VAO, VBO & EBO
	glBindVertexArray(mVao);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEbo);
	//
	// Load Vertex Data
	glBufferData(GL_ARRAY_BUFFER, mBuffer.size() * sizeof(glm::vec4), mBuffer.data(), GL_STATIC_DRAW);

	// Load Element Data
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size() * sizeof(glm::ivec3), mIndices.data(), GL_STATIC_DRAW);

	// Get Position Attribute location (must match name in shader)
	GLuint posLoc = glGetAttribLocation(mProgram, "vert_Position");

	// Get Normal Attribute location (must match name in shader)
	GLuint norLoc = glGetAttribLocation(mProgram, "vert_Normal");

	// Get Texture Attribute location (must match name in shader)
	GLuint texLoc = glGetAttribLocation(mProgram, "vert_UV");

	// Set Vertex Attribute Pointers
	glVertexAttribPointer(posLoc, 4, GL_FLOAT, GL_FALSE, 12 * sizeof(GLfloat), NULL);
	glVertexAttribPointer(norLoc, 4, GL_FLOAT, GL_FALSE, 12 * sizeof(GLfloat), (GLvoid*)(4*sizeof(float)));
	glVertexAttribPointer(texLoc, 4, GL_FLOAT, GL_FALSE, 12 * sizeof(GLfloat), (GLvoid*)(8*sizeof(float)));

	// Enable Vertex Attribute Arrays
	glEnableVertexAttribArray(posLoc);
	glEnableVertexAttribArray(norLoc);
	glEnableVertexAttribArray(texLoc);

	// Unbind VAO, VBO & EBO
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glUniform1i(glGetUniformLocation(mProgram, "u_texture_Map"), 0);

	// Model Matrix
	mModelMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f)) *
				   glm::rotate(glm::mat4(), (float)mPosX, glm::vec3(1.0f, 0.0f, 0.0f)) * 
				   glm::rotate(glm::mat4(), (float)mPosY, glm::vec3(0.0f, 1.0f, 0.0f));
	
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_Model"),
						1, GL_FALSE, glm::value_ptr(mModelMatrix));
	// ----------------------------------------
	// View Matrix
	// ----------------------------------------
	// Copy Model Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_Model"), 1, GL_FALSE, glm::value_ptr(mModelMatrix));
	// Copy View Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_View"),  1, GL_FALSE, glm::value_ptr(mCamera->getViewMatrix()));
	//glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_IView"), 1, GL_FALSE, glm::value_ptr(glm::inverse(mCamera->getViewMatrix())));

	// ----------------------------------------
	// Projection Matrix
	// ----------------------------------------
	glm::mat4 projectionMatrix;
	
	// Calculate Perspective Projection
	projectionMatrix = glm::perspective(glm::radians(67.0f), 1.0f, 0.2f, 50.0f);

	// Copy Projection Matrix to Shader
	glUseProgram(mProgram);
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_Projection"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
}

