/*
 * jupiter.cpp
 *
 *  Created on: Wed Jun  6 19:44:47 NZST 2018
 *      Author: Frederick Tan
 */

#include <jupiter.h>

Jupiter::Jupiter(Camera *camera)
	: Planet(camera, 0.000069911 * 10000, "images/2k_jupiter.jpg")
{
	mEccentricity = 0.048;
	mSemiMajor = 0.077857;
	mRevolutionInSec = 11.86 * 365.0;
};

Jupiter::~Jupiter()
{
};
