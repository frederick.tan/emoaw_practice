/*
 * sun.cpp
 *
 *  Created on: Fri Jun  8 06:21:26 NZST 2018
 *      Author: Frederick Tan
 */

#include <sun.h>

Sun::Sun(Camera *camera)
	: HeavenlyBody(camera, 0.000696000 * 1000, "images/2k_sun.jpg", 0.5, 0.0, 0.5)
{
	printf("Creating Sun\n");
};

Sun::~Sun()
{
};

void Sun::Draw(double dt, double modelThetaX, double modelThetaY)
{
	mModelMatrix = glm::translate(glm::mat4(), glm::vec3(mOriginalPosX, 0.0f, mOriginalPosZ));// * 
		       //glm::rotate(glm::mat4(), (float)modelThetaY, glm::vec3(0.0f, 1.0f, 0.0f)) *
		       //glm::rotate(glm::mat4(), (float)modelThetaX, glm::vec3(1.0f, 0.0f, 0.0f));
	// Copy to Reflection mProgram
	glUseProgram(mProgram);

        glUniform1i(glGetUniformLocation(mProgram, "frag_is_light_source"), 1);
	// Copy Model Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_Model"), 1, GL_FALSE, glm::value_ptr(mModelMatrix));
	// Copy View Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_View"),  1, GL_FALSE, glm::value_ptr(mCamera->getViewMatrix()));
	HeavenlyBody::Draw();
}
