/*
 * uranus.h
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef URANUS_H
#define URANUS_H

#include <planet.h>

class Uranus : public Planet
{
public:
	Uranus(Camera *camera);
	virtual ~Uranus();
};
#endif

