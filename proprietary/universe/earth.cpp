/*
 * earth.cpp
 *
 *  Created on: Wed Jun  6 19:44:31 NZST 2018
 *      Author: Frederick Tan
 */

#include <earth.h>

Earth::Earth(Camera *camera)
	: Planet(camera, 0.000006371 * 1000, "images/2k_earth_daymap.jpg")
{
	mEccentricity = 0.017;
	mSemiMajor = 0.01496;
	mRevolutionInSec = 365.26;
};

Earth::~Earth()
{
};

