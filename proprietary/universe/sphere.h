/**
 * @file: Sphere.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_SPHERE_H__
#define __OPENGLWRAPPER_SPHERE_H__

class Sphere
{
public:
	Sphere();
	virtual ~Sphere();
	virtual bool Create();
};
#endif
