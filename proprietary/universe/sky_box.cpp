/*
 * sky_box.cpp
 *
 *  Created on: Mon Jun  4 23:23:03 NZST 2018
 *      Author: Frederick Tan
 */

#include <sky_box.h>
#include <shader.h>
#include <opengl_util.h>

#include <iostream>

using namespace std;

SkyBox::SkyBox(Camera *mCamera, const std::string& directory, const std::string& mFilenamesPosX, const std::string& mFilenamesNegX, const std::string& mFilenamesPosY, const std::string& mFilenamesNegY, const std::string& mFilenamesPosZ, const std::string& mFilenamesNegZ)
	: mDirectory(directory)
	, mVao(0)
	, mVbo(0)
	, mEbo(0)
	, mCamera(mCamera)
{
	mFilenames[fiPosX] = mFilenamesPosX;
	mFilenames[fiNegX] = mFilenamesNegX;
	mFilenames[fiPosY] = mFilenamesPosY;
	mFilenames[fiNegY] = mFilenamesNegY;
	mFilenames[fiPosZ] = mFilenamesPosZ;
	mFilenames[fiNegZ] = mFilenamesNegZ;
	Init();
};

SkyBox::~SkyBox()
{
	glDeleteVertexArrays(1, &mVao);
	glDeleteBuffers(1, &mVbo);
	glDeleteBuffers(1, &mEbo);
	glDeleteProgram(mProgram);
};

void SkyBox::Init()
{
	LoadTextureCubeMap();
	Shader shader("./shader/skybox.vert.glsl",  "", "", "", "./shader/skybox.frag.glsl");
	mProgram = shader.GetProgram();
	glUseProgram(mProgram);
	CreateSkybox();
	// Create VAO, VBO & EBO
	glGenVertexArrays(1, &mVao);
	glGenBuffers(1, &mVbo);
	glGenBuffers(1, &mEbo);

	// Bind VAO, VBO & EBO
	glBindVertexArray(mVao);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEbo);

	// Load Vertex Data
	glBufferData(GL_ARRAY_BUFFER, mBuffer.size() * sizeof(glm::vec4), mBuffer.data(), GL_STATIC_DRAW);

	// Load Element Data
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size() * sizeof(glm::ivec3), mIndices.data(), GL_STATIC_DRAW);

	// Get Position Attribute location (must match name in shader)
	GLuint posLoc = glGetAttribLocation(mProgram, "vert_Position");

	// Set Vertex Attribute Pointers
	glVertexAttribPointer(posLoc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);

	// Enable Vertex Attribute Arrays
	glEnableVertexAttribArray(posLoc);

	// Unbind VAO, VBO & EBO
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	// Set Sample Texture Unit
	
	glUniform1i(glGetUniformLocation(mProgram, "u_texture_Map"), 0);

	// Copy Orientation Matrix to Skybox shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_View"),  1, GL_FALSE, glm::value_ptr(mCamera->getOrientationMatrix()));

	// ----------------------------------------
	// Projection Matrix
	// ----------------------------------------
	glm::mat4 projectionMatrix;
	
	// Calculate Perspective Projection
	projectionMatrix = glm::perspective(glm::radians(67.0f), 1.0f, 0.2f, -50.0f);

	// Copy Projection Matrix to Shader
	//glUseProgram(mProgram);
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_Projection"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
}

void SkyBox::Draw()
{
	// Copy to Skybox mProgram
	glUseProgram(mProgram);

	// Copy Skybox View Matrix to Shader
	glUniformMatrix4fv(glGetUniformLocation(mProgram, "u_View"),  1, GL_FALSE, glm::value_ptr(mCamera->getOrientationMatrix()));

	// Bind Vertex Array Object
	glBindVertexArray(mVao);

	// Disable Depth-Testing
	glDisable(GL_DEPTH_TEST);

	// Set active Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
		
	// Bind Texture Map
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTexture);

	// Draw Elements (Triangles)
	glDrawElements(GL_TRIANGLES, mIndices.size() * 3, GL_UNSIGNED_INT, NULL);

	// Renable Depth-Testing
	glEnable(GL_DEPTH_TEST);

	// Set active Texture Unit 0
	glActiveTexture(GL_TEXTURE0);

	// Unbind Texture Map
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	// Draw Elements (Triangles)
}

void SkyBox::LoadTextureCubeMap()
{
	// Generate mTexture
	glGenTextures(1, &mTexture);

	// Bind mTexture
	glBindTexture(GL_TEXTURE_CUBE_MAP, mTexture);

	// Load six faces
	for(int i = 0; i < 6; i++)
	{
		string filename = mDirectory + "/" + mFilenames[i];
		//cout << "Loading: " << filename << endl;
		// Load image from file
		unsigned char *image = OpenGLUtil::LoadImage(filename.c_str(), mWidth, mHeight, mN, true);

		// ----------------------------------------
		// No Mip-Mapping - Copy Image into mTexture
		// glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

		// ----------------------------------------
		// Mip-Mapping
		if(i == 0) {
			// Mip-Mapping
			int levels_x = (int)glm::log2((float)mWidth);
			int levels_y = (int)glm::log2((float)mHeight);
			int max_levels = glm::max(levels_x, levels_y);

			// Set storage - log_2(image size)
			glTexStorage2D(GL_TEXTURE_CUBE_MAP, max_levels, GL_RGBA8, mWidth, mHeight);
		}

		// Copy image data into mTexture
		glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 0, 0, mWidth, mHeight, GL_RGBA, GL_UNSIGNED_BYTE, image);

		// ----------------------------------------

		// Delete image data
		delete[] image;
	}

	// ----------------------------------------
	// No Mip-Mapping
	// Configure Texture
	// glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	// glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE); 

	// ----------------------------------------
	// Mip-Mapping
	// Configure Texture
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE); 

	// Generate Mipmap
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	// ----------------------------------------

	// ------------------------------
	// Anistropic Filtering
	// ------------------------------
	// Get Maximum Anistropic level
	GLfloat maxAnistropy = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnistropy);

	// Enable Anistropic Filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnistropy);


	// Unbind mTexture
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void SkyBox::CreateSkybox()
{
	// Vertices
	mBuffer.push_back(glm::vec4(-10.0f,  10.0f, -10.0f,  10.0f)); // Left  - Top    - Back  - 0
	mBuffer.push_back(glm::vec4( 10.0f,  10.0f, -10.0f,  10.0f)); // Right - Top    - Back  - 1
	mBuffer.push_back(glm::vec4(-10.0f,  10.0f,  10.0f,  10.0f)); // Left  - Top    - Front - 2
	mBuffer.push_back(glm::vec4( 10.0f,  10.0f,  10.0f,  10.0f)); // Right - Top    - Front - 3
	mBuffer.push_back(glm::vec4(-10.0f, -10.0f, -10.0f,  10.0f)); // Left  - Bottom - Back  - 4
	mBuffer.push_back(glm::vec4( 10.0f, -10.0f, -10.0f,  10.0f)); // Right - Bottom - Back  - 5
	mBuffer.push_back(glm::vec4(-10.0f, -10.0f,  10.0f,  10.0f)); // Left  - Bottom - Front - 6
	mBuffer.push_back(glm::vec4( 10.0f, -10.0f,  10.0f,  10.0f)); // Right - Bottom - Front - 7

	// Indexes
	mIndices.push_back(glm::ivec3(0, 2, 3)); // Top
	mIndices.push_back(glm::ivec3(0, 3, 1));
	mIndices.push_back(glm::ivec3(4, 5, 7)); // Bottom
	mIndices.push_back(glm::ivec3(4, 7, 6));
	mIndices.push_back(glm::ivec3(0, 2, 6)); // Left
	mIndices.push_back(glm::ivec3(0, 6, 4));
	mIndices.push_back(glm::ivec3(3, 1, 5)); // Right
	mIndices.push_back(glm::ivec3(3, 5, 7));
	mIndices.push_back(glm::ivec3(2, 3, 7)); // Front
	mIndices.push_back(glm::ivec3(2, 7, 6));
	mIndices.push_back(glm::ivec3(1, 0, 4)); // Back
	mIndices.push_back(glm::ivec3(1, 4, 5));
}
