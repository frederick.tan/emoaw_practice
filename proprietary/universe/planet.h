/*
 * planet.h
 *
 *  Created on: Wed Jun  6 16:23:17 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef PLANET_H
#define PLANET_H

#include <heavenly_body.h>

class Planet : public HeavenlyBody
{
public:
	Planet(Camera* camera, double radius, const std::string& textureFilename);
	virtual ~Planet();
	// Each planet has a defined way of moving and everytime this function is called
	// the planet should move following its own path
	virtual void Move(double dt, double modelThetaX, double modelThetaY);
private:
	double ComputeEccentricAnomaly(double ec, double m, double dp);
	double ComputeTrueAnomaly(double ec, double E, double dp);
protected:
	double mEccentricity;		// e
	double mMeanRadius;		// Km x 10^9
	double mSemiMajor;		// Km x 10^9
	double mRotationInSec;		// 1 sec == 1 day
	double mRevolutionInSec;	// 1 sec == 1 day
};
#endif
