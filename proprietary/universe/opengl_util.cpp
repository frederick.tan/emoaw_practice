/**
 * @file: opengl_util.cpp
 * @about: Wrapper class for OpenGL
 * @author: Frederick Tan
 **/

#include <opengl_util.h>

#include <memory>
#include <iostream>
#include <fstream>
#include <cmath>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

using namespace std;

GLFWwindow* OpenGLUtil::CreateWindow(int width, int height, const char *title, int major, int minor, GLFWmonitor *monitor, GLFWwindow *share)
{
	// Request an OpenGL context with specific features
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);

	// If Version is 3 or higher
	if (major >= 3)
	{
		// Request Forward Compatibility
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		// If version is 3.2 or higher
		if (major > 3 || minor >= 2)
		{
			// Request Core Profile
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		}
	}

	GLFWwindow *window = glfwCreateWindow(width, height, title, monitor, share);
	// Check Returned Window
	if (window == NULL) {
		return NULL;
	}

	// Set Context
	glfwMakeContextCurrent(window);

	return window;
}

unsigned char* OpenGLUtil::LoadImage(const char *filename, int &width, int &height, int &n, bool flip)
{
	// Force RGBA 
	int force_channels = 4;

	// Load image
	unsigned char *image = stbi_load(filename, &width, &height, &n, force_channels);

	// Channels forced to 4
	n = 4;

	// Check result
	if(!image) {
		// Print error message
		std::cerr << "Error: could not load image: " << filename << std::endl;
		
		// Return error
		return NULL;
	}

	// Temporary buffer
	unsigned char *t = new unsigned char[width*n];

	// If flip on
	if(flip) {
		// Flip image vertically
		for(int iy = 0; iy < height/2; iy++) {
			// Copy row iy into temporary buffer
			memcpy(t, &image[iy*width*n], width*n);

			// Copy row ((height-1)-iy) into row iy
			memcpy(&image[iy*width*n], &image[((height-1)-iy)*width*n], width*n);

			// Copy temporary buffer into row ((height-1)-iy)
			memcpy(&image[((height-1)-iy)*width*n], t, width*n);
		}
	}

	// Delete temporary buffer
	delete[] t;

	// Check dimensions are power-of-2
	if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
		// Print warning message
		std::cerr << "Warning: image " << filename << " is not power-of-2 dimensions" << std::endl;
	}

	// Print log message
	std::cout << "Loaded: " << filename << std::endl;

	// Return image
	return image;
}
