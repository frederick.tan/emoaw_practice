/*
 * interface_opengl_object.h
 *
 *  Created on: Wed Jun  6 16:23:17 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef INTERFACE_OPENGL_OBJECT_H
#define INTERFACE_OPENGL_OBJECT_H

#include <opengl_platform_headers.h>
#include <camera.h>

#include <string>
#include <vector>

class IOpenGLObject
{
public:
	IOpenGLObject()
	{
		mVao = 0;
		mVbo = 0;
		mEbo = 0;
	};
	virtual ~IOpenGLObject()
	{
		glDeleteVertexArrays(1, &mVao);
		glDeleteBuffers(1, &mVbo);
		glDeleteBuffers(1, &mEbo);
		glDeleteProgram(mProgram);
	};
protected:
	GLuint mProgram;
	GLuint mVao;
	GLuint mVbo;
	GLuint mEbo;
	std::vector<glm::vec4> mBuffer;
	std::vector<glm::ivec3> mIndices;
	glm::mat4 mModelMatrix;
	GLuint mTexture;
	std::string mTextureFilename;
};

#endif
