/*
 * pluto.cpp
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#include <pluto.h>

Pluto::Pluto(Camera *camera)
	: Planet(camera, 0.000001195 * 1000, "images/2k_pluto.png")
{
	mEccentricity = 0.248;
	mSemiMajor = 0.590638;
	mRevolutionInSec = 248.59 * 365.0;
};

Pluto::~Pluto()
{
};

