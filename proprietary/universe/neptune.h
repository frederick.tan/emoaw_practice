/*
 * neptune.h
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef NEPTUNE_H
#define NEPTUNE_H

#include <planet.h>

class Neptune : public Planet
{
public:
	Neptune(Camera *camera);
	virtual ~Neptune();
};
#endif

