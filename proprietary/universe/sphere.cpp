/**
 * @file: Sphere.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <sphere.h>

#include <math.h>
#include <stdio.h>

Sphere::Sphere()
{
}

Sphere::~Sphere()
{
	mBuffer.clear();
	mIndexes.clear();
}

bool Sphere::Create()
{
	bool status = true;
	GLfloat xOffset = mStartingPoint[0];
	GLfloat yOffset = mStartingPoint[1];
	GLfloat zOffset = mStartingPoint[2];
	// Longitude
	for (int i1 = 0; i1 < mDensity1; i1++)
	{
		// Theta [0, pi]
		float theta = i1 * M_PI / (mDensity1-1);

		// Longitude offset
		int offset1 = -mDensity2;

		// Latitude
		for (int i2 = 0; i2 < mDensity2; i2++)
		{
			// Phi [0, 2pi)
			float phi = i2 * M_PI * 2.0 / mDensity2;

			// Calculate point
			mBuffer.push_back(mSize * sin(theta) * cos(phi) + xOffset);	// x
			mBuffer.push_back(mSize * cos(theta) + yOffset);		// y
			mBuffer.push_back(mSize * sin(theta) * sin(phi) + zOffset);	// z
			mBuffer.push_back(mColor[0]);
			mBuffer.push_back(mColor[1]);
			mBuffer.push_back(mColor[2]);
			// Calculate normal

			// Latitude offset
			int offset2 = (i2 < (mDensity2 - 1)) ? 1 : -(mDensity2 - 1);

			// Add triangles between layers
			if (i1 > 0)
			{
				// Index of current vertex
				int k = i1 * mDensity2 + i2;

				// Add Indexes
				mIndexes.push_back(k + offset1); mIndexes.push_back(k); mIndexes.push_back(k + offset2);
				mIndexes.push_back(k + offset1); mIndexes.push_back(k + offset2); mIndexes.push_back(k + offset1 + offset2);
			}
		}
	}
	return status;
}
