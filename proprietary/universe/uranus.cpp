/*
 * uranus.cpp
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#include <uranus.h>

Uranus::Uranus(Camera *camera)
	: Planet(camera, 0.000025362 * 1000, "images/2k_uranus.jpg")
{
	mEccentricity = 0.047;
	mSemiMajor = 0.287246;
	mRevolutionInSec = 84.01 * 365.0;
};

Uranus::~Uranus()
{
};

