/*
 * sky_box.h
 *
 *  Created on: Mon Jun  4 23:23:03 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef SKY_BOX_H
#define SKY_BOX_H

#include <opengl_platform_headers.h>
#include <camera.h>
#include <memory>
#include <string>
#include <vector>

class SkyBox
{
private:
	enum EFilenameIndex
	{
		fiPosX,
		fiNegX,
		fiPosY,
		fiNegY,
		fiPosZ,
		fiNegZ,
		fiTotal
	};
public:
	SkyBox(Camera *camera,
		   const std::string& directory,
	       const std::string& filenamePosX,
	       const std::string& filenameNegX,
	       const std::string& filenamePosY,
	       const std::string& filenameNegY,
	       const std::string& filenamePosZ,
	       const std::string& filenameNegZ);
	virtual ~SkyBox();
	GLuint GetTexture() {return mTexture;};
	void GetDimentsions(int &width, int &height, int &n) { width = mWidth; height = mHeight; mN = n; };
	GLuint GetProgram() {return mProgram;};
	void CreateSkybox();
	void Draw();
private:
	void Init();
	void LoadTextureCubeMap();
private:
	const std::string mDirectory;
	std::string mFilenames[fiTotal];
	GLuint mTexture;
	GLuint mProgram;
	int mWidth;
	int mHeight;
	int mN;
	std::vector<glm::vec4> mBuffer;
	std::vector<glm::ivec3> mIndices;
	GLuint mVao;
	GLuint mVbo;
	GLuint mEbo;
	Camera *mCamera;
};
#endif
