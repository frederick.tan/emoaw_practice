/*
 * heavenly_body.h
 *
 *  Created on: Wed Jun  6 16:23:17 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef HEAVENLY_BODY_H
#define HEAVENLY_BODY_H

#include <interface_opengl_object.h>

#include <memory>

class HeavenlyBody : public IOpenGLObject
{
public:
	HeavenlyBody(Camera* camera, double radius, const std::string& textureFilename, double x = 0.0, double y = 0.0, double z = 0.0);
	virtual ~HeavenlyBody();
	// Each planet has a defined way of moving and everytime this function is called
	// the planet should move following its own path
	void Draw();
private:
	void Init();
protected:
	double mOriginalPosX;
	double mOriginalPosY;
	double mOriginalPosZ;
	double mRadius;
	double mEllapsedTime;
	Camera *mCamera;
};
#endif

