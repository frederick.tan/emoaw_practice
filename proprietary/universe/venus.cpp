/*
 * venus.cpp
 *
 *  Created on: Wed Jun  6 19:44:41 NZST 2018
 *      Author: Frederick Tan
 */

#include <venus.h>

Venus::Venus(Camera *camera)
	: Planet(camera, 0.0000060518 * 1000, "images/2k_venus_atmosphere.jpg")
{
	mEccentricity = 0.007;
	mSemiMajor = 0.010821;
	mRevolutionInSec = 224.7;
};

Venus::~Venus()
{
};
