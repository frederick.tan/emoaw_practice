/**
 * @file: opengl_util.h
 * @about: Wrapper class for OpenGL
 * @author: Frederick Tan
 **/

#ifndef OPENGL_UTIL_H
#define OPENGL_UTIL_H

#include <opengl_platform_headers.h>

#include <string.h>
#include <memory>

class OpenGLUtil
{
public:
	static GLFWwindow* CreateWindow(int width, int height, const char *title, int major, int minor, GLFWmonitor *monitor = 0, GLFWwindow *share = 0);
	static unsigned char* LoadImage(const char *filename, int &width, int &height, int &n, bool flip);
};

#endif
