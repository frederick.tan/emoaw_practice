/*
 * jupiter.h
 *
 *  Created on: Wed Jun  6 19:44:47 NZST 2018
 *      Author: Frederick Tan
 */

#ifndef JUPITER_H
#define JUPITER_H

#include <planet.h>

class Jupiter : public Planet
{
public:
	Jupiter(Camera *camera);
	virtual ~Jupiter();
};
#endif
