/*
 * saturn.cpp
 *
 *  Created on: Wed Jun  6 19:44:31 NZST 2018
 *      Author: Frederick Tan
 */

#include <saturn.h>

Saturn::Saturn(Camera *camera)
	: Planet(camera, 0.000058232 * 1000, "images/2k_saturn.jpg")
{
	mEccentricity = 0.056;
	mSemiMajor = 0.143353;
	mRevolutionInSec = 29.46 * 365.0;
};

Saturn::~Saturn()
{
};
