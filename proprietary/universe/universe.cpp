/**
 * Assignment number 3, 158.709, 2018 S1
 * Tan, Frederick Abril, 16227617
 * This program will create an empty world with only a flag in the middle
 **/

#include <opengl_platform_headers.h>
#include <camera.h>
#include <object_factory.h>
#include <opengl_util.h>
#include <shader.h>
#include <sky_box.h>

#include <earth.h>
#include <jupiter.h>
#include <mars.h>
#include <mercury.h>
#include <neptune.h>
#include <pluto.h>
#include <saturn.h>
#include <uranus.h>
#include <venus.h>
#include <sun.h>

#include <iostream>
#include <vector>

Camera *camera = nullptr;
static bool pause = true;
static float camera_movement_factor = 0.0;
// --------------------------------------------------------------------------------
// GLFW Callbacks
// --------------------------------------------------------------------------------

// Called on Error Event
void onError(int error, const char *description)
{
	// Print Error message
	std::cerr << "Error: " << error << " : " << description << std::endl;
}

// Called on Window Close Event
void onWindowClose(GLFWwindow *window)
{
	// Nothing to do right now
	// Do not call glfwDestroyWindow from here
}

// Called on Window Size Event
void onFramesunBufferSize(GLFWwindow *window, int width, int height)
{
	// Set-up the window/screen coordinates
	glfwMakeContextCurrent(window);
	glViewport(0, 0, width, height);
	glfwMakeContextCurrent(NULL);
}

// --------------------------------------------------------------------------------
// Keyboard Input
// --------------------------------------------------------------------------------
void onKey(GLFWwindow *window, int key, int scancode, int action, int mods) {
	// If user presses 'F'
	if((key == GLFW_KEY_F) && (action == GLFW_PRESS)) {
		// Set framebuffer to update
		//update_framebuffer = true;
	}

	// If user presses 'P'
	if((key == GLFW_KEY_P) && (action == GLFW_PRESS)) {
		// Pause / unpause
		pause = !pause;
	}

	// If user presses 'M'
	if((key == GLFW_KEY_M) && (action == GLFW_PRESS)) {
		// Switch Mode
		//mode = !mode;
		if (camera_movement_factor >= 10.0) camera_movement_factor = 0;
		else camera_movement_factor++;
	}

	// If user presses 'N'
	if((key == GLFW_KEY_N) && (action == GLFW_PRESS)) {
		// Decrease speed 
		if (camera_movement_factor <= 0.0) camera_movement_factor = 10.0;
		else camera_movement_factor--;
	}
}

// --------------------------------------------------------------------------------
// Mouse Input
// --------------------------------------------------------------------------------
void onMouseButton(GLFWwindow *window, int button, int action, int mods)
{
	// Update Camera
	camera->onMouseButton(window, button, action, mods);
}

void onCursorPosition(GLFWwindow *window, double x, double y)
{
	// Update Camera
	camera->onCursorPosition(window, x, y);
}

GLFWwindow *Init()
{
	glfwSetErrorCallback(onError);

	// Initialise GLFW
	if (!glfwInit()) return 0;

	// Set GLFW Window Hint - Full-Screen Antialiasing 16x
	glfwWindowHint(GLFW_SAMPLES, 16);

	// Create Window
	GLFWwindow *window = OpenGLUtil::CreateWindow(1000, 1000, "Assignment 03", 4, 3);

	// Check Window
	if (window == NULL) return 0;


#if (defined(_WIN32) || defined(__linux__))
	// Initialise GLEW
	GLenum glewInitResult = glewInit();
	if (glewInitResult != GLEW_OK) return 0;
	printf("glewInit %d\n", glewInitResult);
#endif

	// Enable multi-sampling - Antialiasing
	glEnable(GL_MULTISAMPLE);

	// Set window callback functions
	glfwSetFramebufferSizeCallback(window, onFramesunBufferSize);
	glfwSetWindowCloseCallback(window, onWindowClose);

	// Set keyboard input callback functions
	glfwSetKeyCallback(window, onKey);

	// Set mouse input callback functions
	glfwSetMouseButtonCallback(window, onMouseButton);
	glfwSetCursorPosCallback(window, onCursorPosition);

	// ----------------------------------------
	// Initialise OpenGL
	// ----------------------------------------
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	return window;
}

int main(int argc, char *argv[])
{
    GLFWwindow *window = Init();
    if (window == 0) return 1;
    printf("Init done\n");

    camera = new GimbalFreeLookCamera(window, glm::vec3(10.0f, 0.0f, 20.0));

    SkyBox *skybox = new SkyBox(camera, "images", "starfield_lf.tga", "starfield_rt.tga",
    		"starfield_up.tga", "starfield_dn.tga", "starfield_ft.tga", "starfield_bk.tga");

    //GLuint texture = skybox->GetTexture();
    Earth *earth = new Earth(camera);
    Jupiter *jupiter = new Jupiter(camera);
    Mars *mars = new Mars(camera);
    Mercury *mercury = new Mercury(camera);
    Neptune *neptune = new Neptune(camera);
    Pluto *pluto = new Pluto(camera);
    Saturn *saturn = new Saturn(camera);
    Uranus *uranus = new Uranus(camera);
    Venus *venus = new Venus(camera);
    Sun *sun = new Sun(camera);
        
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // Create light components
    GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8, 1.0f };
    GLfloat specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    GLfloat position[] = { 0.5f, 0.0f, 0.5f, 1.0f };

    // Assign created components to GL_LIGHT0
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    // ----------------------------------------
    // Skybox
    // ----------------------------------------
    float modelThetaX =  0.4f;
    float modelThetaY =  0.0f;
    float time = glfwGetTime();

    while (!glfwWindowShouldClose(window))
    {
	// Make the context of the given window current on the calling thread
	glfwMakeContextCurrent(window);

	// Set clear (background) colour to black
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Clear Screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Update Time
	float current_time = glfwGetTime();
	float dt = current_time - time;
	time = current_time;

	dt /= pow(10, camera_movement_factor);

	// ----------------------------------------
	// Update Model Matrix
	if(glfwGetKey(window, GLFW_KEY_UP))
        {
            // Key - Up Arrow
            modelThetaX -= 1.5f * dt;
	}
	if(glfwGetKey(window, GLFW_KEY_DOWN))
        {
            // Key - Down Arrow
            modelThetaX += 1.5f * dt;
	}
        if(glfwGetKey(window, GLFW_KEY_LEFT))
        {
	    // Key - Left Arrow
	    modelThetaY -= 1.5f * dt;
	}
	if(glfwGetKey(window, GLFW_KEY_RIGHT))
        {
	    // Key - Right Arrow
	    modelThetaY += 1.5f * dt;
	}
	// ----------------------------------------
	// ----------------------------------------
	// Update Camera
	//dynamic_cast<GimbalFreeLookCamera*>(camera)->moveLeft(dt);
	camera->update(dt);

	if (pause) dt = 0.0;
	// ----------------------------------------
	// ----------------------------------------
	// Recompute Model Matrix
	earth->Move(dt, modelThetaX, modelThetaY);
	jupiter->Move(dt, modelThetaX, modelThetaY);
	mars->Move(dt, modelThetaX, modelThetaY);
	mercury->Move(dt, modelThetaX, modelThetaY);
	neptune->Move(dt, modelThetaX, modelThetaY);
	pluto->Move(dt, modelThetaX, modelThetaY);
	saturn->Move(dt, modelThetaX, modelThetaY);
	uranus->Move(dt, modelThetaX, modelThetaY);
	venus->Move(dt, modelThetaX, modelThetaY);
	// ----------------------------------------
	// Draw Object
	// ----------------------------------------
	skybox->Draw();
	earth->Draw();
	jupiter->Draw();
	mars->Draw();
	mercury->Draw();
	neptune->Draw();
	pluto->Draw();
	saturn->Draw();
	uranus->Draw();
        venus->Draw();
	sun->Draw(dt, modelThetaX, modelThetaY);
	// ----------------------------------------

	// Swap the back and front buffers
	glfwSwapBuffers(window);

	// Poll window events
	glfwPollEvents();
}
	delete skybox;
	delete earth;
	delete jupiter;
	delete mars;
	delete mercury;
	delete neptune;
	delete pluto;
	delete saturn;
	delete uranus;
	delete venus;
	delete sun;
	sun = 0;
	delete camera;
	camera = 0;

	// Stop receiving events for the window and free resources; this must be
	// called from the main thread and should not be invoked from a callback
	glfwDestroyWindow(window);

	// Terminate GLFW
	glfwTerminate();
	return 0;
}
