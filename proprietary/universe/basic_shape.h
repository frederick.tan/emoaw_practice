/**
 * @file: BasicShape.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_BASIC_SHAPE_H__
#define __OPENGLWRAPPER_BASIC_SHAPE_H__

#include <opengl_platform_headers.h>

#include <vector>

class BasicShape
{
public:
	typedef std::vector<GLfloat> ObjectData;
	typedef std::vector<GLuint> ObjectIndexes;
public:
	BasicShape();
	virtual ~BasicShape();
	virtual bool Create() = 0;
protected:
	GLfloat mSize;
	GLint mDensity1;
	GLint mDensity2;
	ObjectData mBuffer;
	ObjectIndexes mIndexes;
	GLfloat mColor[3];
	GLfloat mStartingPoint[3];
};
#endif
