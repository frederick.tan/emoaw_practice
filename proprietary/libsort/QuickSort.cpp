/**
 * File: QuickSort.cpp
 * Description: Sorts numbers
 * Created by: Frederick Tan 
 **/

#include <QuickSort.h>

template <class T>
QuickSort<T> ::QuickSort()
{
}

template <class T>
QuickSort<T>::~QuickSort()
{
}

template <class T>
bool QuickSort<T>::Sort(std::vector<T> &vect, int m, int n)
{
	T key;
	int i, j, k;

	if (m < n)
	{
		k = SelectPivot(m, n);
		Swap(vect[m], vect[k]);
		key = vect[m];
		i = m + 1;
		j = n;
		while (i <= j)
		{
			while((i <= n) && (vect[i] <= key)) i++;
			while((j >= m) && (vect[j] > key)) j--;
			if (i < j) Swap(vect[i], vect[j]);
		}
		Swap(vect[m], vect[j]);
		Sort(vect, m, j - 1);
		Sort(vect, j + 1, n);
	}

	return true;
}
