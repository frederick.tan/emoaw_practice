/**
 * File: QuickSort.h
 * Description: Sorts numbers
 * Created by: Frederick Tan 
 **/
#ifndef _QUICKSORT_H_
#define _QUICKSORT_H_

#include <vector>

template <class T>
class QuickSort
{
public:
	QuickSort();
	virtual ~QuickSort();
	void AddObject(T object) { mObjectList.push_back(object); };
	bool Sort() { return Sort(mObjectList, 0, mObjectList.size() - 1); };
	std::vector <T>GetObjectList() { return mObjectList; };
private:
	void Swap(T &obj1, T &obj2) { T temp; temp = obj1; obj1 = obj2; obj2 = temp; };
	int SelectPivot(int i, int j) { return ((i / j) / 2);};
	bool Sort(std::vector<T> &vect, int m, int n);
private:
	std::vector <T> mObjectList;
};

#endif
