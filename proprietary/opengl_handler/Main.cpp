/**
 * @file: Main.cpp
 * @brief:
 * @author: Frederick Tan
 **/

#include <OpenGLWrapper.h>
#include <FlagWorld.h>
#include <GroundWorld.h>
#include <BasicShader.h>
#include <Transform.h>
#include <Flag.h>
#include <Sphere.h>
#include <Triangle.h>

using namespace OpenGLWrapper;

int main(int argc, char *argv[])
{
	unsigned int speedFactor = 1;
	if (argc == 2)
	{
		if (argv[1][0] == 'h')
		{
			printf("Usage: %s <speed>\n", argv[0]);
			printf("Where <speed> is the speed factor from 1 to 10 (1x to 10x)\n");
		}
		else
		{
			speedFactor = atoi(argv[1]);
			if (speedFactor > 10) speedFactor = 10;
		}
	}
	OpenGLWrapper::OpenGLWrapper oglw(800, 800, "Fred Test");
	BasicShader shader;
	BasicShader shader2;
	FlagWorld flagWorld;
	GroundWorld groundWorld;

	flagWorld.CreateScene();
	groundWorld.CreateScene();
	shader.LoadObjectData(flagWorld);
	shader.UseProgram();

	// Model Matrix
	float modelMatrix[16];
	Transform::Translate(0.0f, 0.0f, 0.0f, modelMatrix);
	//shader.SetMatrix(Shader::mtModel, modelMatrix);
	// ----------------------------------------

	// View Matrix
	float viewMatrix[16];
	float viewPosition[3] = { 0.0f,  0.0f,  0.5f};
	float viewUp[3]       = { 0.0f,  1.0f,  0.0f};
	float viewForward[3]  = { 0.0f,  0.0f, -1.0f};

	Transform::Normalize(viewUp, viewUp);
	Transform::Normalize(viewForward, viewForward);

	// identity(viewMatrix);
	Transform::View(viewPosition, viewForward, viewUp, viewMatrix);
	shader.SetMatrix(Shader::mtView, viewMatrix);

	// Projection Matrix
	float projectionMatrix[16];
	Transform::Identity(projectionMatrix);
	shader.SetMatrix(Shader::mtProjection, projectionMatrix);

	while (!oglw.ShouldCloseWindow())
	{
		// Make the context of the given window current on the calling thread
		oglw.MakeContextCurrent();

		oglw.ClearWindow();

		// Use Program
		shader.UseProgram();

		// // ----------------------------------------
		// // Rotation Matrix - X
		float rotation[16];
		Transform::RotateY(speedFactor * glfwGetTime() / 10, rotation);

		shader.SetMatrix(Shader::mtModel, rotation);

		glBindVertexArray(shader.Vao());

		// Draw Elements (Triangles)
		glDrawElements(GL_TRIANGLES, flagWorld.IndexesSize(), GL_UNSIGNED_INT, NULL);

		//shader2.UseProgram();

		// Swap the back and front buffers
		oglw.SwapBuffers();

		// Poll window events
		glfwPollEvents();
	}
	return 0;
}
