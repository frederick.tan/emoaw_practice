/**
 * @file: BasicShape.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_BASIC_SHAPE_H__
#define __OPENGLWRAPPER_BASIC_SHAPE_H__

#include <opengl_platform_headers.h>

#include <vector>

namespace OpenGLWrapper
{

class BasicShape
{
public:
	typedef std::vector<GLfloat> ObjectData;
	typedef std::vector<GLuint> ObjectIndexes;
public:
	BasicShape();
	virtual ~BasicShape();
	const GLvoid* Data() { return (GLvoid*)mBuffer.data(); };
	GLsizeiptr DataSize() { return mBuffer.size() * sizeof(GLfloat); };
	const GLvoid* Indexes() { return (GLvoid*)mIndexes.data(); };
	GLsizeiptr IndexesSize() { return mIndexes.size() * sizeof(GLuint); };
	void SetColor(const GLfloat r, const GLfloat g, const GLfloat b);
	void SetStartingPoint(const GLfloat x, const GLfloat y, const GLfloat z);
	void SetSize(const GLfloat size);
	void SetDensity(const GLint density1, const GLint density2);
	virtual bool Create() = 0;
protected:
	GLfloat mSize;
	GLint mDensity1;
	GLint mDensity2;
	ObjectData mBuffer;
	ObjectIndexes mIndexes;
	GLfloat mColor[3];
	GLfloat mStartingPoint[3];
};

};

#endif
