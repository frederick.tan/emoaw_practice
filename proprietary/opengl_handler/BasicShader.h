/**
 * @file: BasicBasicShader.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_BASIC_SHADER_H__
#define __OPENGLWRAPPER_BASIC_SHADER_H__

#include <Shader.h>

namespace OpenGLWrapper
{

class BasicShader : public Shader
{
public:
	BasicShader();
	virtual ~BasicShader();
private:
	virtual void SetAttributes();
private:
	GLuint mPositionLocation;
	GLuint mColourLocation;
};

};
#endif
