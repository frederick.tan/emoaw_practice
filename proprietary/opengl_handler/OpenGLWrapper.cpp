/**
 * @file: OpenGLWrapper.cpp
 * @about: Wrapper class for OpenGL
 * @author: Frederick Tan
 **/

#include <OpenGLWrapper.h>

#include <memory>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

namespace OpenGLWrapper
{

const int OpenGLWrapper::MajorVersion = 3;
const int OpenGLWrapper::MinorVersion = 2;

OpenGLWrapper::OpenGLWrapper(int width, int height, const char *title)
	: mWidth(width)
	, mHeight(height)
	, mWindow(nullptr)
	, mMonitor(nullptr)
	, mSharedWindow(nullptr)
	, mTitle(title)
{
	Init();
}

OpenGLWrapper::~OpenGLWrapper()
{
	glfwDestroyWindow(mWindow);
	glfwTerminate();
}

bool OpenGLWrapper::ShouldCloseWindow()
{
	return glfwWindowShouldClose(mWindow);
}

void OpenGLWrapper::ClearWindow()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGLWrapper::MakeContextCurrent()
{
	glfwMakeContextCurrent(mWindow);
}

void OpenGLWrapper::SwapBuffers()
{
	glfwSwapBuffers(mWindow);
}

void OpenGLWrapper::OnError(int error, const char *description)
{
	cerr << "Error: " << error << " : " << description << std::endl;
}

void OpenGLWrapper::OnWindowClose(GLFWwindow *window)
{
	// Nothing to do right now
	// Do not call glfwDestroyWindow from here
	if (window)
	{
		// Nothing to do here
	}
}

// Called on Window Size Event
void OpenGLWrapper::OnFramebufferSize(GLFWwindow *window, int width, int height)
{
	// Set-up the window/screen coordinates
	glfwMakeContextCurrent(window);
	glViewport(0, 0, width, height);
	glfwMakeContextCurrent(NULL);
}

bool OpenGLWrapper::CreateWindow()
{
	// Request an OpenGL context with specific features
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, MajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, MinorVersion);

	// If Version is 3 or higher
	if (MajorVersion >= 3)
	{
		// Request Forward Compatibility
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		// If version is 3.2 or higher
		if (MajorVersion > 3 || MinorVersion >= 2)
		{
			// Request Core Profile
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		}
	}

	mWindow = glfwCreateWindow(mWidth, mHeight, mTitle.c_str(), nullptr, nullptr);

	if (!mWindow) return false;
		
	// Set Context
	glfwMakeContextCurrent(mWindow);

	// Initialise GLEW
	if (glewInit() != GLEW_OK)
	{
		return false;
	}

	// Set window callback functions
	glfwSetFramebufferSizeCallback(mWindow, OpenGLWrapper::OnFramebufferSize);
	glfwSetWindowCloseCallback(mWindow, OpenGLWrapper::OnWindowClose);

	return true;
}

bool OpenGLWrapper::Init()
{
	// Set Error Callback
	glfwSetErrorCallback(OpenGLWrapper::OnError);

	if (!glfwInit())
	{
		cerr << "Error initialising GLFW" << endl;
		return false;
	}

	if (!CreateWindow())
	{
		cerr << "Error creating Window" << endl;
		return false;
	}

	// Initialise OpenGL
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	return true;
}
}
