/**
 *
 *
 *
 **/

#include <OpenGLWrapper.h>
#include <BasicShader.h>
#include <Transform.h>
#include <Flag.h>
#include <Sphere.h>
#include <Triangle.h>

using namespace OpenGLWrapper;

int main(int argc, char *argv[])
{
	OpenGLWrapper::OpenGLWrapper oglw(600, 600, "Fred Test");
	BasicShader shader;
	GLfloat color[] = {0.5f, 0.27f, 0.8f};
	Sphere sphere(0.14, 1000, 1000, color);
	shader.LoadObjectData(sphere);
	shader.UseProgram();

	// Model Matrix
	float modelMatrix[16];
	Transform::Translate(0.0f, 0.0f, 0.0f, modelMatrix);
	shader.SetMatrix(Shader::mtModel, modelMatrix);
	// ----------------------------------------

	// View Matrix
	float viewMatrix[16];
	float viewPosition[3] = { 0.0f,  0.0f,  0.5f};
	float viewUp[3]       = { 0.0f,  1.0f,  0.0f};
	float viewForward[3]  = { 0.0f,  0.0f, -1.0f};

	// float viewPosition[3] = { 0.0f,  1.0f,  0.2f};
	// float viewUp[3]       = { 0.0f,  0.1f, -1.0f};
	// float viewForward[3]  = { 0.0f, -1.0f, -0.1f};

	//float viewPosition[3] = { 1.0f,  0.0f,  1.0f};
	//float viewUp[3]       = { 0.0f,  1.0f,  0.0f};
	//float viewForward[3]  = { -0.5f,  0.0f, -1.0f};

	Transform::Normalize(viewUp, viewUp);
	Transform::Normalize(viewForward, viewForward);

	// identity(viewMatrix);
	Transform::View(viewPosition, viewForward, viewUp, viewMatrix);
	shader.SetMatrix(Shader::mtView, viewMatrix);

	// Projection Matrix
	float projectionMatrix[16];
	Transform::Identity(projectionMatrix);
	//Transform::Perspective(1.0f, 67.0f * M_PI / 180.0f, 0.2f, 10.0f, projectionMatrix);

	shader.SetMatrix(Shader::mtProjection, projectionMatrix);
	printf("DBG 004\n");

	//oglw.MakeContextCurrent();
	//getchar();

	while (!oglw.ShouldCloseWindow())
	{
		// Make the context of the given window current on the calling thread
		oglw.MakeContextCurrent();

		oglw.ClearWindow();

		// Use Program
		shader.UseProgram();

		// // ----------------------------------------
		// // Rotation Matrix - X
		float rotation[16];
		Transform::RotateY(glfwGetTime() * 10, rotation);

		//shader.SetMatrix(Shader::mtModel, rotation);

		// // ----------------------------------------

		// Bind Vertex Array Object
		//glBindVertexArray(vao);
		shader.BindVertexArray(sphere);

		// Draw Elements (Triangles)
		glDrawElements(GL_TRIANGLES, sphere.IndexesSize(), GL_UNSIGNED_INT, NULL);

		// Swap the back and front buffers
		oglw.SwapBuffers();

		// Poll window events
		glfwPollEvents();
	}
	return 0;
}
