/**
 * @file: Transform.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_TRANSFORM_H__
#define __OPENGLWRAPPER_TRANSFORM_H__

namespace OpenGLWrapper
{

class Transform
{
public:
	Transform();
	virtual ~Transform();
public:
	static void Identity(float I[16]);
	static void Translate(float tx, float ty, float tz, float T[16]);
	static void RotateX(float theta, float Rx[16]);
	static void RotateY(float theta, float Ry[16]);
	static void RotateZ(float theta, float Rz[16]);
	static void Rotate(float theta, float rx, float ry, float rz, float R[16]);
	static void Scale(float sx, float sy, float sz, float S[16]);
	static void Multiply44(float a[16], float b[16], float c[16]);
	static void Multiply3(float s, float u[3], float v[3]);
	static void Multiply4(float s, float u[4], float v[4]);
	static float Length3(float v[3]);
	static float Length4(float v[4]);
	static void Normalize(float v[3], float u[3]);
	static void CrossProduct(float a[3], float b[3], float c[3]);
	static float DotProduct3(float a[3], float b[3]);
	static float DotProduct4(float a[4], float b[4]);
	static void View(float p[4], float f[3], float u[3], float V[16]);
	static void Orthographic(float width, float height, float near1, float far1, float matrix[16]);
	static void Perspective(float aspect, float fov, float near1, float far1, float matrix[16]);
};

};

#endif
