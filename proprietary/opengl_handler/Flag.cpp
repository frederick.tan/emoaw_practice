/**
 * @file: Flag.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <Flag.h>

#include <math.h>
#include <stdio.h>

namespace OpenGLWrapper
{

Flag::Flag()
{
}

Flag::~Flag()
{
	mBuffer.clear();
	mIndexes.clear();
}

bool Flag::Create()
{
	bool status = true;
	const GLfloat originalHeight = 0.5f;
	const GLfloat originalLength = 1.0f;
	const GLfloat ampFactor = 80.0;
	GLuint index = 0;
	GLfloat z = mStartingPoint[2];
	// Original size of the flag is 1 length x 0.5 height
	GLfloat xOffset = mStartingPoint[0];
	GLfloat xFactor = 1.7f / mSize;
	GLfloat newLength = originalLength * mSize;
	GLfloat xStep = newLength / mDensity2;
	GLfloat yStart = mStartingPoint[1];
	GLfloat newHeight = originalHeight * mSize;
	GLfloat yStep = newHeight/mDensity1;
	GLfloat yEnd = yStart - newHeight;	// The direction is down

	GLfloat yOffset1 = yStart;
	GLfloat yOffset2 = yOffset1 - yStep;
	GLfloat r = mColor[0];
	GLfloat g = mColor[1];
	GLfloat b = mColor[2];
	while (yOffset2 > yEnd)
	{
		for (int j = 0; j < mDensity2; j++)
		{
			float theta1 = j * M_PI * xFactor / mDensity2;
			float theta2 = (j + 1) * M_PI * xFactor / mDensity2;
			GLfloat x1 = (xStep * j) + xOffset;
			GLfloat x2 = xStep * (j + 1) + xOffset;
			float y1 = sin(theta1) / (ampFactor) + yOffset1;
			float y2 = sin(theta1) / (ampFactor) + yOffset2;
			float y3 = sin(theta2) / (ampFactor) + yOffset1;
			float y4 = sin(theta2) / (ampFactor) + yOffset2;
			mBuffer.push_back(x1); mBuffer.push_back(y1); mBuffer.push_back(z);
			mBuffer.push_back(r); mBuffer.push_back(g); mBuffer.push_back(b);

			mBuffer.push_back(x2); mBuffer.push_back(y3); mBuffer.push_back(z);
			mBuffer.push_back(r); mBuffer.push_back(g); mBuffer.push_back(b);

			mBuffer.push_back(x1); mBuffer.push_back(y2); mBuffer.push_back(z);
			mBuffer.push_back(r); mBuffer.push_back(g); mBuffer.push_back(b);

			mBuffer.push_back(x2); mBuffer.push_back(y4); mBuffer.push_back(z);
			mBuffer.push_back(r); mBuffer.push_back(g); mBuffer.push_back(b);

			mIndexes.push_back(index); mIndexes.push_back(index + 2); mIndexes.push_back(index + 3);
			mIndexes.push_back(index); mIndexes.push_back(index + 1); mIndexes.push_back(index + 3);
			index += 4;
		}
		yOffset1 -= yStep;
		yOffset2 -= yStep;
	}
	return status;
}
};
