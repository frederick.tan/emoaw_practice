/**
 * @file: Sphere.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_SPHERE_H__
#define __OPENGLWRAPPER_SPHERE_H__

#include <BasicShape.h>

namespace OpenGLWrapper
{

class Sphere : public BasicShape
{
public:
	Sphere();
	virtual ~Sphere();
	virtual bool Create();
};
};
#endif
