/**
 * @file: Ground.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_GROUND_H__
#define __OPENGLWRAPPER_GROUND_H__

#include <BasicShape.h>

namespace OpenGLWrapper
{

class Ground : public BasicShape
{
public:
	Ground();
	virtual ~Ground();
private:
	virtual bool Create();
};

};

#endif
