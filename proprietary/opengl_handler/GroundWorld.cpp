/**
 * @file: GroundWorld.cpp
 * @about: This is file creates a 3d Scene with one flag
 * @author: Frederick Tan
 **/

#include <GroundWorld.h>
#include <BasicShape.h>
#include <ObjectFactory.h>

#include <stdio.h>

namespace OpenGLWrapper
{

GroundWorld::GroundWorld()
{
}

GroundWorld::~GroundWorld()
{
	for (auto &a: mShapes)
	{
		delete a;
		printf("Deleted object\n");
	}
	mShapes.clear();
}

bool GroundWorld::CreateScene()
{
	BasicShape *ground = ObjectFactory::MakeObject(ObjectFactory::oidGround);
	ground->SetColor(0.3f, 0.0f, 0.7f);
	ground->SetStartingPoint(0.0f, 0.0f, 0.0f);
	ground->SetSize(0.9);
	ground->SetDensity(1, 1);
	ground->Create();
	mShapes.push_back(ground);
	return true;
}

}

