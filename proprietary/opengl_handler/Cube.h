/**
 * @file: Cube.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_CUBE_H__
#define __OPENGLWRAPPER_CUBE_H__

#include <BasicShape.h>

namespace OpenGLWrapper
{

class Cube : public BasicShape
{
public:
	Cube();
	virtual ~Cube();
private:
	bool Create();
};

};

#endif
