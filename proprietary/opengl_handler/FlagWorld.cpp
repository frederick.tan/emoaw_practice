/**
 * @file: FlagWorld.cpp
 * @about: This is file creates a 3d Scene with one flag
 * @author: Frederick Tan
 **/

#include <FlagWorld.h>
#include <BasicShape.h>
#include <ObjectFactory.h>

#include <stdio.h>

namespace OpenGLWrapper
{

FlagWorld::FlagWorld()
{
}

FlagWorld::~FlagWorld()
{
	for (auto &a: mShapes)
	{
		delete a;
	}
	mShapes.clear();
}

bool FlagWorld::CreateScene()
{
	BasicShape *ground = ObjectFactory::MakeObject(ObjectFactory::oidGround);
	ground->SetColor(0.00f, 1.0f, 0.00f);
	ground->SetStartingPoint(0.0f, -0.12f, 0.0f);
	ground->SetSize(0.9);
	ground->SetDensity(1, 1);
	ground->Create();
	mShapes.push_back(ground);

	BasicShape *pole = ObjectFactory::MakeObject(ObjectFactory::oidPole);
	pole->SetColor(0.12f, 0.12f, 0.12f);
	pole->SetStartingPoint(-0.02f, -0.25f, 0.0f);
	pole->SetSize(0.0162f);
	pole->SetDensity(10, 10);
	pole->Create();
	mShapes.push_back(pole);

	BasicShape *sphere = ObjectFactory::MakeObject(ObjectFactory::oidSphere);
	sphere->SetColor(1.0f, 1.0f, 0.0f);
	sphere->SetStartingPoint(-0.02f, 0.55f, 0.0f);
	sphere->SetSize(0.03f);
	sphere->SetDensity(20, 30);
	sphere->Create();
	mShapes.push_back(sphere);

	BasicShape *flag = ObjectFactory::MakeObject(ObjectFactory::oidFlag);
	flag->SetColor(1.0f, 0.0f, 0.0f);
	flag->SetStartingPoint(0.0f, 0.5f, 0.0f);
	flag->SetSize(0.4f);
	flag->SetDensity(20, 50);
	flag->Create();
	mShapes.push_back(flag);

	return true;
}

}

