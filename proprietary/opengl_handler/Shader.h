/**
 * @file: Shader.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_SHADER_H__
#define __OPENGLWRAPPER_SHADER_H__

#include <opengl_platform_headers.h>

#include <string>

namespace OpenGLWrapper
{

class BasicShape;
class SceneStager;

class Shader
{
public:
	enum EMatrixType
	{
		mtModel		= 0,
		mtView		= 1,
		mtProjection	= 2,
		mtUnknown	= 255
	};
public:
	Shader(const char *vertexFilename, const char *controlFilename, const char *evaluationFileName, const char *geometryFilename, const char *fragmentFilename);
	virtual ~Shader();
	GLuint GetProgram() { return mProgram; };
	void UseProgram();
	void SetMatrix(EMatrixType type, float matrix[16]);
	void LoadObjectData(SceneStager &scene);
	GLuint Vao() {return mVao; };
	GLuint Vbo() {return mVbo; };
	GLuint Ebo() {return mEbo; };
protected:
	virtual void SetAttributes() = 0;
private:
	char *ReadFile(const char *filename);
	GLuint CheckShader(GLuint shader);
	GLuint CheckProgram(GLuint program);
	GLuint LoadShader(GLuint type, const char *filename);
	void LoadProgram();
protected:
	GLuint mProgram;
private:
	GLuint mVao;
	GLuint mVbo;
	GLuint mEbo;
	const std::string mVertexFileName;
	const std::string mControlFileName;
	const std::string mEvaluationFileName;
	const std::string mGeometryFileName;
	const std::string mFragmentFileName;
};

};
#endif
