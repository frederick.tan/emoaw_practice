/**
 * @file: ObjectFactory.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_OBJECT_FACTORY_H__
#define __OPENGLWRAPPER_OBJECT_FACTORY_H__

#include <opengl_platform_headers.h>

#include <vector>

namespace OpenGLWrapper
{

class BasicShape;

class ObjectFactory
{
public:
	enum EObjectId
	{
		oidTriangle	= 0,
		oidSphere	= 1,
		oidCube		= 2,
		oidFlag		= 100,
		oidPole		= 101,
		oidGround	= 102,
	};
public:
	ObjectFactory();
	virtual ~ObjectFactory();
	static BasicShape *MakeObject(EObjectId oid);
};

};

#endif
