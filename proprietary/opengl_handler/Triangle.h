/**
 * @file: Triangle.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_TRIANGLE_H__
#define __OPENGLWRAPPER_TRIANGLE_H__

#include <BasicShape.h>

namespace OpenGLWrapper
{

class Triangle : public BasicShape
{
public:
	Triangle();
	virtual ~Triangle();
private:
	virtual bool Create();
};

};

#endif
