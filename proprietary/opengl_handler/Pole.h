/**
 * @file: Pole.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_POLE_H__
#define __OPENGLWRAPPER_POLE_H__

#include <BasicShape.h>

namespace OpenGLWrapper
{

class Pole : public BasicShape
{
public:
	Pole();
	virtual ~Pole();
private:
	virtual bool Create();
};

};

#endif
