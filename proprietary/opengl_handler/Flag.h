/**
 * @file: Flag.h
 * @about:
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_FLAG_H__
#define __OPENGLWRAPPER_FLAG_H__

#include <BasicShape.h>

namespace OpenGLWrapper
{

class Flag : public BasicShape
{
public:
	Flag();
	virtual ~Flag();
private:
	virtual bool Create();
};

};

#endif
