/**
 * @file: GroundWorld.h
 * @about: This is file creates a 3d Scene with one flag
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_GROUND_WORLD_H__
#define __OPENGLWRAPPER_GROUND_WORLD_H__

#include <SceneStager.h>

namespace OpenGLWrapper
{

class GroundWorld : public SceneStager
{
public:
	GroundWorld();
	virtual ~GroundWorld();
	virtual bool CreateScene();
};

};

#endif
