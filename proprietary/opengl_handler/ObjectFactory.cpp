/**
 * @file: ObjectFactory.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <ObjectFactory.h>
#include <Cube.h>
#include <Flag.h>
#include <Ground.h>
#include <Pole.h>
#include <Sphere.h>
#include <Triangle.h>

#include <stdio.h>

namespace OpenGLWrapper
{

ObjectFactory::ObjectFactory()
{
}

ObjectFactory::~ObjectFactory()
{
}

BasicShape *ObjectFactory::MakeObject(EObjectId oid)
{
	BasicShape *object = nullptr;
	switch (oid)
	{
	case oidCube:
	{
		object = new Cube;
		break;
	}
	case oidFlag:
	{
		object = new Flag;
		break;
	}
	case oidGround:
	{
		object = new Ground;
		break;
	}
	case oidPole:
	{
		object = new Pole;
		break;
	}
	case oidSphere:
	{
		object = new Sphere;
		break;
	}
	case oidTriangle:
	{
		object = new Triangle;
		break;
	}
	}
	return object;
}

};
