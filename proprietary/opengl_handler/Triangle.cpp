/**
 * @file: Triangle.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <Triangle.h>

#include <stdio.h>

namespace OpenGLWrapper
{

Triangle::Triangle()
{
}

Triangle::~Triangle()
{
}

bool Triangle::Create()
{
	bool status = true;
	mIndexes.push_back(0);
	mIndexes.push_back(1);
	mIndexes.push_back(2);

	mBuffer.push_back(0.0f); mBuffer.push_back(0.577f); mBuffer.push_back(0.0f);
	mBuffer.push_back(1.0f); mBuffer.push_back(0.0f); mBuffer.push_back(0.0f);

	mBuffer.push_back(0.5f); mBuffer.push_back(-0.289f); mBuffer.push_back(0.0f);
	mBuffer.push_back(0.0f); mBuffer.push_back(1.0f); mBuffer.push_back(0.0f);

	mBuffer.push_back(-0.5f); mBuffer.push_back(-0.289f); mBuffer.push_back(0.0f);
	mBuffer.push_back(0.0f); mBuffer.push_back(0.0f); mBuffer.push_back(1.0f);

	return status;
}

};
