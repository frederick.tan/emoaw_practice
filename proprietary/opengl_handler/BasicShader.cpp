/**
 * @file: BasicShader.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <BasicShader.h>

#include <iostream>
#include <fstream>

using namespace std;

namespace OpenGLWrapper
{

BasicShader::BasicShader()
	: Shader("./shader/vert.glsl", "", "", "", "./shader/frag.glsl")
{
}

BasicShader::~BasicShader()
{
}

void BasicShader::SetAttributes()
{
	// Get Position Attribute location (must match name in shader)
	mPositionLocation = glGetAttribLocation(mProgram, "vert_Position");
	// Get Colour Attribute location (must match name in shader)
	mColourLocation = glGetAttribLocation(mProgram, "vert_Colour");
	// Set Vertex Attribute Pointers
	glVertexAttribPointer(mPositionLocation, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), NULL);
	glVertexAttribPointer(mColourLocation, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3*sizeof(float)));
	// Enable Vertex Attribute Arrays
	glEnableVertexAttribArray(mPositionLocation);
	glEnableVertexAttribArray(mColourLocation);
}

};
