/**
 * @file: Pole.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <Pole.h>

#include <math.h>
#include <stdio.h>

namespace OpenGLWrapper
{

Pole::Pole()
{
}

Pole::~Pole()
{
	mBuffer.clear();
	mIndexes.clear();
}

bool Pole::Create()
{
	bool status = true;
	GLfloat y = 0.0f;
	GLfloat r = mSize;
	GLfloat xOffset = mStartingPoint[0];
	GLfloat yOffset = mStartingPoint[1];
	GLfloat zOffset = mStartingPoint[2];
	for (int i1 = 0; i1 < mDensity1; i1++)
	{
		y = 0.75 * i1 / mDensity1 + yOffset;
		for (int i2 = 0; i2 < mDensity2; i2++)
		{
			float theta = i2 * M_PI * 2.0 / mDensity2;
			GLfloat x = r * cos(theta) + xOffset;
			GLfloat z = r * sin(theta) + zOffset;
			mBuffer.push_back(x); mBuffer.push_back(y); mBuffer.push_back(z);
			mBuffer.push_back(mColor[0]);
			mBuffer.push_back(mColor[1]);
			mBuffer.push_back(mColor[2]);
                      
			int offset2 = (i2 < (mDensity2 - 1)) ? 1 : -(mDensity2 - 1); 
			if (i1 > 0)
			{
				// Index of current vertex
				int k = i1 * mDensity2 + i2;
				mIndexes.push_back(k - mDensity1); mIndexes.push_back(k); mIndexes.push_back(k + offset2);
				mIndexes.push_back(k - mDensity1); mIndexes.push_back(k + offset2); mIndexes.push_back(k - mDensity1 + offset2);
			}
		}
	}
	return status;
}
};
