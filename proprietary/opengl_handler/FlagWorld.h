/**
 * @file: FlagWorld.h
 * @about: This is file creates a 3d Scene with one flag
 * @author: Frederick Tan
 **/

#ifndef __OPENGLWRAPPER_FLAG_WORLD_H__
#define __OPENGLWRAPPER_FLAG_WORLD_H__

#include <SceneStager.h>

namespace OpenGLWrapper
{

class FlagWorld : public SceneStager
{
public:
	FlagWorld();
	virtual ~FlagWorld();
	virtual bool CreateScene();
};

};

#endif
