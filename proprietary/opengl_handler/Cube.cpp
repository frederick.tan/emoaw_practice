/**
 * @file: Cube.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <Cube.h>

namespace OpenGLWrapper
{

Cube::Cube()
{
}

Cube::~Cube()
{
}

bool Cube::Create()
{
	bool status = true;
	mIndexes.push_back(0);
	mIndexes.push_back(1);
	mIndexes.push_back(2);

	mBuffer.push_back(0.0f); mBuffer.push_back(0.577f); mBuffer.push_back(0.0f);
	mBuffer.push_back(1.0f); mBuffer.push_back(0.0); mBuffer.push_back(0.0f);

	mBuffer.push_back(0.5f); mBuffer.push_back(-0.289); mBuffer.push_back(0.0f);
	mBuffer.push_back(0.0f); mBuffer.push_back(1.0); mBuffer.push_back(0.0f);

	mBuffer.push_back(-0.5f); mBuffer.push_back(-0.289); mBuffer.push_back(0.0f);
	mBuffer.push_back(0.0f); mBuffer.push_back(0.0); mBuffer.push_back(1.0f);

	return status;
}

};
