/**
 * @file: Ground.cpp
 * @about:
 * @author: Frederick Tan
 **/

#include <Ground.h>

#include <math.h>
#include <stdio.h>

namespace OpenGLWrapper
{

Ground::Ground()
{
}

Ground::~Ground()
{
	mBuffer.clear();
	mIndexes.clear();
}

bool Ground::Create()
{
	bool status = true;
	GLfloat y = mStartingPoint[1];

	mBuffer.push_back(-mSize); mBuffer.push_back(y); mBuffer.push_back(0.0f);
	mBuffer.push_back(mColor[0]); mBuffer.push_back(mColor[1]); mBuffer.push_back(mColor[2]);
	mBuffer.push_back(0.0f); mBuffer.push_back(y); mBuffer.push_back(mSize);
	mBuffer.push_back(mColor[0]); mBuffer.push_back(mColor[1]); mBuffer.push_back(mColor[2]);
	mBuffer.push_back(mSize); mBuffer.push_back(y); mBuffer.push_back(0.0f);
	mBuffer.push_back(mColor[0]); mBuffer.push_back(mColor[1]); mBuffer.push_back(mColor[2]);
	mBuffer.push_back(0.0f); mBuffer.push_back(y); mBuffer.push_back(-mSize);
	mBuffer.push_back(mColor[0]); mBuffer.push_back(mColor[1]); mBuffer.push_back(mColor[2]);

	mIndexes.push_back(0); mIndexes.push_back(1); mIndexes.push_back(2);
	mIndexes.push_back(0); mIndexes.push_back(2); mIndexes.push_back(3);
	return status;
}
};
