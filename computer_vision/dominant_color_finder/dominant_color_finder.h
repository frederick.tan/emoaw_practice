/*
 * dominant_color_finder.h
 *
 *  Created on: Sun Nov 18 09:45:27 NZDT 2018
 *      Author: Frederick Tan
 */

#ifndef DOMINANT_COLOR_FINDER_H
#define DOMINANT_COLOR_FINDER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <functional>
#include <set>
#include <string>

class DominantColorFinder {
public:
    DominantColorFinder(const std::string &filename, int nClusters = 8, int nThreads = 8);

    virtual ~DominantColorFinder();

public:
    void Process();

private:
    const std::string mFilename;
    int mNClusters;
    int mNThreads;
};

#endif
