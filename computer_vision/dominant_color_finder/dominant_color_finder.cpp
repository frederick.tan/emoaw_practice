/*
 * dominant_color_finder.cpp
 *
 *  Created on: Sun Nov 18 09:45:27 NZDT 2018
 *      Author: Frederick Tan
 */

#include <dominant_color_finder.h>

#include <chrono>
#include <iostream>

using namespace cv;
using namespace std;

DominantColorFinder::DominantColorFinder(const string &filename, int nClusters, int nThreads)
        : mFilename(filename), mNClusters(nClusters), mNThreads(nThreads) {
    if (mFilename.empty()) {
        throw invalid_argument("Missing filename");
    }
};

DominantColorFinder::~DominantColorFinder() {
};

void DominantColorFinder::Process() {

    auto start = chrono::system_clock::now();
    Mat3b img = imread(mFilename.c_str());

    int n = img.rows * img.cols;

    Mat data = img.reshape(1, n);
    data.convertTo(data, CV_32F);

    int x = 0;
    int y = 0;
    int width = 0;
    int height = 400;
    Mat canvas(height, 1000, CV_8UC3, Scalar(0));
    vector<int> labels;
    Mat1f colors;
    kmeans(data, mNClusters, labels, cv::TermCriteria(), 1, cv::KMEANS_PP_CENTERS, colors);

    vector<int> labelCount(mNClusters, 1);

    cv::setNumThreads(mNThreads);

    parallel_for_(Range(0, n), [&](const Range &range) {
        for (int i = range.start; i < range.end; i++) {
            data.at<float>(i, 0) = colors(labels[i], 0);
            data.at<float>(i, 1) = colors(labels[i], 1);
            data.at<float>(i, 2) = colors(labels[i], 2);
        }
    });

    for (auto label : labels) {
        labelCount[label] += 1;
    }

    sort(labelCount.begin(), labelCount.end(), [](const int &a, const int &b) -> bool { return a > b; });

    for (unsigned int i = 0; i <= labelCount.size(); i++) {
        double pct = (double) labelCount[i] / n;
        if (pct == 0) continue;
        width = (pct * 1000);
        cv::Rect rect(x, y, width, height);

        rectangle(canvas, rect, Scalar(colors(i, 0), colors(i, 1), colors(i, 2)), 8,
                  FILLED);
        Point point[1][4];
        point[0][0] = Point(x, y);
        point[0][1] = Point(x, height);
        point[0][2] = Point(x + width, height);
        point[0][3] = Point(x + width, y);
        const Point *ppt[2] = {point[0]};
        int npt[] = {4};
        fillPoly(canvas, ppt, npt, 1, Scalar(colors(i, 0), colors(i, 1), colors(i, 2)),
                 8);
        x += width;
    }

    chrono::duration<double> elapsed_seconds = chrono::system_clock::now() - start;

    cout << "Elapsed time: " << elapsed_seconds.count() << "s\n";

    imshow("Rectangles", canvas);
    imshow("Original", img);
    waitKey(0);
}

int main(int argc, char *argv[]) {
    DominantColorFinder *dominantColorFinder = nullptr;
    try {
        if (argc < 2) {
            cout << "Usage: " << argv[0] << "<filename> <optional: number of bins> <optional: number of threads>"
                 << endl;
        } else if (argc == 2) {
            dominantColorFinder = new DominantColorFinder(argv[1]);
        } else if (argc == 3) {
            dominantColorFinder = new DominantColorFinder(argv[1], atoi(argv[2]));
        } else if (argc == 4) {
            dominantColorFinder = new DominantColorFinder(argv[1], atoi(argv[2]), atoi(argv[3]));
        }
        if (dominantColorFinder) dominantColorFinder->Process();
    } catch (invalid_argument ex) {
        cout << ex.what() << endl;
    }
    if (dominantColorFinder) delete dominantColorFinder;
    return 0;
}