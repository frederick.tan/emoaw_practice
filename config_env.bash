#!/bin/bash

export EMOAW_HOME=`pwd`
export EMOAW_INSTALL_DIR=$EMOAW_HOME/install
export PKG_CONFIG_PATH=${EMOAW_INSTALL_DIR}/glfw/usr/local/lib/pkgconfig/:${PKG_CONFIG_PATH}
export EMOAW_BUILD_DIR=$EMOAW_HOME/build
