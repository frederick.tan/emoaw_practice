/**
 * @file: Time.h
 * @brief: Prints time upon creation and destruction
 * @author: Frederick Tan 
 **/

#include <Time.h>

#include <stdio.h>
#include <sys/time.h>

Time::Time()
{
	gettimeofday(&mBegin, NULL);
}

Time::~Time()
{
	gettimeofday(&mEnd, NULL);
	long long int start = mBegin.tv_usec + (mBegin.tv_sec * 1000000);
	long long int end = mEnd.tv_usec + (mEnd.tv_sec * 1000000);
	printf("\nExecution time: %llu us\n", end - start);
}
