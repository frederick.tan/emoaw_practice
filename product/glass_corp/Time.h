/**
 * @file: Time.h
 * @brief: Prints time upon creation and destruction
 * @author: Frederick Tan 
 **/
#ifndef _TIME_H_
#define _TIME_H_

#include <time.h>

class Time
{
public:
	Time();
	virtual ~Time();
private:
	struct timeval mBegin;
	struct timeval mEnd;
};

#endif
