/**
 * @file: Main.cpp
 * @brief: Magic Cube Representation
 * @author: Frederick Tan
 **/

#include <QuickSort.h>
#include <Time.h>

#include <stdio.h>
#include <stdlib.h>

#include <vector>

//8, 15, 19
//24, 1, 17
//10, 26, 6
//12, 25, 5
//7, 14, 21
//23, 3, 16
//22, 2, 18
//11, 27, 4
//9, 13, 20
const int Layer1Index[] = { 7, 14, 18, 23, 0, 16, 9, 25, 5};
const int Layer2Index[] = { 11, 24, 4, 6, 13, 20, 22, 2, 15};
const int Layer3Index[] = { 21, 1, 17, 10, 26, 3, 8, 12, 19};

static const int CubeCount = 27;

void usage(const char *programName)
{
	printf("%s <n1> <n2> <n3> ... <n27>\n", programName);
	printf("Where <nX> is the transparency value of a cube to be added\n");
	printf("The returned value will be the transparency value of the cubes with the following order:\n");
	printf("[0,0], [0,1], [0,2] \n");
	printf("[0,3], [0,4], [0,5] \n");
	printf("[0,6], [0,7], [0,8] \n");
	printf("-------------------\n");
	printf("[1,0], [1,1], [1,2] \n");
	printf("[1,3], [1,4], [1,5] \n");
	printf("[1,6], [1,7], [1,8] \n");
	printf("-------------------\n");
	printf("[2,0], [2,1], [2,2] \n");
	printf("[2,3], [2,4], [3,5] \n");
	printf("[2,6], [2,7], [2,8] \n");
}

int main(int argc, char *argv[])
{
	Time execTime;
	QuickSort sorter;
	if (argc != CubeCount + 1)
	{
		usage(argv[0]);
		return 1;
	}

	for (int i = 1; i < argc; i++)
	{
		sorter.AddObject(atof(argv[i]));
	}

	sorter.Sort();
	std::vector <double> vect = sorter.GetObjectList();

	// Print layer 1
	int count = 0;
	for (auto index: Layer1Index)
	{
		count++;
		printf("[%08.4lf]", vect[CubeCount - index - 1]);
		if (((count) % 3) == 0)
		{
			printf("\n");
		}
	}
	printf("------------------------\n");
	count = 0;
	for (auto index: Layer2Index)
	{
		count++;
		printf("[%08.4lf]", vect[CubeCount - index - 1]);
		if (((count) % 3) == 0)
		{
			printf("\n");
		}
	}
	printf("------------------------\n");
	count = 0;
	for (auto index: Layer3Index)
	{
		count++;
		printf("[%08.4lf]", vect[CubeCount - index - 1]);
		if (((count) % 3) == 0)
		{
			printf("\n");
		}
	}
	return 0;
}
