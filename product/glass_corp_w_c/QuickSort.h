/**
 * File: QuickSort.h
 * Description: Sorts numbers
 * Created by: Frederick Tan 
 **/
#ifndef _LIBSORT_QUICKSORT_H_
#define _LIBSORT_QUICKSORT_H_

#include <vector>

class QuickSort
{
public:
	QuickSort();
	virtual ~QuickSort();
	void AddObject(double object) { mObjectList.push_back(object); };
	bool Sort() { return Sort(mObjectList, 0, mObjectList.size() - 1); };
	std::vector<double>& GetObjectList() { return mObjectList; };
private:
	void Swap(std::vector<double> &vect, int i, int j) { double temp = vect[i]; vect[i] = vect[j]; vect[j] = temp; };
	int SelectPivot(int i, int j)
	{
		return ((i + j) / 2);
	};
	bool Sort(std::vector<double> &vect, int m, int n);
private:
	std::vector<double> mObjectList;
};

#endif
